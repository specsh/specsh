# Spec.sh website 

Simple website for specsh based on the Forty site template, designed by [HTML5 UP](https://html5up.net/forty). Check out https://hunterchang.com/gatsby-starters/ for more Gatsby starters and templates.

## Installation

Run `gatsby develop` in the terminal to develop the site.
