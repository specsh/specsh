import React from 'react'

const BannerLanding = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>Spec.sh</h1>
            </header>
            <div className="content">
                <p>A new concept to old standards<br />
                Requirements Management done right.</p>
            </div>
        </div>
    </section>
)

export default BannerLanding
