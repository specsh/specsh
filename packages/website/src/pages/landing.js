import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import BannerLanding from '../components/BannerLanding'

import pic08 from '../assets/images/pic08.jpg'
import pic09 from '../assets/images/pic09.jpg'
import pic10 from '../assets/images/pic10.jpg'

const Landing = (props) => (
    <div>
        <Helmet>
            <title>Spec.sh - New concept to old standards</title>
            <meta name="description" content="Landing Page" />
        </Helmet>

        <BannerLanding />

        <div id="main">
            <section id="one">
                <div className="inner">
                    <header className="major">
                        <h2>Show me the specs</h2>
                    </header>
                    <p>Introduction...</p>
                </div>
            </section>
            <section id="two" className="spotlights">
                <section>
                    <Link to="/generic" className="image">
                        <img src={pic08} alt="" />
                    </Link>
                    <div className="content">
                        <div className="inner">
                            <header className="major">
                                <h3>Developer Centric</h3>
                            </header>
                            <p>
                               Keep the specs close. Requirements should be easily managable and should be easy to consult and update.
                            </p>
                            <p>
                               <strong>Spec.sh</strong> got you covered with easy to use <em>cli</em> tools
                               and an intuitive requirements management system.
                            </p>
                            <ul className="actions">
                                <li><Link to="/generic" className="button">Learn more</Link></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section>
                    <Link to="/generic" className="image">
                        <img src={pic09} alt="" />
                    </Link>
                    <div className="content">
                        <div className="inner">
                            <header className="major">
                                <h3>Based on standards</h3>
                            </header>
                            <p>
                              Specifications created with <strong>Spec.sh</strong> are based on the ESA standards for requirements.
                            </p>
                            <ul className="actions">
                                <li><Link to="/generic" className="button">Learn more</Link></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section>
                    <Link to="/generic" className="image">
                        <img src={pic10} alt="" />
                    </Link>
                    <div className="content">
                        <div className="inner">
                            <header className="major">
                                <h3>Write once - Use everywhere.</h3>
                            </header>
                            <p></p>
                            <ul className="actions">
                                <li><Link to="/generic" className="button">Learn more</Link></li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section>
                <Link to="/generic" className="image">
                    <img src={pic10} alt="" />
                    </Link>
                    <div className="content">
                    <div className="inner">
                    <header className="major">
                    <h3>Collaboration and Synchronisation</h3>
                </header>
                <p>
                  <strong>Spec.sh</strong> uses git for version management and synchronisation.
                  Developers can use their existing tools to manage and consult requirements.

                  The management interface keeps track of changes automatically and can approve requirement change requests made by developers.
                </p>
                <ul className="actions">
                    <li><Link to="/generic" className="button">Learn more</Link></li>
                </ul>
                </div>
                </div>
                </section>
            </section>
        </div>

    </div>
)

export default Landing
