# Message

```
const messenger = Messenger({
  base: 'https://schemas.spec.sh'
})

// have to specify the type, even though it will only be send in the header.
messenger.message('/user/info', userInfo)
```
So this should be done on the express/connect level.
e.g. res.send('user/info', userInfo)

Many ways to do it, could also require a class mapping. This is undoable however.
Because we have objects comming from everywhere. 

So for now I want to extend something on request and response.
I would need it on the express level and on the request/axios level.

Which is very easy:

https://stackoverflow.com/questions/31977047/extending-express-request-object-using-typescript

All I need is:
https://github.com/swagger-api/swagger-node

```
app.user((req, res, next) => {
  req.msg(...)
})
```
However not very optimal, extending each request..

```$xslt
const express = require('express')
const schemaParser = require('schema-parser')

const app = express()

// parse application/schema-instance
app.use(schemaParser({
  contentType: 'application/schema-instance+json', // default
  typeField: 'schema', // default
  validate: true
})

app.use(function (req, res) {
  res.setHeader('Content-Type', 'application/schema-instance+json; schema="https://schemas.spec.sh/user/register"'
  res.write('you posted:\n')
  res.end(JSON.stringify(req.body, null, 2))
})
```
Can support schemas to be only valid from a certain base, a whitelist of valid schema domains.
Also each endpoint has valid input & output, and should be defined as such.
I know this is similar to swagger, but swagger is very much about describing.
Not really should again look at swagger, because there exists some servers doing this.
Which I then only have to tweak a bit.

Must be totally based on this: https://github.com/BigstickCarpet/swagger-server

https://github.com/BigstickCarpet/swagger-server
The full stack is what makes it difficult, but once setup...
The guys working for postman, mr messinger.

Man look at this:
```$xslt
    swagger_controllers:
      - onError: json_error_handler
      - cors
      - swagger_security
      - _swagger_validate
      - express_compatibility
      - _router
```
Already there.
Which means for the UI, I only have to put something in the header in able to get the schema.
Probably already that does exist.

### Description

This describes the core message method of spec.sh

At the heart of the messaging system is json schema.

Within json schema we can describe each message format very thoroughly.

Also note the same system is used for any data format description.
So not only in the context of message passing.

The system is created with support for protobuf in mind.

Storage format:
https://docs.mongodb.com/manual/reference/operator/query/jsonSchema/

Each document will be served with:
```json
Content-Type: application/schema-instance+json; schema="https://schemas.spec.sh/user/register"
```
This will ensure the dataflow within the system will stay consistent.
Each microservice will be able to use this information to validate incoming messages and refuse any unknown messages.

I'm planning to use:
`Content-Type: application/schema-instance+json; schema="https://schemas.test.com/user/profile"`
to send along schema information with json documents. Mainly to able to validate documents 

However.... Is't this what swagger is also about?

### Protobuf support
Also not that protobuf has an equivalent for this and protobuf can be directly 
generated from json schemas, so it will be very easy to support both message types.

See: https://tools.ietf.org/id/draft-wright-json-schema-01.html#rfc.section.10.2
And that would totally match with protobuf.

What I'm missing here is how to define a collection.
But I guess it can be derived.
However you cannot derive if the schema itself describes an array?

Exactly what I need:
http://json-schema.org/latest/json-schema-hypermedia.html
Seems to be different. This is the semantic web again, describing what it is.
I do not care about that at the moment.
ALso the same problem "collection and item links":
http://json-schema.org/latest/json-schema-hypermedia.html#collectionAndItem
https://tools.ietf.org/html/rfc6573

There (for me) also is the problem of hybrid collections.
Initially I just reason about clear entities, an error message.
A user profile. User profiles, Error Messages etc.
When returning hybrid collections it's kinda undoable to also describe these.
Although not sure, I think you just should define them, because json schema
fully supports describing collections like that. Generics? <T>[]

```$xslt
 in the case of a Link Header field

   Link: <...>; rel="item"; title="View Product X001"
   Link: <...>; rel="item"; title="View Product X002"
```

Like this seems useful:

http://buzzword.org.uk/2009/draft-inkster-profile-parameter-00.html

```$xslt
Content-Type: application/json; profile="http://purl.example/acme/json/flow"

{
	title : "Complaints Procedure" ;
	steps: [
	 ...
```
And a discussion:
https://github.com/json-schema-org/json-schema-spec/issues/222
Which ofcourse says it's not a solution.

Actual commit, describing how to do it.
https://github.com/json-schema-org/json-schema-spec/pull/414/files#diff-f8351ad4b29f76d24d34d5b0f50ea16bR701

Thus:
```$xslt
Content-Type: application/json; schema="http://example.com/bob"
```
Also notice, this can ofcourse easily be reconfigured later.


### References

 - https://stackoverflow.com/questions/30505408/what-is-the-correct-protobuf-content-type
 - https://tools.ietf.org/html/draft-rfernando-protocol-buffers-00
 - https://developers.google.com/protocol-buffers/docs/techniques#self-description
 - https://www.charlesproxy.com/documentation/using-charles/protocol-buffers/

They talk of sending the message type within the header.

```$xslt
application/protobuf; proto=org.some.Message
application/x-protobuf; messageType="x.y.Z"
application/x-protobuf or application/x-google-protobuf
```

https://www.charlesproxy.com/documentation/using-charles/protocol-buffers/:

This means a complete Content-Type header will look like:
```$xslt

Content-Type: application/x-protobuf; desc="http://localhost/Model.desc"; messageType="com.xk72.sample.PurchaseOrder"; delimited=true
```


