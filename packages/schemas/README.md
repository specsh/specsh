# Spec.sh Schemas

### Deploy schemas

To deploy the schemas run:
`yarn deploy`

### Available schemas

Many useful for web services:
https://github.com/snowplow/iglu-central/tree/master/schemas

Useful:
https://raw.githubusercontent.com/snowplow/iglu-central/master/schemas/com.clearbit.enrichment/person/jsonschema/1-0-0
https://raw.githubusercontent.com/snowplow/iglu-central/master/schemas/com.clearbit.enrichment/company/jsonschema/1-0-0

Rather a few:
https://github.com/kaiinui/jsonschema-schema.org
