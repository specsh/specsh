export const FullSchema = {
  "$schema": "http://json-schema.org/draft-06/schema#",
  "title": "Project Documentation",
  "type": "object",
  "properties": {
    "srd": {
      "$schema": "http://json-schema.org/draft-06/schema#",
      "definitions": {
        "functionalRequirements": {
          "type": "array",
          "title": "Requirements",
          "items": {
            "type": "object",
            "title": "Requirement",
            "properties": {
              "class": {
                "type": "string",
                "title": "Class"
              },
              "attributes": {
                "type": "array",
                "title": "Attributes",
                "items": {
                  "type": "object",
                  "properties": {
                    "name": {
                      "type": "string",
                      "title": "Attribute Name"
                    },
                    "type": {
                      "type": "string",
                      "title": "Attribute Type"
                    },
                    "description": {
                      "type": "string",
                      "title": "Attribute Description"
                    },
                    "priority": {
                      "$schema": "http://json-schema.org/draft-06/schema#",
                      "type": "string",
                      "title": "Priority",
                      "enum": [
                        "M",
                        "S",
                        "C",
                        "W"
                      ]
                    }
                  }
                }
              },
              "functions": {
                "type": "array",
                "title": "Functions",
                "items": {
                  "type": "object",
                  "properties": {
                    "title": {
                      "type": "string",
                      "title": "Function title"
                    },
                    "signature": {
                      "type": "string",
                      "title": "Function Signature"
                    },
                    "description": {
                      "type": "string",
                      "title": "Function Description"
                    },
                    "priority": {
                      "$schema": "http://json-schema.org/draft-06/schema#",
                      "type": "string",
                      "title": "Priority",
                      "enum": [
                        "M",
                        "S",
                        "C",
                        "W"
                      ]
                    },
                    "parameters": {
                      "type": "array",
                      "title": "Parameters",
                      "items": {
                        "type": "object",
                        "properties": {
                          "name": {
                            "type": "string",
                            "title": "Parameter name"
                          },
                          "type": {
                            "type": "string",
                            "title": "Parameter type"
                          },
                          "description": {
                            "type": "string",
                            "title": "Parameter description"
                          },
                          "priority": {
                            "$schema": "http://json-schema.org/draft-06/schema#",
                            "type": "string",
                            "title": "Priority",
                            "enum": [
                              "M",
                              "S",
                              "C",
                              "W"
                            ]
                          }
                        }
                      }
                    },
                    "requirements": {
                      "type": "string",
                      "title": "Specific requirements on the function or parameters"
                    },
                    "precondition": {
                      "type": "string",
                      "title": "Precondition"
                    },
                    "postcondition": {
                      "type": "string",
                      "title": "Postcondition"
                    }
                  }
                }
              }
            }
          }
        },
        "nonFunctionalRequirements": {
          "type": "array",
          "title": "Requirements",
          "items": {
            "type": "object",
            "properties": {
              "environment": {
                "type": "string",
                "title": "Environment"
              },
              "requirements": {
                "type": "array",
                "title": "Requirements",
                "items": {
                  "type": "object",
                  "properties": {
                    "priority": {
                      "$schema": "http://json-schema.org/draft-06/schema#",
                      "type": "string",
                      "title": "Priority",
                      "enum": [
                        "M",
                        "S",
                        "C",
                        "W"
                      ]
                    },
                    "requirement": {
                      "type": "string",
                      "title": "Requirement"
                    }
                  }
                }
              }
            }
          }
        }
      },
      "title": "Software Requirements Document",
      "type": "object",
      "description": "The contents of the ESA description of a SRD,\nwhich is based on the IEEE table of contents.",
      "properties": {
        "Introduction": {
          "type": "object",
          "title": "Introduction",
          "properties": {
            "Purpose": {
              "type": "string",
              "title": "Purpose",
              "description": "The purpose of this particular SRD and its intended readership."
            },
            "Scope": {
              "type": "string",
              "title": "Scope",
              "description": "Scope of the software.\nIdentifies the product by name, explains what the software will do."
            },
            "DefinitionsAndAbbreviations": {
              "type": "object",
              "title": "Definitions and Abbreviations",
              "description": "The definitions of all used terms, acronyms and abbreviations.",
              "properties": {
                "definitions": {
                  "type": "array",
                  "title": "Definitions",
                  "required": [
                    "definition",
                    "description"
                  ],
                  "items": {
                    "type": "object",
                    "properties": {
                      "definition": {
                        "type": "string",
                        "title": "Definition"
                      },
                      "description": {
                        "type": "string",
                        "title": "Description"
                      }
                    }
                  }
                },
                "abbreviations": {
                  "type": "array",
                  "title": "Abbreviations",
                  "required": [
                    "abbreviation",
                    "description"
                  ],
                  "items": {
                    "type": "object",
                    "properties": {
                      "abbreviation": {
                        "type": "string",
                        "title": "Abbreviation"
                      },
                      "description": {
                        "type": "string",
                        "title": "Description"
                      }
                    }
                  }
                }
              }
            },
            "ListOfReferences": {
              "type": "array",
              "title": "List of References",
              "description": "All applicable documents.",
              "items": {
                "type": "string",
                "title": "Reference"
              }
            },
            "Overview": {
              "type": "string",
              "title": "Overview",
              "description": "Short description of the rest of the SRD and how it is organized."
            }
          }
        },
        "GeneralDescription": {
          "type": "object",
          "title": "General Description",
          "properties": {
            "RelationToCurrentProjects": {
              "type": "string",
              "title": "Relation to current projects",
              "description": "The context of this project in relation to other current projects."
            },
            "RelationToPredecessorAndSuccessorProjects": {
              "type": "string",
              "title": "Relation to predecessor and successor projects",
              "description": "The context of this project in relation to past and future projects."
            },
            "FunctionAndPurpose": {
              "type": "string",
              "title": "Function and Purpose",
              "description": "A general overview of the function and purpose of the product."
            },
            "Environment": {
              "type": "string",
              "title": "Environment",
              "description": "Hardware and operating system of target system and development\nsystem. Who will use the system (user roles in URD)."
            },
            "RelationToOtherSystems": {
              "title": "Relation to other systems",
              "description": "Is the product an independent system, part of a larger system,\nreplacing another system? The essential characteristics of these other systems.",
              "$schema": "http://json-schema.org/draft-06/schema#",
              "type": "object",
              "properties": {
                "description": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "string",
                  "title": "Description"
                },
                "sections": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "title": {
                        "type": "string",
                        "title": "Title"
                      },
                      "text": {
                        "type": "string",
                        "title": "Text"
                      }
                    }
                  }
                }
              }
            },
            "GeneralConstraints": {
              "title": "General Constraints",
              "description": "Reasons why constraints exist: background information and justification (analogous to URD).",
              "$schema": "http://json-schema.org/draft-06/schema#",
              "type": "object",
              "properties": {
                "description": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "string",
                  "title": "Description"
                },
                "sections": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "title": {
                        "type": "string",
                        "title": "Title"
                      },
                      "text": {
                        "type": "string",
                        "title": "Text"
                      }
                    }
                  }
                }
              }
            },
            "ModelDescription": {
              "title": "Model Description",
              "description": "A description of the logical model.",
              "$schema": "http://json-schema.org/draft-06/schema#",
              "type": "object",
              "properties": {
                "description": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "string",
                  "title": "Description"
                },
                "sections": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "title": {
                        "type": "string",
                        "title": "Title"
                      },
                      "text": {
                        "type": "string",
                        "title": "Text"
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "SpecificRequirements": {
          "type": "object",
          "title": "Specific Requirements",
          "properties": {
            "FunctionalRequirements": {
              "title": "Functional Requirements",
              "description": "A list of all functional requirements (what should the system do).",
              "type": "array",
              "items": {
                "type": "object",
                "title": "Requirement",
                "properties": {
                  "class": {
                    "type": "string",
                    "title": "Class"
                  },
                  "attributes": {
                    "type": "array",
                    "title": "Attributes",
                    "items": {
                      "type": "object",
                      "properties": {
                        "name": {
                          "type": "string",
                          "title": "Attribute Name"
                        },
                        "type": {
                          "type": "string",
                          "title": "Attribute Type"
                        },
                        "description": {
                          "type": "string",
                          "title": "Attribute Description"
                        },
                        "priority": {
                          "$schema": "http://json-schema.org/draft-06/schema#",
                          "type": "string",
                          "title": "Priority",
                          "enum": [
                            "M",
                            "S",
                            "C",
                            "W"
                          ]
                        }
                      }
                    }
                  },
                  "functions": {
                    "type": "array",
                    "title": "Functions",
                    "items": {
                      "type": "object",
                      "properties": {
                        "title": {
                          "type": "string",
                          "title": "Function title"
                        },
                        "signature": {
                          "type": "string",
                          "title": "Function Signature"
                        },
                        "description": {
                          "type": "string",
                          "title": "Function Description"
                        },
                        "priority": {
                          "$schema": "http://json-schema.org/draft-06/schema#",
                          "type": "string",
                          "title": "Priority",
                          "enum": [
                            "M",
                            "S",
                            "C",
                            "W"
                          ]
                        },
                        "parameters": {
                          "type": "array",
                          "title": "Parameters",
                          "items": {
                            "type": "object",
                            "properties": {
                              "name": {
                                "type": "string",
                                "title": "Parameter name"
                              },
                              "type": {
                                "type": "string",
                                "title": "Parameter type"
                              },
                              "description": {
                                "type": "string",
                                "title": "Parameter description"
                              },
                              "priority": {
                                "$schema": "http://json-schema.org/draft-06/schema#",
                                "type": "string",
                                "title": "Priority",
                                "enum": [
                                  "M",
                                  "S",
                                  "C",
                                  "W"
                                ]
                              }
                            }
                          }
                        },
                        "requirements": {
                          "type": "string",
                          "title": "Specific requirements on the function or parameters"
                        },
                        "precondition": {
                          "type": "string",
                          "title": "Precondition"
                        },
                        "postcondition": {
                          "type": "string",
                          "title": "Postcondition"
                        }
                      }
                    }
                  }
                }
              }
            },
            "NonFunctionalRequirements": {
              "title": "Non-functional Requirements",
              "description": "A list of all non functional requirements (performance, interface,\noperational, resource, verification/testing, portability, maintainability,\nreliability, security, safety, documentation, other, ...), linked to\nfunctional requirements. Each category of non-functional\nrequirements has its own subsection.",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "environment": {
                    "type": "string",
                    "title": "Environment"
                  },
                  "requirements": {
                    "type": "array",
                    "title": "Requirements",
                    "items": {
                      "type": "object",
                      "properties": {
                        "priority": {
                          "$schema": "http://json-schema.org/draft-06/schema#",
                          "type": "string",
                          "title": "Priority",
                          "enum": [
                            "M",
                            "S",
                            "C",
                            "W"
                          ]
                        },
                        "requirement": {
                          "type": "string",
                          "title": "Requirement"
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "RequirementsTraceabilityMatrix": {
          "type": "object",
          "title": "Requirements Traceability Matrix",
          "description": "This linking is done outside of the scope of Json Schema."
        },
        "UI": {
          "type": "object",
          "title": "Ability To show UI Interface"
        }
      }
    },
    "urd": {
      "$schema": "http://json-schema.org/draft-06/schema#",
      "definitions": {
        "requirements": {
          "type": "array",
          "title": "Requirements",
          "items": {
            "type": "object",
            "properties": {
              "priority": {
                "$schema": "http://json-schema.org/draft-06/schema#",
                "type": "string",
                "title": "Priority",
                "enum": [
                  "M",
                  "S",
                  "C",
                  "W"
                ]
              },
              "requirement": {
                "type": "string",
                "title": "Requirement"
              }
            }
          }
        }
      },
      "title": "User Requirements Document",
      "type": "object",
      "description": "The contents of the ESA description of a URD,\nwhich is based on the IEEE table of contents.",
      "properties": {
        "Introduction": {
          "type": "object",
          "properties": {
            "Purpose": {
              "type": "string",
              "description": "The purpose of this particular URD and its intended readership."
            },
            "Scope": {
              "type": "string",
              "description": "Scope of the software.\nIdentifies the product by name, explains what the software will do."
            },
            "DefinitionsAndAbbreviations": {
              "type": "object",
              "description": "The definitions of all used terms, acronyms and abbreviations.",
              "properties": {
                "definitions": {
                  "type": "array",
                  "required": [
                    "definition",
                    "description"
                  ],
                  "items": {
                    "type": "object",
                    "properties": {
                      "definition": {
                        "type": "string",
                        "title": "Definition"
                      },
                      "description": {
                        "type": "string",
                        "title": "Description"
                      }
                    }
                  }
                },
                "abbreviations": {
                  "type": "array",
                  "required": [
                    "abbreviation",
                    "description"
                  ],
                  "items": {
                    "type": "object",
                    "properties": {
                      "abbreviation": {
                        "type": "string",
                        "title": "Abbreviation"
                      },
                      "description": {
                        "type": "string",
                        "title": "Description"
                      }
                    }
                  }
                }
              }
            },
            "ListOfReferences": {
              "type": "array",
              "description": "All applicable documents.",
              "items": {
                "type": "string",
                "title": "Reference"
              }
            },
            "Overview": {
              "type": "string",
              "description": "Short description of the rest of the URD and how it is organized."
            }
          }
        },
        "GeneralDescription": {
          "type": "object",
          "properties": {
            "ProductPerspective": {
              "type": "string",
              "description": "The relation to other systems."
            },
            "GeneralCapabilities": {
              "description": "The main capabilities.",
              "$schema": "http://json-schema.org/draft-06/schema#",
              "type": "object",
              "properties": {
                "description": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "string",
                  "title": "Description"
                },
                "sections": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "title": {
                        "type": "string",
                        "title": "Title"
                      },
                      "text": {
                        "type": "string",
                        "title": "Text"
                      }
                    }
                  }
                }
              }
            },
            "GeneralConstraints": {
              "type": "string",
              "description": "Reasons why constraints exist: background information and justification."
            },
            "UserCharacteristics": {
              "description": "The characteristics of the different user roles.",
              "$schema": "http://json-schema.org/draft-06/schema#",
              "type": "object",
              "properties": {
                "description": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "string",
                  "title": "Description"
                },
                "sections": {
                  "$schema": "http://json-schema.org/draft-06/schema#",
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "title": {
                        "type": "string",
                        "title": "Title"
                      },
                      "text": {
                        "type": "string",
                        "title": "Text"
                      }
                    }
                  }
                }
              }
            },
            "EnvironmentDescription": {
              "type": "string",
              "description": "A description of the operational environment."
            },
            "AssumptionsAndDependencies": {
              "type": "string",
              "description": "The assumptions upon which the specific requirements (in the next section) are based."
            }
          }
        },
        "SpecificRequirements": {
          "type": "object",
          "title": "Specific Requirements",
          "properties": {
            "CapabilityRequirements": {
              "title": "Capability Requirements",
              "description": "A list of all capability requirements.",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "priority": {
                    "$schema": "http://json-schema.org/draft-06/schema#",
                    "type": "string",
                    "title": "Priority",
                    "enum": [
                      "M",
                      "S",
                      "C",
                      "W"
                    ]
                  },
                  "requirement": {
                    "type": "string",
                    "title": "Requirement"
                  }
                }
              }
            },
            "ConstraintRequirements": {
              "title": "Constraint Requirements",
              "description": "A list of all constraint requirements (interfaces, portability,\nadaptability, availability, security, safety, standards, resources,\ntime scales, ...).",
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "priority": {
                    "$schema": "http://json-schema.org/draft-06/schema#",
                    "type": "string",
                    "title": "Priority",
                    "enum": [
                      "M",
                      "S",
                      "C",
                      "W"
                    ]
                  },
                  "requirement": {
                    "type": "string",
                    "title": "Requirement"
                  }
                }
              }
            }
          }
        },
        "UseCases": {
          "type": "object",
          "title": "Use Cases",
          "properties": {
            "description": {
              "type": "string"
            },
            "sections": {
              "type": "array",
              "title": "Sections",
              "items": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string",
                    "title": "Title"
                  },
                  "sections": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "properties": {
                        "title": {
                          "type": "string",
                          "title": "Title"
                        },
                        "goals": {
                          "type": "string",
                          "title": "Goals"
                        },
                        "summary": {
                          "type": "string",
                          "title": "Summary"
                        },
                        "priority": {
                          "$schema": "http://json-schema.org/draft-06/schema#",
                          "type": "string",
                          "title": "Priority",
                          "enum": [
                            "M",
                            "S",
                            "C",
                            "W"
                          ]
                        },
                        "steps": {
                          "type": "array",
                          "title": "Steps",
                          "items": {
                            "type": "object",
                            "properties": {
                              "type": {
                                "type": "string",
                                "enum": [
                                  "action",
                                  "response"
                                ]
                              },
                              "description": {
                                "type": "string",
                                "title": "Description"
                              }
                            }
                          }
                        },
                        "alternatives": {
                          "type": "array",
                          "title": "Alternatives",
                          "items": {
                            "type": "object",
                            "properties": {
                              "ref": {
                                "type": "number",
                                "title": "Reference"
                              },
                              "description": {
                                "type": "string",
                                "title": "Description"
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}