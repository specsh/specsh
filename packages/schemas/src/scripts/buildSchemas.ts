import { compileFromFile } from 'json-schema-to-typescript'
import * as fs from 'fs'
import * as path from 'path'

export interface TypescriptInterface {
  name: string,
  dtsFile: string
}

export function writeDtsFile (schema: TypescriptInterface) {
  fs.writeFileSync(
    path.resolve(
      __dirname,
      `../schemas/${schema.name}.d.ts`
    ),
    schema.dtsFile
  )
}

(async function (): Promise<void> {
  const name = 'index'
  const fileName = `${name}.json`
  const file = path.resolve(__dirname, '../schemas', fileName)
  const cwd = path.resolve(__dirname, '../schemas')
  const dtsFile = await compileFromFile(file, {
    cwd,
    style: {
      singleQuote: true,
      semi: false
    }
  })

  writeDtsFile({
    name,
    dtsFile
  })

  return
})()
  .catch((error) => {
    console.error(error)
  })
