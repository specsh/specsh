import * as GraphQLSchema from 'jsonschema-to-graphql'
import * as Resolver from 'jsonschema-to-graphql/dist/knex-resolver'
import * as knex from 'knex'
import { loadSchema, writeSchema } from '../schema'

const resolver = new Resolver(knex)

const opts = {
  resolver : resolver,
  skipConstraintModels: false,
  skipOperatorFields: false,
  skipPaginationFields: false,
  skipSortByFields: false
}

;(async () => {
  const graphQLSchema = GraphQLSchema.builder(opts)
    .addSchema(
      loadSchema('index.json')
      , {
        // exclude: ['ignoreField']
      }
    )
    // .addCustomQueryFunction(require('./schemas/CustomFunction'))
    .build()

  writeSchema(graphQLSchema, 'fullSchema.ql')
})()
  .catch((error) => {
    console.log(error)
  })
