import { loadSchema, writeSchema } from '../schema'

(async () => {
  const fullSchema = await loadSchema('index.json')

  writeSchema(fullSchema, 'fullSchema.json')
})()
  .catch((error) => {
    console.error(error)
  })
