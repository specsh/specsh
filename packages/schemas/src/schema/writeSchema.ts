import {resolve} from 'path'
import { writeFile } from '../file'
import { JSONSchema6 } from 'json-schema'

export async function writeSchema (schema: JSONSchema6, fileName: string) {
  return writeFile(
    resolve(
      __dirname,
      '../schemas',
      fileName
    ),
    JSON.stringify(schema, null, 2)
  )
}
