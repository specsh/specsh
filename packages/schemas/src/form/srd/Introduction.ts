import { flexDiv } from '../common/index'

export const Introduction = [
  {
    key: 'Purpose',
    type: 'textarea'
  },
  {
    key: 'Scope',
    type: 'textarea'
  },
  {
    key: 'DefinitionsAndAbbreviations.definitions',
    type: 'array',
    items: [{
      ...flexDiv,
      items: [
        { key: 'DefinitionsAndAbbreviations.definitions[].definition', flex: '1 1 25%', notitle: true, placeholder: 'Definition' },
        { key: 'DefinitionsAndAbbreviations.definitions[].description', flex: '1 1 75%', notitle: true, placeholder: 'Description' }
      ]
    }]
  },
  {
    key: 'DefinitionsAndAbbreviations.abbreviations',
    type: 'array',
    items: [{
      ...flexDiv,
      items: [
        { key: 'DefinitionsAndAbbreviations.abbreviations[].abbreviation', flex: '1 1 25%', notitle: true, placeholder: 'Abbreviation' },
        { key: 'DefinitionsAndAbbreviations.abbreviations[].description', flex: '1 1 75%', notitle: true, placeholder: 'Description' }
      ]
    }]
  },
  {
    key: 'ListOfReferences',
    type: 'array',
    items: [
      {
        key: 'ListOfReferences[]',
        placeholder: 'Reference'
      }
    ]
  },
  {
    key: 'Overview',
    type: 'textarea'
  }
]
