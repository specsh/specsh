/**
 * The index contains the entire Software Requirements document.
 *
 * All form definitions are there.
 *
 * Only thing which needs to happen now is to make include work
 * recursively for the entire document.
 *
 * Which means it must be able to find and translate all keys.
 *
 * Then it should become possible to pick *any* segment of the document and pop up the correct form.
 *
 * So a wizard can be created and modals of the different segments.
 *
 * Next I must enable saving and fetching the document.
 *
 * Implement the left menu, which is just a stock sidebar.
 * It shows the index of the document. Clicking it shows you the relevant forms.
 *
 * Next per section it can be determined whether it should actually only show the form
 * or a different kind of layout. e.g. the top chapters could just show some cards which you
 * can click to go deeper into the document. Perhaps the card already list the available sections also
 * These cards could also show some statistical information about the items contained within the subsections.
 *
 * Next actual styling of the document.
 *
 * The preview window can be a collapsable sidebar on the right.
 * Mostly it will just be a common mark renderer, not how also these templates will be grouped per section.
 * Just like all other .yaml .ts .json .d.ts files are now.
 **/
import { FunctionalRequirements } from './SpecificRequirements/FunctionalRequirements'
import { SpecificRequirements } from './SpecificRequirements'
import { Introduction } from './Introduction'
import { GeneralDescription } from './GeneralDescription'

export const layouts = {
  FunctionalRequirements,
  GeneralDescription,
  SpecificRequirements,
  Introduction
}