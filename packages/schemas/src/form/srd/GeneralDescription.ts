import { include } from '../util/include'

export const chapters = {
  type: 'array',
  items: [
    { key: 'title', type: 'text' },
    { key: 'text', type: 'textarea' }
  ]
}

export const GeneralDescription = [
  {
    key: 'RelationToCurrentProjects',
    type: 'textarea'
  },
  {
    key: 'RelationToPredecessorAndSuccessorProjects',
    type: 'textarea'
  },
  {
    key: 'FunctionAndPurpose',
    type: 'textarea'
  },
  {
    key: 'Environment',
    type: 'textarea'
  },
  {
    key: 'RelationToOtherSystems',
    items: [
      {
        key: 'RelationToOtherSystems.description',
        type: 'textarea'
      },
      {
        key: 'RelationToOtherSystems.sections',
        ...include('RelationToOtherSystems.sections[]', chapters)
      }
    ]
  },
  {
    key: 'GeneralConstraints',
    items: [
      {
        key: 'GeneralConstraints.description',
        type: 'textarea'
      },
      {
        key: 'GeneralConstraints.sections',
        ...include('GeneralConstraints.sections[]', chapters)
      }
    ]
  },
  {
    key: 'ModelDescription',
    items: [
      {
        key: 'ModelDescription.description',
        type: 'textarea'
      },
      {
        key: 'ModelDescription.sections',
        ...include('ModelDescription.sections[]', chapters)
      }
    ]
  }
]
