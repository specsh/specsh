import { FunctionalRequirements } from './SpecificRequirements/FunctionalRequirements'
import { NonFunctionalRequirements } from './SpecificRequirements/NonFunctionalRequirements'

export const SpecificRequirements = [
  {
    key: 'FunctionalRequirements',
    type: 'array',
    items: [
      ...FunctionalRequirements
    ]
  },
  {
    key: 'NonFunctionalRequirements',
    type: 'array',
    items: [
      ...NonFunctionalRequirements
    ]
  }
]
