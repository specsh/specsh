import { flexDiv, priority } from '../../common/index'

/**
 * Ok have to rethink this.
 *
 * Easiest way to create this visually?
 * Seems flattening all the keys doesn't help much..
 *
 *
 */

export const FunctionalRequirements = [
  {
    key: 'FunctionalRequirements[].class'
  },
  {
    key: 'FunctionalRequirements[].attributes',
    type: 'array',
    listItems: 1,
    items: [
      {
        ...flexDiv,
        items: [
          {
            key: 'FunctionalRequirements[].attributes[].name',
            flex: '1 1 15%',
            notitle: true,
            placeholder: 'Name'
          },
          {
            key: 'FunctionalRequirements[].attributes[].type',
            flex: '1 1 15%',
            notitle: true,
            placeholder: 'Type'
          },
          {
            key: 'FunctionalRequirements[].attributes[].description',
            flex: '4 4 50%',
            notitle: true,
            placeholder: 'Description'
          },
          {
            key: 'FunctionalRequirements[].attributes[].priority',
            ...priority
          }
        ]
      }
    ]
  },
  {
    key: 'FunctionalRequirements[].functions',
    type: 'array',
    listItems: 1,
    items: [
      {
        ...flexDiv,
        items: [
          {
            key: 'FunctionalRequirements[].functions[].signature',
            flex: '1 1 25%',
            notitle: true,
            placeholder: 'Signature'
          },
          {
            key: 'FunctionalRequirements[].attributes[].description',
            flex: '1 1 55%',
            notitle: true,
            placeholder: 'Description'
          },
          {
            key: 'FunctionalRequirements[].attributes[].priority',
            ...priority
          }
        ]
      }
    ]
  }
]
