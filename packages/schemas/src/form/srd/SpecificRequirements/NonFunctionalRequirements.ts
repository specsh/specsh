import { flexDiv, priority } from '../../common/index'

export const NonFunctionalRequirements = [
  {
    key: 'NonFunctionalRequirements[].environment'
  },
  {
    key: 'NonFunctionalRequirements[].requirements',
    type: 'array',
    listItems: 1,
    items: [
      {
        ...flexDiv,
        items: [
          {
            key: 'NonFunctionalRequirements[].requirements[].requirement',
            flex: '1 1 55%',
            notitle: true,
            placeholder: 'Requirement'
          },
          {
            key: 'NonFunctionalRequirements[].requirements[].priority',
            ...priority
          }
        ]
      }
    ]
  }
]
