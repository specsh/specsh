import { isObject } from '../isObject'
import { setPrefix } from './setPrefix'

export function prefixObject (prefix: any) {
  return (value: any) => {
    if (isObject(value)) {
      return {
        ...value,
        key: setPrefix(prefix, value.key)
      }
    }
  }
}
