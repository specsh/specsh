export function setPrefix (prefix: string, key: string) {
  return `${prefix}.${key}`
}
