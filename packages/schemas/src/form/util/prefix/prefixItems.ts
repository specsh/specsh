import { prefixObject } from './prefixObject'
import { FormItems } from '../../types'

export function prefixItems (prefix: string, items: FormItems) {
  return items.map(prefixObject(prefix))
}
