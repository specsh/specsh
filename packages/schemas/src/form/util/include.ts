import { prefixItems } from './prefix'
import { FormEntity } from '../types'

/**
 * Makes sure the keys are correctly prefixed.
 *
 * Right now only supports one kind of key. (object within items)
 * But should support all kinds (direct key (string), etc.)
 *
 * @param prefix
 * @param entity
 * @returns {any}
 */
export function include (prefix: string, entity: FormEntity) {
  if (entity.items) {
    return {
      ...entity,
      items: prefixItems(prefix, entity.items)
    }
  }
  return entity
}
