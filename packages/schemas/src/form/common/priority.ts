export const priority = {
  flex: '2 2 20%',
  type: 'radiobuttons',
  notitle: true,
  titleMap: [
    { value: 'M', name: 'M' },
    { value: 'S', name: 'S' },
    { value: 'C', name: 'C' },
    { value: 'W', name: 'W' }
  ]
}
