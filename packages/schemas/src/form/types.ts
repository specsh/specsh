export type FormItems = any[] // later also: | string

export interface FormEntity {
  items: FormItems
}
