# Spec.sh git adapter

Maintains the `.spec` directory within your repository.

This adapter is used by several other adapters:
 - specsh-adapter-http
 - specsh-adapter-electron
 - specsh-adapter-cli

All changes done on a spec.sh document will eventually be saved within a git repository.

In able to expose this functionality to the web interface a http server will have to
be started which provides an API to store changes locally in git.

The cli can do this directly using the generic specsh-git adapter.

Adapter-http is an intermediate sollution to easily apply changes from the UI.
However is this the final solution? There is electron and webbased.

Webbased will only be able to communicate with github to apply the changes.

My first goal is to enable doing it locally and one repository at a time.
So it seems electron based make more sense than from a ui website + http endpoint.

I cannot ignore this fact, because it's rather crucial to the concept.
And I shouldn't start code anything which I will not use anyway.


