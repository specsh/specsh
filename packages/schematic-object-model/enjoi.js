const Joi = require('joi')
const enjoi = require('enjoi')

const schema = require('../schemas/src/schemas/resume/resume.json')

const jschema = enjoi(schema)

console.log(jschema)

// doesn't seem so simple..Joi is in the whole structure.

Joi.validate({
    basics: {
      name: 'Rob Halff'
    }
}, jschema, function (error, value) {
    if (error) {
        console.error(error)
    } else {
        console.log('correct')
    }
})


