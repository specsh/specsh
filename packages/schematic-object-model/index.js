/**
 * Schema Relations
 *
 */

const ProjectSchema = {
    properties: {
        title: {
            type: 'string',
            title: 'Project Title'
        },
        description: {
            type: 'string',
            title: 'Description'
        }
    }
}

const SRDSchema = {
    title: 'Software Requirements Document',
    properties: {
        title: {
            type: 'string',
            title: 'Title'
        },
        contents: {
            type: 'string',
            title: 'Contents'
        }
    }

}

const SchemaSet = {
    title: 'Project Definition',
    properties: {
        project: {
            title: 'Project',
            $ref: '//project'
        },
        srd: {
            title: 'Software Requirements Diagram',
            $ref: '/srd'
        }
    }
}
// the problem is this is one on one, so the relations ship does not become clear.
// still I must define which parts to chop? Yeah, because now I'm only composing the schema.
const RelationSchema = [
    [{
        schema: '://project',
        path: '/srd'
    },
    {
        schema: '://srd'
    }]
]
Ok, there is disambiguation between SchemaSet and RelationSchema now
I must not use $ref, because that makes the SchemaSet just another schema, which confuses.

const SchemaSet = {

}

