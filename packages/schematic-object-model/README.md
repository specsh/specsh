# Schematic Object Relational Model

This repository will demonstrate the usage of this kind of linking.

Including storage to mongodb.

### Link Schema

Starts with unrelated entities, just like a class diagram/entity relation ship diagram.

Next the links are defined. The link schema describes which relations are possible.
External links could go through a schema proxy to always get correct definitions of 
those external entities.

Entities:
  - Project
  - SRD
  - URD
  
Linked Relations:
  -
  
 Also keep in mind the consumabilty of these relations.
 And the forms in which they should be rendered.
 - no rendering (hidden relationship)
 - select list
 - tags
 - rendering but relationship as a fact, immutable in the form.
 - inline, add/edit/remove the related entity inline
 
Also note there can be just one schema with items.
Yet the items are promoted to a seperate collection.
Which:
 1. should ensure the items become a reference to a seperate schema.
 2. above is not even necessary actually.
 
I would like to create layers on top defining the relations
 
Each action causing transformation. 
Very much like schema.links. However links is used in a different context there.
Links are part of hyper schema.

Ok I don think $ref should dictated whether something can be defined as
a relation or link, It should matter really. whether it's defined inline or not.
Also the relation declaration must not be tied to the schema itself.
One can define multiple relation sets on one schema.
E.g. inline images, or a gallery with infinite images.
This also should facilitate easy restructuring.
I feel for defining the relationships using dot paths, with recipes.
The relation must not contradict the schema, thus
only array items are candidates for linking.
Two schemas will be unchanged, the relation is always defined as
a third party. Thus always defined as many to many.
1 to 1 is a many to many relation ship with both end points having 
a limit of 1

This is very much like how graphs are defined.
Schemas are the components.
The edges/connections are the relation ship.
Which means in an applications one does not speak of schemas
as seperate entities, but always in the form of a graph.
The linkage in this case in done by dot paths.
Which links one part of a schema to another part of a schema.
mostly this will be a deep path x.x.x vs another schema's root .

schema1#/x.x.x.x -> schema2#

But while we are working with schemas we will use the / separator.

Thus one relation ship in it core looks like:
a/b/c/d -> /

where a/b/c/d defines whether we have oneOf or manyOf the right hand side.

schema1#a/b/c/d -> schema2#
schema1#a/b/c/d -> schema3#
Defines 2 relations, 2 edges to two different schemas.
Which could be oneOf, anyOf, allOf,
however notice, that that relationship originates from the definition in
schema1# which defines this, just like normal.
So in that regard it's exactly the same as $ref, however $ref does not
define a relations ship.

So we apply to the rules of the already defined schema.
Then however there is the case of 2 unrelated schemas.
By defining the relations we effectively add fields to the left hand schema.
And I feel a lot for having those links be defined one way always.
So if one relation applies another, another relation must be created.
Because (maybe even in many cases), right hand side does not even
have to be aware it's part of a greater collection.
Being able to add extra and different meta data based on direction
also enables more fine grained control.

    one on one
 ------------------>
<-----------------

Something else:

{
  provider: {
    type: 'string'
  }
}

schema1#provider -> schema2#
Where the relation ship is one on one or one to many.
Where on on one is also one to Many with a limit of 1.

In my setup schema always equals collection.

So when defined you end up with a relationional schema.
This schema is not yet the relational schema instance.

So how to create an instance of this relational schema.

{
  "source": {
    "schema": "http:///....",
    "path": "x/x/y" // cannot be root.
  }, 
  "target": {
    "schema": "http:///...."
    "path": "/" // mostly aways root (thus the default if omitted)
  }
}

[ // exactly 2 [0] source [1] target
  {
    "schema": "http://....",
    "path": "a/b/c"
  },
  {
    "schema": "http://...",
  }
]

Ok, that's the first step, actually define there is a relation.

This modeling of the relation is imporant.
We can then also define constraints within this relation ship (filters)
Which basically directly map to mongo query parameters.
Then these relations can be applied based on conditions.
e.g. the user's scope.
If the user's scope is different a schema can be linked together 
differently. Note how this is only possible as long as we do not
modify the source and target schema's themselves, which is covered.

So the relational modelling will be rather robust.

Now we have the schema (components) and the Relational schemas (links)
as basic building blocks for the Schematic object relational model.

As said the relation set and a schema are also not a one on one relationship.
A schema can have many relational sets. Mainly defering by their filter parameters.

This implies the fact that there must be a way to determine which
relational set currently applies to the schema being displayed.

If there is only one it's easy, just show it.

However other options are, load none, load one, or load a specific one.

Now this also implies we need a container model effectively,
making any of these combinations a fixed set itself.
Just like how flows are also a component.
This also means we never work with schemas directly as an entity.
Schemas are always contained within a wrapping container/schema.

e.g.

Linked schema 1
<container>
  <ProjectSchema />
  <LinkSet />
</container>

Linked schema 2 
<container>
  <ProjectSchema />
  <LinkSet2 />
</container>

The above are then schemas themselves.
Perhaps the above notation also indicates the seperation of concern.
Who is responsible for creating these containers.
 - Schemas
 - Schema Instances
 - Relations
 - Relation Instances
 - SchemaSets
 - SchemaSet Instances
 
Ok, anything substancial yet?...
So I create a new instance of a schema set.
No of SchemaSets there are no instances.
A schemaset will lead to representation of the schema.
Which is then submitted and creates instances of the schema
and instances of the relations and possibly instances of
the related schema, but that's only the case if the relation
was create/updated/removed inline.

And both the schema at the left as right side are very simple instances.
Just instances of their own schemas.

Even variations in Schema sets will still result in simple creation
of the left and right schema instances.

The schema set however defines how information is retrieved and stored.

The frontend will then just requests schema sets, with optional dynamic
parameters passed to it. It's probably very much how views work, but that's ok

So I think I'm actually getting close.
Should I call it a schema set?  RequestModel, PostModel?
Neh SchemaSet
I could look at json-ld and hyperschema a bit more.
However just for notation, because they serve a very different purpose.



 




### ...

belongsTo
belongsToMany
hasOne
hasMany

scopes:
wrapping nature, defines context for the query.
e.g. client scope, user scope etc.
These are generic constraints.
Which may or might not be present.
Scope is not really about linkage.
But constraints to the overall, which can be applied
to the linkage but also the subject itself.

https://docs.mongodb.com/manual/core/data-model-design/
https://docs.mongodb.com/manual/tutorial/model-referenced-one-to-many-relationships-between-documents/
http://docs.sequelizejs.com/manual/tutorial/associations.html

### Srd example

We will define a project first.
Projects can contain one occurance of each document type.
Each document belongs to one project.
Each project has only one of each document type (src, urd)
Each project belongs to a user, however this constraint
is already accomplished on the database level.
The user is the owner of the database.
which means a user can potentially give fine grained access
to his own resources. On dot path level.
So permissions can be handled by views on the queries itself.
I will however at this point not include that yet.

For now I need:
 - A schema to create a project
 - A schema to create each document (srd, urd)
 - A way to link documents to projects.
 - A definition schema which describes these relationships.
 - Knowledge by the framework on how to render these relationships.
   Both in form as in display.
   
### In essence.

In essence each schema includes *all* references.
As if everything would have to be created in one form.
A relation schema can define which parts should be linked
instead of included into the schema.
This means on `$ref!` level I should define how to resolve
a reference. Should it be included, or are there any linkage
rules, which need to be applied.

If there is a linkage rule, it has implications for that field.
e.g. has many projects, will result in the ability to add rows.
However it will result in select boxes to pick the right item.
With possible ability to jump to creation of such an item.
It does not only have consequences for how the form is render
but also on how the data is stored.
Perhaps we should not change the fields to be suffixed by `_id`
but keep the naming as is.
I want to say this `$ref` is now a new collection.
So I think there should be a new collection named `relations`
Which will contain all the ref links.
So either resolve the `$ref` or treat the `$ref` as a relation.

```angular2html
$source_ref, $target_ref, relationType
source_schema, target_schema, relationType
```
relationType: belongsTo, belongsToMany, hasOne, hasMany (oneOf, allOf)
one to one is the same as one to many with the limit set to 1.
So there are only collections.

Many-to-Many. e.g. friends.
On first thought this just means recursive references.
Graph, unlimited linking. Also two way linking.
user1_id user2_id
user2_id user1_id
The first does *not* imply the second, doesn't imply because it
complicates logic. So if it's two way, you must define it as such.
and insert two relationships.


## References:
Tries to solve the same problem, it think this is very close to what I want.
So I should probably use it.
http://jsonapi.org/format/#fetching
http://legacytotheedge.blogspot.nl/2015/03/tips-for-designing-json-schema.html

Ok this should boost the implementation:
https://github.com/holidayextras/jsonapi-server
Don't really like how it's written with Joi included everywhere.
Also it doesn't use schemas at all. However I like the whole
concept of how the api is defined, so I want to use that.
Yeah very much would like to use that one as references.
But keep Json Schema as source and for validation.
I could reuse the test suite.

```angular2html
  var collection = this._db.collection(request.params.type);
```
Means it will work exactly how I want, because the type will be the schema name.
So the only disliked thing is joy vs json schema.
Happa: https://github.com/tlivings/enjoi

Ok, lux is very well written..

This is useful:
https://github.com/SeyZ/jsonapi-serializer
JSONApiSerializer('http://specsh/my.schema.json', opts).serialize(data)
So I would use that in conjuction with my own storage in mongodb.
I could also use the short name 'resume' and have the schema be
available in the registery.




I want to use the type: '' to just point to the json schema..

Tries something similar.
https://github.com/json-schema-faker/json-schema-faker/issues/299
