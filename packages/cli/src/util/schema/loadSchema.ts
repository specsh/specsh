import * as path from 'path'
import { readFile } from '../file'
import { JSONSchema6 } from 'json-schema'

const parser = require('json-schema-ref-parser')

export async function loadSchema (name: string): Promise<JSONSchema6> {
  const schema = await readFile(
    path.resolve(__dirname, '../../../schemas/', name),
    'utf-8'
  )

  return parser.dereference(JSON.parse(schema))
}
