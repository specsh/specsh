export function schemaPath (path: string) {
  return 'properties.' + path.split('.').join('.properties.')
}
