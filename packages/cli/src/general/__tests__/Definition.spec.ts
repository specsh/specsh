import { Definition } from '../Definition'

describe('Definition', () => {
  it('can create an definition', () => {
    const def = {
      definition: 'user requirements document',
      description: 'a document that specifies what the user expects the software to be able to do.'
    }

    const definition = new Definition(def.definition, def.description)

    expect(JSON.stringify(def)).toEqual(JSON.stringify(definition))
  })
})
