import { Abbreviation } from '../Abbreviation'

describe('Abbreviation', () => {
  it('can create an abbreviation', () => {
    const abbreviation = new Abbreviation('AI', 'Artificial Intelligence')

    expect(JSON.stringify(abbreviation)).toEqual(JSON.stringify({
      abbreviation: 'AI',
      description: 'Artificial Intelligence'
    }))
  })
})
