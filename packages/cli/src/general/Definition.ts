export class Definition {
  constructor (
    public definition: string,
    public description: string
  ) {}
}
