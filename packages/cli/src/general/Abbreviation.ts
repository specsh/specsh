export class Abbreviation {
  constructor (
    public abbreviation: string,
    public description: string
  ) {}
}
