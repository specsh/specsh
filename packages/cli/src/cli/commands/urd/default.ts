import { prompt } from 'inquirer'
// import * as Dot from 'dot-object'
import { loadSchema } from '../../../util'

import {
  Command,
  Context,
  command,
  metadata
} from 'clime'

export class CliContext extends Context {
  async promptForInput (): Promise<any> {
    // can become generic schema inquirer.
    const answer = await prompt([{
      name: 'accepted',
      type: 'confirm',
      message: 'Accept as baseline?',
      default: false
    }])

    return answer
  }
}

@command({
  description: 'URD Creation'
})
export default class extends Command {
  @metadata
  async execute (context: CliContext) {
    const schema = await loadSchema('UserRequirementsDocument.yaml')
    return JSON.stringify(schema)
  }
}
