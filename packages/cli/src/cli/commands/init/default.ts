import {
  Command,
  command,
  param
} from 'clime'

@command({
  description: 'Initialize .spec directory'
})

export default class extends Command {
  public execute (
    @param({
      name: 'dir',
      description: 'Spec.sh directory',
      required: true,
      default: '.spec'
    })
    dir: string
  ) {
    return `Hello, ${dir}!`
  }
}
