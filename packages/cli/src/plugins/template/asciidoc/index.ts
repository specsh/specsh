import * as asciidoctorJS from 'asciidoctor.js'
import { ITemplatePlugin } from '../../../core'

const asciidoctor = asciidoctorJS()

export class AsciidoctorTemplate implements ITemplatePlugin {
  // would be nice if I can preload with asciidoctor
  // now I have the problem I should join these files myself.
  // because I have to apply mustache also.
  // Ok so during include, I have to apply mustache first to
  // repeat the blocks, it seems this is possible.
  // ok so the includes are done automatically.
  // then we have the option to act on the entire document.
  convert () {
    const content = 'http://asciidoctor.org[*Asciidoctor*] ' +
      'running on http://opalrb.org[_Opal_] ' +
      'brings AsciiDoc to Node.js!'

    const html = asciidoctor.convert(content)

    console.log(html)
  }
}
