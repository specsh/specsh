# Spec.sh - Template Plugin Asciidoc

This is the spec.sh Asciidoc Template Plugin.

All Spec.sh template plugins use Mustache for dynamic content generation.

A part from that there is no extra syntax.

The following variables are available:

 - `user_requirements`
 - `abbreviations: Array<{abbreviation: string, description: string>)`
 - `definitions: Array<{definition: string, description: string>)`
 etc..
