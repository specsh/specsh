import * as express from 'express'
import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { DocumentsModule } from '../../src/app/modules/documents'
import { DocumentsService } from '../../src/app/modules/documents/documents.service'
import { INestApplication } from '@nestjs/common'

describe('Documents', () => {
  let server
  let app: INestApplication

  const documentsService = { findAll: () => ['srd'] }

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [DocumentsModule],
    })
      .overrideComponent(DocumentsService)
      .useValue(documentsService)
      .compile()

    server = express()
    app = module.createNestApplication(server)
    await app.init()
  })

  it(`/GET documents`, () => {
    return request(server)
      .get('/documents')
      .expect(200)
      .expect({
        data: documentsService.findAll()
      })
  })

  afterAll(async () => {
    await app.close()
  })
})