export const environment = {
  port: Number(process.env.PORT) | 3030,
  whitelist: [
    'http://localhost:4200',
    'http://specsh'
  ]
}
