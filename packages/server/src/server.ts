import { NestFactory } from '@nestjs/core'
import { ApplicationModule } from './app/app.module'
import { INestApplication } from '@nestjs/common/interfaces/nest-application.interface'
import * as helmet from 'helmet'
import { origin } from './app/common/cors'
import { environment } from './environments/environment'

const app: Promise<INestApplication> = NestFactory.create(ApplicationModule)
app.then(instance => {
  instance.enableCors({ origin })
  instance.use(helmet())
  instance.listen(environment.port, () =>
    console.log(`Listening on port ${environment.port}`)
  )
})
  .catch((error) => {
    console.error(error)
  })
