// const fullSchema = require('specsh-schemas/src/schemas/fullSchema.json')
import { FullSchema } from '@specsh/schemas'

// const SYNC_URL = 'http://localhost:10102/'

const schema = {
  version: 0,
  type: 'object',
  properties: {
    type: {
      type: 'string',
      enum: ['srd', 'urd'],
      primary: true
    },
    contents: {
      type: 'object',
      properties: FullSchema.properties.srd.properties
    }
  }
}

export const documentsProviders = [
  {
    provide: 'DocumentModelToken',

    useFactory: async (database: any) => {
      return database.collection({
        name: 'srd',
        schema
      })
    },
    inject: ['RxDBToken']
  }
]
