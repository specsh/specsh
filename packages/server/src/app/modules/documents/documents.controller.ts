import {
  Bind,
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Put,
  Res
} from '@nestjs/common'
import {
  ApiUseTags,
  ApiResponse,
  ApiOperation
} from '@nestjs/swagger'

import { DocumentsService } from './documents.service'

@ApiUseTags('documents')
@Controller('documents')
export class DocumentsController {
    constructor (
    private readonly documentsService: DocumentsService
  ) {}

  // PUT /documents { type: xxx, contents: xxx }
    @Put()
  @ApiOperation({
    title: 'Create / Update document'
  })
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.'
  })
  @ApiResponse({
    status: 403,
    description: 'Forbidden.'
  })
  async create (
    @Body() body,
    @Res() res
 ) {
    const { type, contents } = body

    const ret = await this.documentsService.create({
      type,
      contents
    })

    res.status(HttpStatus.OK).json(ret)
  }

  // GET /documents/findAll
    @Get()
  @Bind(Res())
  async findAll (res) {
    const ret = await this.documentsService.findAll()
    res.status(HttpStatus.OK).json(ret)
  }

  // let's hope the above has precedence
  // GET /api/documents/:name
    @Get('/:type')
  @Bind(Param('type'), Res())
  async getDocumentByType (type, res) {
    const document = await this.documentsService.findByType(type)

    res.status(HttpStatus.OK).json(document)
  }

  // GET /api/documents/findOne/:index
    @Get('/findOne/:index')
  @Bind(Param('index'), Res())
  async find (param, res) {
    const index = parseInt(param, 10)
    const ret = await this.documentsService.findOne(index)

    res.status(HttpStatus.OK).json(ret)
  }
}
