import { Module } from '@nestjs/common'
import { DocumentsController } from './documents.controller'
import { DocumentsService } from './documents.service'
import { documentsProviders } from './documents.providers'
import { gitstoreProviders } from '../../common/providers/gitstore.providers'
import { DatabaseModule } from '../../database'

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [
    DocumentsController
  ],
  providers: [
    DocumentsService,
    ...documentsProviders,
    ...gitstoreProviders
  ]

})
export class DocumentsModule {}
