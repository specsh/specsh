import {
  HttpException,
  Inject,
  Injectable
} from '@nestjs/common'

@Injectable()
export class DocumentsService {
    constructor (
    @Inject('DocumentModelToken') private readonly documentModel,
    @Inject('GitStoreToken') private readonly gitStore
  ) { }

    async create (item: any) {
    const result = await this.documentModel.upsert(item)

    await this.updateStore()

    return result
  }

    async updateStore () {
    return this.documentModel
      .dump(true)
      .then((json: any) => {
        this.gitStore.update(json.name, json)
      })
  }

    async findAll () {
    return this.documentModel.find().exec()
  }

    async findByType (type: string) {
    return this.documentModel
      .findOne()
      .where('type')
      .eq(type)
      .exec()
  }

    async findOne (params: any) {
    return this.documentModel.findOne(params).exec()
  }

    async notFound (item: any) {
    if (!item) {
      throw new HttpException('Documents not found', 404)
    }
    return Promise.resolve(item)
  }
}
