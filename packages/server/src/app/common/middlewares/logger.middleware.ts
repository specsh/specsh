import * as logger from 'npmlog'

import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response } from 'express'

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    use (req: Request, _: Response, next: Function) {
    logger.http(req)
    next()
  }
}
