import { writeFile, readFile, mkdirp } from 'fs-extra'
import { resolve } from 'path'
import * as yaml from 'js-yaml'

const dir = './.specsh'

const extensions = {
  yaml: 'yml',
  json: 'json'
}

export interface GitStoreOptions {
    dir: string,
    format: 'yaml' | 'json'
}

class GitStore {
    constructor (
    private options: GitStoreOptions
  ) { }

    async init () {
    await mkdirp(resolve(this.options.dir))

    return true
  }

    ext (name: string) {
    return `${name}.${extensions[this.options.format]}`
  }

    async initCollection (name: string) {
    await mkdirp(this.options.dir)

    return writeFile(
      resolve(
        this.options.dir,
        this.ext(name)),
        ''
      )
  }

    async loadCollection (name: string) {
    return readFile(
      resolve(
        this.options.dir,
        this.ext(name)
      )
    )
  }

    async load (name: string) {
    let json

    try {
      json = this.loadCollection(name)
    } catch {
      await this.initCollection(name)
      json = this.loadCollection(name)
    }

    return json
  }

    serialize (json: any): string {
    if (this.options.format === 'yaml') {
      return yaml.safeDump(json)
    }

    return JSON.stringify(json)
  }

    deserialize <T> (json: any): T {
    if (this.options.format === 'yaml') {
      return yaml.safeLoad(json)
    }

    return JSON.parse(json) as T
  }

    async update (name: string, json: any) {
    return writeFile(
      resolve(
        this.options.dir,
        this.ext(name)),
        this.serialize(json)
    )
  }

    static create (options: GitStoreOptions) {
    return new GitStore(options)
  }
}

export const gitstoreProviders = [
  {
    provide: 'GitStoreToken',
    useFactory: async () => {
      const store = GitStore.create({
        dir,
        format: 'yaml'
      })

      await store.init()

      return store
    }
  }
]
