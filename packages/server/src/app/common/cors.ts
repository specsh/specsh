import { environment } from '../../environments/environment'

const {
  whitelist = []
} = environment

export function origin (
  origin: string,
  callback: (arg1: Error | null, arg2?: boolean) => void
) {
  if (origin === undefined || whitelist.indexOf(origin) !== -1) {
    callback(null, true)
  } else {
    callback(new Error(`not allowed by cors: ${origin}`))
  }
}
