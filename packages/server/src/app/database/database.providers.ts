import RxDB from 'rxdb'
import * as memoryAdapter from 'pouchdb-adapter-memory'

const name = 'specsh'
const adapter = 'memory'

RxDB.plugin(memoryAdapter)

export const databaseProviders = [
  {
    provide: 'RxDBToken',
    useFactory: async () => {
      return RxDB.create({
        name,
        adapter
      })
    }
  }
]
