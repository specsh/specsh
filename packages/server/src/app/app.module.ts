import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'
import { LoggerMiddleware } from './common/middlewares/logger.middleware'
import { DocumentsModule } from './modules/documents'
import { DocumentsController } from './modules/documents/documents.controller'

@Module({
  imports: [
    DocumentsModule
  ]
})
export class ApplicationModule implements NestModule {
  configure (consumer: MiddlewareConsumer): void {
  consumer
    .apply(LoggerMiddleware)
    // .with('ApplicationModule')
    .forRoutes(DocumentsController)
  }
}
