# Spec.sh document server

Server for [Spec.sh](https://spec.sh).

This is the document server for spec.sh

This is meant to be a generic REST service to store schema based documents.

The documents will be saved in a non relational manner.

For each document saved there is a public schema available.

These schemas will be served by the schema service.

Which is just a repository of available and released schemas.

These schema's are core to the spec.sh system.

They will be used for validation, but also the frontend will generate forms based on these schemas.

Each schema therefor also can be accompanied with form `layout`'s.
These layouts describe how a document should be represented while in edit or create mode.

The same system can be used to present generic widgets for presentation.
Although the presentations are not a form, there is nothing stopping us from using
the rendering systems to render tables e.g. and as soon as we do want to process some
input, all functionality will be available.

All these will be defined by layouts. How the custom widgets are loaded is still to be decided.

This whole schema + document + layout system is core to the whole system.
And should be preferred above any custom hand coding of forms and display widgets.
Any custom validation should also be prevented as much as possible.

The documents are saved using rxdb.

### Installation

```
$ yarn install
```

### Start

```
$ yarn start
```

### API

Put()
`http://localhost:3000/documents`: Put new document ({ type, contents })

Get()
`http://localhost:3000/documents`: Show All available documents 

Get()
`http://localhost:3000/documents/srd`: Show SRD

### Reference

[RxDB Node Example](https://github.com/pubkey/rxdb/tree/master/examples/node)
