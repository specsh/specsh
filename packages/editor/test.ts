const schema = require('../schemas/src/schemas/layout.schema.json')

// import { dotFlatten } from './src/app/schemas/util'

import { buildLayoutFromSchema } from 'angular2-json-schema-form'

console.log(buildLayoutFromSchema({
  schema,
  arrayMap: new Map()
}, null))
