'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">@specsh/editor documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-dc03d9afeb99306e56075acada26d082"' : 'data-target="#xs-components-links-module-AppModule-dc03d9afeb99306e56075acada26d082"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-dc03d9afeb99306e56075acada26d082"' :
                                            'id="xs-components-links-module-AppModule-dc03d9afeb99306e56075acada26d082"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageNotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CommitModule.html" data-type="entity-link">CommitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CommitModule-b1b3863efd4533752799e5d15698ba45"' : 'data-target="#xs-components-links-module-CommitModule-b1b3863efd4533752799e5d15698ba45"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CommitModule-b1b3863efd4533752799e5d15698ba45"' :
                                            'id="xs-components-links-module-CommitModule-b1b3863efd4533752799e5d15698ba45"' }>
                                            <li class="link">
                                                <a href="components/CommitComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CommitComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-ee7d075c254def3a92329c03351c666e"' : 'data-target="#xs-injectables-links-module-CoreModule-ee7d075c254def3a92329c03351c666e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-ee7d075c254def3a92329c03351c666e"' :
                                        'id="xs-injectables-links-module-CoreModule-ee7d075c254def3a92329c03351c666e"' }>
                                        <li class="link">
                                            <a href="injectables/AppApi.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AppApi</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LayoutService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LayoutService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LocalStorageService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LocalStorageService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/NotificationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/SchemaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>SchemaService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreStoreModule.html" data-type="entity-link">CoreStoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreStoreModule-e7cdd40dd21a23c4520f81ad3da1bc1a"' : 'data-target="#xs-injectables-links-module-CoreStoreModule-e7cdd40dd21a23c4520f81ad3da1bc1a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreStoreModule-e7cdd40dd21a23c4520f81ad3da1bc1a"' :
                                        'id="xs-injectables-links-module-CoreStoreModule-e7cdd40dd21a23c4520f81ad3da1bc1a"' }>
                                        <li class="link">
                                            <a href="injectables/UserProfileActions.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserProfileActions</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DebugModule.html" data-type="entity-link">DebugModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' : 'data-target="#xs-components-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' :
                                            'id="xs-components-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' }>
                                            <li class="link">
                                                <a href="components/DebugComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DebugComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' : 'data-target="#xs-injectables-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' :
                                        'id="xs-injectables-links-module-DebugModule-40a717ece0c6045721c596c050f9391e"' }>
                                        <li class="link">
                                            <a href="injectables/DebugService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DebugService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DebugRoutingModule.html" data-type="entity-link">DebugRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EditorModule.html" data-type="entity-link">EditorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' : 'data-target="#xs-components-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' :
                                            'id="xs-components-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DocumentComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DocumentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EditorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' : 'data-target="#xs-directives-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' :
                                        'id="xs-directives-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                        <li class="link">
                                            <a href="directives/AceEditorDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">AceEditorDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' : 'data-target="#xs-pipes-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' :
                                            'id="xs-pipes-links-module-EditorModule-b5a650304fe05f32572960b1c04bcaf9"' }>
                                            <li class="link">
                                                <a href="pipes/MarkedPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MarkedPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FooterModule.html" data-type="entity-link">FooterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FooterModule-9bdd95e3d534a64b89338eccb9d50544"' : 'data-target="#xs-components-links-module-FooterModule-9bdd95e3d534a64b89338eccb9d50544"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FooterModule-9bdd95e3d534a64b89338eccb9d50544"' :
                                            'id="xs-components-links-module-FooterModule-9bdd95e3d534a64b89338eccb9d50544"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FooterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link">MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NavbarModule.html" data-type="entity-link">NavbarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NavbarModule-d1018de65b6e5fbe9898b5365b45dd5f"' : 'data-target="#xs-components-links-module-NavbarModule-d1018de65b6e5fbe9898b5365b45dd5f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NavbarModule-d1018de65b6e5fbe9898b5365b45dd5f"' :
                                            'id="xs-components-links-module-NavbarModule-d1018de65b6e5fbe9898b5365b45dd5f"' }>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SchemasModule.html" data-type="entity-link">SchemasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SchemasModule-4b40d07c1df72dd10abaf0f5233e6932"' : 'data-target="#xs-components-links-module-SchemasModule-4b40d07c1df72dd10abaf0f5233e6932"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SchemasModule-4b40d07c1df72dd10abaf0f5233e6932"' :
                                            'id="xs-components-links-module-SchemasModule-4b40d07c1df72dd10abaf0f5233e6932"' }>
                                            <li class="link">
                                                <a href="components/SchemaFormLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemaFormLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SchemaLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemaLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SchemasComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SchemasFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemasFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SchemasListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemasListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SchemasNavigationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SchemasNavigationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SchemasRoutingModule.html" data-type="entity-link">SchemasRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SchemasRoutingModule.html" data-type="entity-link">SchemasRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SidebarModule.html" data-type="entity-link">SidebarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SidebarModule-78f0cf3a40966a3ecaf0ebf8cdfddfd9"' : 'data-target="#xs-components-links-module-SidebarModule-78f0cf3a40966a3ecaf0ebf8cdfddfd9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SidebarModule-78f0cf3a40966a3ecaf0ebf8cdfddfd9"' :
                                            'id="xs-components-links-module-SidebarModule-78f0cf3a40966a3ecaf0ebf8cdfddfd9"' }>
                                            <li class="link">
                                                <a href="components/SidebarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SidebarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserMenuModule.html" data-type="entity-link">UserMenuModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserMenuModule-8c22a519c90204f35e31062b38fdf6a0"' : 'data-target="#xs-components-links-module-UserMenuModule-8c22a519c90204f35e31062b38fdf6a0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserMenuModule-8c22a519c90204f35e31062b38fdf6a0"' :
                                            'id="xs-components-links-module-UserMenuModule-8c22a519c90204f35e31062b38fdf6a0"' }>
                                            <li class="link">
                                                <a href="components/ThemesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ThemesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserMenuComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserMenuComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link">UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' : 'data-target="#xs-components-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' :
                                            'id="xs-components-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostPasswordComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LostPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SocialComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SocialComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' : 'data-target="#xs-injectables-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' :
                                        'id="xs-injectables-links-module-UserModule-c9e5008fd23627e82bac882783d172cd"' }>
                                        <li class="link">
                                            <a href="injectables/AuthProvider.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthProvider</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserRoutingModule.html" data-type="entity-link">UserRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/DocumentsComponent.html" data-type="entity-link">DocumentsComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DocumentsListComponent.html" data-type="entity-link">DocumentsListComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/NavigationComponent.html" data-type="entity-link">NavigationComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/ActionTypes.html" data-type="entity-link">ActionTypes</a>
                            </li>
                            <li class="link">
                                <a href="classes/AddSchemaAction.html" data-type="entity-link">AddSchemaAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/AppInitAction.html" data-type="entity-link">AppInitAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuthAuthenticatedAction.html" data-type="entity-link">AuthAuthenticatedAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuthAuthenticationRedirectAction.html" data-type="entity-link">AuthAuthenticationRedirectAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuthDeauthenticateAction.html" data-type="entity-link">AuthDeauthenticateAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/Back.html" data-type="entity-link">Back</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChangeThemeAction.html" data-type="entity-link">ChangeThemeAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CheckVersion.html" data-type="entity-link">CheckVersion</a>
                            </li>
                            <li class="link">
                                <a href="classes/CollapseSidebar.html" data-type="entity-link">CollapseSidebar</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateSchemaAction.html" data-type="entity-link">CreateSchemaAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudAddAction.html" data-type="entity-link">CrudAddAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudCreateAction.html" data-type="entity-link">CrudCreateAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudDeleteAction.html" data-type="entity-link">CrudDeleteAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudEditAction.html" data-type="entity-link">CrudEditAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudListAction.html" data-type="entity-link">CrudListAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudRemoveAction.html" data-type="entity-link">CrudRemoveAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudUpdateAction.html" data-type="entity-link">CrudUpdateAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/DocumentCommitAction.html" data-type="entity-link">DocumentCommitAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/DocumentsActions.html" data-type="entity-link">DocumentsActions</a>
                            </li>
                            <li class="link">
                                <a href="classes/DocumentUpdateAction.html" data-type="entity-link">DocumentUpdateAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/ErrorNotificationAction.html" data-type="entity-link">ErrorNotificationAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/ExpandSidebar.html" data-type="entity-link">ExpandSidebar</a>
                            </li>
                            <li class="link">
                                <a href="classes/Forward.html" data-type="entity-link">Forward</a>
                            </li>
                            <li class="link">
                                <a href="classes/Go.html" data-type="entity-link">Go</a>
                            </li>
                            <li class="link">
                                <a href="classes/InfoNotificationAction.html" data-type="entity-link">InfoNotificationAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoadDocumentAction.html" data-type="entity-link">LoadDocumentAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoadSchemasAction.html" data-type="entity-link">LoadSchemasAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoadSchemasFailAction.html" data-type="entity-link">LoadSchemasFailAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoadSchemasSuccessAction.html" data-type="entity-link">LoadSchemasSuccessAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoginCredentials.html" data-type="entity-link">LoginCredentials</a>
                            </li>
                            <li class="link">
                                <a href="classes/NavigationSerializer.html" data-type="entity-link">NavigationSerializer</a>
                            </li>
                            <li class="link">
                                <a href="classes/NotificationAction.html" data-type="entity-link">NotificationAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/ReceivedAppVersion.html" data-type="entity-link">ReceivedAppVersion</a>
                            </li>
                            <li class="link">
                                <a href="classes/RemoveDocumentAction.html" data-type="entity-link">RemoveDocumentAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/RemoveSchemaAction.html" data-type="entity-link">RemoveSchemaAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/RemoveSchemaFailAction.html" data-type="entity-link">RemoveSchemaFailAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/RemoveSchemaSuccessAction.html" data-type="entity-link">RemoveSchemaSuccessAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemaChangeAction.html" data-type="entity-link">SchemaChangeAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemaEditAction.html" data-type="entity-link">SchemaEditAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemaEditFormLayoutAction.html" data-type="entity-link">SchemaEditFormLayoutAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemaRemovedAction.html" data-type="entity-link">SchemaRemovedAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemasPopulateAction.html" data-type="entity-link">SchemasPopulateAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SchemaUpdatedAction.html" data-type="entity-link">SchemaUpdatedAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SelectDocumentAction.html" data-type="entity-link">SelectDocumentAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/SuccessNotificationAction.html" data-type="entity-link">SuccessNotificationAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/ThemeChange.html" data-type="entity-link">ThemeChange</a>
                            </li>
                            <li class="link">
                                <a href="classes/ToggleSidebar.html" data-type="entity-link">ToggleSidebar</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateAppVersion.html" data-type="entity-link">UpdateAppVersion</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateSchemaAction.html" data-type="entity-link">UpdateSchemaAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserLoginAction.html" data-type="entity-link">UserLoginAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserLogoutAction.html" data-type="entity-link">UserLogoutAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSignin.html" data-type="entity-link">UserSignin</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSignInAction.html" data-type="entity-link">UserSignInAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSigninStart.html" data-type="entity-link">UserSigninStart</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSigninSuccess.html" data-type="entity-link">UserSigninSuccess</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSignout.html" data-type="entity-link">UserSignout</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSignoutSuccess.html" data-type="entity-link">UserSignoutSuccess</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserSignUpAction.html" data-type="entity-link">UserSignUpAction</a>
                            </li>
                            <li class="link">
                                <a href="classes/WarningNotificationAction.html" data-type="entity-link">WarningNotificationAction</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppActions.html" data-type="entity-link">AppActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppActions-1.html" data-type="entity-link">AppActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppApi.html" data-type="entity-link">AppApi</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppEffects.html" data-type="entity-link">AppEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppSettingsEffects.html" data-type="entity-link">AppSettingsEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthActions.html" data-type="entity-link">AuthActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthEffects.html" data-type="entity-link">AuthEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthProvider-1.html" data-type="entity-link">AuthProvider</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigService.html" data-type="entity-link">ConfigService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DocumentService.html" data-type="entity-link">DocumentService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ItemGuard.html" data-type="entity-link">ItemGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JsonPointer.html" data-type="entity-link">JsonPointer</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayoutService.html" data-type="entity-link">LayoutService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LocalStorageService.html" data-type="entity-link">LocalStorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationsActions.html" data-type="entity-link">NotificationsActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationsEffects.html" data-type="entity-link">NotificationsEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationService.html" data-type="entity-link">NotificationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RouterEffects.html" data-type="entity-link">RouterEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SchemasActions.html" data-type="entity-link">SchemasActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SchemasEffects.html" data-type="entity-link">SchemasEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SchemaService.html" data-type="entity-link">SchemaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ThemesActions.html" data-type="entity-link">ThemesActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserActions.html" data-type="entity-link">UserActions</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserEffects.html" data-type="entity-link">UserEffects</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserProfileActions.html" data-type="entity-link">UserProfileActions</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/AuthInterceptor.html" data-type="entity-link">AuthInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/ErrorInterceptor.html" data-type="entity-link">ErrorInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/ResolveApp.html" data-type="entity-link">ResolveApp</a>
                            </li>
                            <li class="link">
                                <a href="guards/ResolveSchemas.html" data-type="entity-link">ResolveSchemas</a>
                            </li>
                            <li class="link">
                                <a href="guards/SchemaCollectionGuard.html" data-type="entity-link">SchemaCollectionGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Action.html" data-type="entity-link">Action</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthProviderInterface.html" data-type="entity-link">AuthProviderInterface</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DocumentsState.html" data-type="entity-link">DocumentsState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GoogleBasicProfile.html" data-type="entity-link">GoogleBasicProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IAppSettings.html" data-type="entity-link">IAppSettings</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IAppVersion.html" data-type="entity-link">IAppVersion</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IAuthState.html" data-type="entity-link">IAuthState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDocument.html" data-type="entity-link">IDocument</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/INotification.html" data-type="entity-link">INotification</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISchemaState.html" data-type="entity-link">ISchemaState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IUserProfile.html" data-type="entity-link">IUserProfile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/JsonSchemaFormInterface.html" data-type="entity-link">JsonSchemaFormInterface</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Layouts.html" data-type="entity-link">Layouts</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PlainObject.html" data-type="entity-link">PlainObject</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RedirectPayload.html" data-type="entity-link">RedirectPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RouterStateUrl.html" data-type="entity-link">RouterStateUrl</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Schema.html" data-type="entity-link">Schema</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SchemaForm.html" data-type="entity-link">SchemaForm</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SchemaMenuItem.html" data-type="entity-link">SchemaMenuItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Schemas.html" data-type="entity-link">Schemas</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SchemasState.html" data-type="entity-link">SchemasState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SpecshState.html" data-type="entity-link">SpecshState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UnsafeAction.html" data-type="entity-link">UnsafeAction</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});