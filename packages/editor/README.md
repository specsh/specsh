# Spec.sh Editor

## General Instructions

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Tech used

### Compodoc

See:
 - [Compodoc](https://compodoc.github.io/website/)
 - [Compodoc Edmo Todomvc Angular](https://github.com/compodoc/compodoc-demo-todomvc-angular)


### Rxjs

See [rxmarbles](http://rxmarbles.com/) for a visualization explanation of the Rx Observables methods. 

## Styling

Styling is based on the [angular-ngrx-material-starter](https://github.com/tomastrajan/angular-ngrx-material-starter).

Set of Material Icons is available here [Material Icons](https://material.io/icons/)

### Further help

This project was initially generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Some setup ideas were taken from [echoes-player](https://github.com/orizens/echoes-player)

Also: [ngx-model-hacker-news-example](https://github.com/tomastrajan/ngx-model-hacker-news-example)

General information about Angular:

[Rangle's Angular Training book](https://angular-2-training-book.rangle.io/)

[Devarchy Angular](https://devarchy.com/angular)

[Augury](http://augury.angular.io/)

[Codelyzer](http://codelyzer.com/)

[Angular resources](https://angular.io/resources)

[Ninja Code Gen](https://app.ninjacodegen.com/codeGen/ngx-md) (My kind of app)

[Apollo](https://www.apollographql.com/docs/angular)

[AngularCommerce](https://github.com/NodeArt/angular-commerce)

[PrimeNG](https://www.primefaces.org/primeng/#/)

## Design examples

[Firebase Console](https://console.firebase.google.com/project/specsh-6aeb0/overview)

### Multiple

Multiple vs Single instances.

It is possible to mark during schema creation whether instances of the schema should be
considered to be singular or a collection.
In reality all schema's will have their own collection, but the `collection` flag will indicate
whether the instance should be treated as a collection within the application or not.
Benefit is also, it becomes just a matter of switching the flag whether this is the case.

Right now the system was designed to only hold one instance of a schema.
Which is rather limited ofcourse.

However both flavors should be possible. If something is singular, things
like drop downs to switch between items will not be shown.
Also there will be no overview / table lists of items. Because there will be just one item.

This can be consulted within the application by looking at the schema's collection flag.
Thus resulting in different work flows for this item.

Each logged in user has it's own database and it's own collection.
Which makes everything scoped to the user.

The backend itself always works with collections, it could also read
the collection flag and just refuse to insert a second item if the type is not a collection.

You will have to group by that distinction and be prepared for it everywhere.

Collection includes singular.

Singular lacks collection.
Which basically means, singular skips over collection everywhere.
And collections is an extra layer on top, or inbetween.

Which also means, the collection flag is an applicaton context.
But on some component level it will need all schema info and
render items differently based on whether they are singular or not.



https://scotch.io/tutorials/easy-node-authentication-setup-and-local

## 

Want to expand on the concept of ....
In browser creation.
Save templates, save scripts, save schemas, save form schemas,
load data source, save layouts.

Use templates, use scripts, use schemas, use form schemas,
use data sources, use layouts.

All schema first approach, new entity new schema.
Thus inverted how most works today.
Also crud out of the box for everything (already solved with documents)

{
  "title": "Markdown Template",
 "properties": {
   "name": { "type": "string", "title": "Name" },
  "contents": {"type": "string", "title": "Contents"}
} 
}

### Angular Bootstrap

https://blog.angularindepth.com/hooking-into-the-angular-bootstrap-process-36e82a01fba8

### For routing

Only thing that works and is simple:
https://plnkr.co/edit/6hkmRkGAIj9X0IA3oWVY?p=preview
https://stackoverflow.com/questions/46427833/using-nested-router-outlets-in-angular-4

### Angular Layout

Good step by step for angular layout:
https://github.com/EladBezalel/material2-start
