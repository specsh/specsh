# Themes

These are the themes available for spec.sh.

Styling is based on the [angular-ngrx-material-starter](https://github.com/tomastrajan/angular-ngrx-material-starter).

Also see this blog post: [The complete guide to angular material themes](https://medium.com/@tomastrajan/the-complete-guide-to-angular-material-themes-4d165a9d24d1)

Set of Material Icons is available here [Material Icons](https://material.io/icons/)

Also: [Custom Themes with Angular Material](https://blog.thoughtram.io/angular/2017/05/23/custom-themes-with-angular-material.html)

Material docs: [Theming](https://github.com/angular/material2/blob/master/guides/theming.md)
