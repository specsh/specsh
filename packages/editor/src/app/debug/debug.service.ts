import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'

import { environment } from '../../environments/environment'

@Injectable()
export class DebugService {
  constructor (
    private http: HttpClient
  ) { }

  public ping () {
    return this.http.get<any[]>(`${environment.api.url}/documents/ping`)
  }
}
