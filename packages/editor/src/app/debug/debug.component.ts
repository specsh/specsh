import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { DebugService } from './debug.service'

@Component({
  selector: 'specsh-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DebugComponent implements OnInit {
  constructor (
    private debugService: DebugService
  ) { }

  public ngOnInit () {
  }

  public ping () {
    this.debugService
      .ping()
      .subscribe((res) => {
        console.log(res)
      })
  }
}
