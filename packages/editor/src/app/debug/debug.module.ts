import { CommonModule } from '@angular/common'
import { HttpClient } from '@angular/common/http'
import { NgModule } from '@angular/core'

import { MaterialModule } from '../vendor/material'
import { DebugRoutingModule } from './debug-routing.module'
import { DebugComponent } from './debug.component'
import { DebugService } from './debug.service'

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    DebugRoutingModule
  ],
  declarations: [
    DebugComponent
  ],
  providers: [
    HttpClient,
    DebugService
  ]
})
export class DebugModule { }
