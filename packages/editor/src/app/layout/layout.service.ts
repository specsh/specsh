import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { ThemeChange } from '../app-layout/index'
import { SpecshState } from '../core/store/reducers'

@Injectable()
export class LayoutService {
  constructor (
    private store: Store<SpecshState>
  ) { }

  public changeTheme (name: string) {
    this.store.dispatch(new ThemeChange(name))
  }
}
