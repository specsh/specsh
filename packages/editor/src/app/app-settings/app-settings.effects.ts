import { Injectable } from '@angular/core'
import { Actions } from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'

@Injectable()
export class AppSettingsEffects {
  constructor (
    public actions$: Actions,
    public store: Store<SpecshState>
    // public versionCheckerService: VersionCheckerService
  ) { }

  /*
  @Effect({ dispatch: false })
  updateAppVersion$ = this.actions$
    .ofType(AppLayout.ActionTypes.APP_UPDATE_VERSION)
    .map(() => this.versionCheckerService.updateVersion())

  @Effect({ dispatch: false })
  checkForNewAppVersion$ = this.actions$
    .ofType(AppLayout.ActionTypes.APP_CHECK_VERSION)
    .map(() => this.versionCheckerService.checkForVersion())
  */
}
