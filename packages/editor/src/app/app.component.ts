import {
  Component,
  HostBinding,
  OnInit
} from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { getAppTheme, ThemeChange } from './app-layout'
import { AppApi } from './app.api'
import { AuthService } from './auth/auth.service'
import { SpecshState } from './core/store/reducers'
import { SchemaService } from './schemas/schemas.service'
import { UserLogoutAction, UserSignInAction, UserSignUpAction } from './user/user.actions'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'app'
  public events = []
  public opened: true

  @HostBinding('class')
  public componentCssClass

  // menu$: Observable <SchemaMenuItem[]>
  public menu: any[]

  public isAuthenticated = false

  constructor (
    private store: Store<SpecshState>,
    private api: AppApi,
    private route: ActivatedRoute,
    private router: Router,
    private schemaService: SchemaService,
    private authService: AuthService
  ) {}

  public themeChange (theme) {
    this.store.dispatch(new ThemeChange(theme))
  }

  public signUp () {
    this.store.dispatch(new UserSignUpAction())
  }

  public signIn () {
    this.store.dispatch(new UserSignInAction())
  }

  public logout () {
    this.store.dispatch(new UserLogoutAction())
  }

  public ngOnInit () {
    this.isAuthenticated = this.authService.isAuthenticated()
    this.themeChange = this.themeChange.bind(this)
    this.signUp = this.signUp.bind(this)
    this.signIn = this.signIn.bind(this)
    this.logout = this.logout.bind(this)

    this.store.select(getAppTheme)
      .subscribe((theme) => this.componentCssClass = theme + '-theme')
    /*
    this.store.select(selectorIsAuthenticated)
      .subscribe((authenticated) => this.isAuthenticated = authenticated)
    */
  }
}
