# App directory

These directory contains the main source code of the Spec.sh editor.

---
A cleanup is very much needed and there should be a clear structure.
At the moment it is messy because I've searched for many different
best practises.

What I will do now is make sure I will follow one path.
I even notice I have created two document.services's


---

Following is a description of the setup:

###

### core

### editor

### footer

### navbar

### sidebar

### vendor

Vendor contains all external module setup.
This is done to remove clutter from the app itself.

Many of the vendor modules require very specific configuration and often changes over time.

### References

Important reading:
https://blog.angular-university.io/angular2-ngmodule/
https://blog.angular-university.io/angular2-router/

