import { HttpClient, HttpErrorResponse } from '@angular/common/http'

import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { get } from 'lodash'
import { of } from 'rxjs'
import { catchError, take } from 'rxjs/operators'
import { environment } from '../../environments/environment'
import { SpecshState } from '../core/store/reducers'
import { ErrorNotificationAction, InfoNotificationAction } from '../notifications/index'
import { SchemaService } from '../schemas/schemas.service'
import { getDocument } from './documents.selectors'

@Injectable()
export class DocumentService {
  private documents = {}

  constructor (
    private http: HttpClient,
    private schemaService: SchemaService,
    private store: Store<SpecshState>
  ) {}

  public getDocument (type: string) {
    return this.http.get(`${environment.api.url}/documents/${type}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error(error) // notification service
          return of(undefined)
        })
      )
  }

  // TODO: this validation should be much earlier.
  // but after submission / commit
  public commit () {
    return this.store
      .select(getDocument('srd'))
      .pipe(take(1))
      .subscribe((document) => {
        if (this.schemaService.validate('srd', document)) {
          this.saveDocument('srd', document)
          this.store.dispatch(new InfoNotificationAction({
            title: 'Document saved',
            message: 'Document commited succesfully'
          }))
        } else {
          this.store.dispatch(new ErrorNotificationAction({
            title: 'Failed to save document',
            message: this.schemaService.getValidationErrors()
          }
          ))
        }
      })
  }

  public saveDocument (
    type: string,
    contents: string
  ) {
    return this.http.put(
      `${environment.api.url}/documents/${type}`, contents
    ).subscribe((result) => {
      console.log(result)
    })
  }

  public getDocumentSegment (name: string, selector: string) {
    return this.selectSegment(this.getDocument(name), selector)
  }

  public selectSegment (document, selector: string) {
    let newDocument

    if (selector) {
      newDocument = {
        definitions: document.definitions,
        ...get(document, selector)
      }
    } else {
      newDocument = {
        definitions: document.definitions,
        ...document
      }
    }

    return newDocument
  }
}
