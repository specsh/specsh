import { Component, Input, OnInit } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { SpecshState } from '../../core/store/reducers'
import { RemoveDocumentAction } from '../documents.actions'
import { getDocuments } from '../documents.selectors'

@Component({
  selector: 'documents-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class DocumentsListComponent implements OnInit {
  @Input() public model
  public schemas$: Observable<Document[]>

  constructor (
    private store: Store<SpecshState>
  ) {}

  public ngOnInit () {
    this.schemas$ = this.store.pipe(select(getDocuments))
  }

  public remove (schemaName: string, documentId: string) {
    this.store.dispatch(new RemoveDocumentAction(schemaName, documentId))
  }
}
