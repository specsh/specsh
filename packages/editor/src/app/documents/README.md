# Documents

Documents are the instances of the schemas which are defined.

This will be a simple crud for these schemas and will pretty much behave
the same as adding schemas themselves.

When that is done it should also be possible to create singel instances and
render differently based on that. Thus not showing lists of the collection.
But directly being redirected to editing an instance.

- A list of documents
- Create a new document
- Edit a specific document
- Remove a specific document

Most functionality already exist in editor itself.

The guards of Schema can just be reused, so it is ensured the schemas
are available for any document.

What is the context of the application will be totally defined by which schemas
have been defined.

I still want to decide what to do next with this content.

The schema should facilitate something, it faciliates:

 - storage of schema instances
 - an api for these schema instances
 - creation of metadata for these schemas, e.g. layout definitions.
 - perhaps exposure through websockets for these instances.
 - validation of input
 
Because I abstract it so much, it becomes more difficult to determine what it is
I'm actually creating.

Seems also I'm moving away from the first intention of creating spec.sh
And now want it to be everything, for every single use case.



