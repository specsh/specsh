import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { SchemaService } from '../schemas/schemas.service'

@Component({
  selector: 'documents-component',
  template: ''
})
export class DocumentsComponent implements OnInit {
  public menu = []

  constructor (private schemaService: SchemaService,
               private route: ActivatedRoute,
               private store: Store<SpecshState>) {
  }

  public ngOnInit () {

    // documents controller is not really needed.
    // but it's nice to have an entry component which always runs.

    /*
    const document = this.route.snapshot.paramMap.get('document')

    this.store.select(getSchema(document))
      .subscribe((schema) => {
        this.menu = createSchemaMenu(schema.schema, document)
      })
    */
  }
}
