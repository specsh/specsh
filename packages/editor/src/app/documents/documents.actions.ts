import { IDocument } from './document.model'

// tslint:disable max-classes-per-file
export class DocumentsActions {
  public static COMMIT = '[Document] COMMIT'
  public static LOAD = '[Document] LOAD'
  public static REMOVE = '[Document] REMOVE'
  public static UPDATE = '[Document] UPDATE'
  public static SELECT_DOCUMENT = '[Document] SELECT'
}

export class SelectDocumentAction {
  public readonly type = DocumentsActions.SELECT_DOCUMENT
}

export class RemoveDocumentAction {
  public readonly type = DocumentsActions.REMOVE
  constructor (public schemaName: string, public documentId: string) { }
}

export class LoadDocumentAction {
  public readonly type = DocumentsActions.LOAD
}

export class DocumentUpdateAction {
  public readonly type = DocumentsActions.UPDATE
  constructor (public payload: IDocument) { }
}

export class DocumentCommitAction {
  public readonly type = DocumentsActions.COMMIT
}

export type Action =
  | LoadDocumentAction
  | RemoveDocumentAction
  | DocumentCommitAction
  | DocumentUpdateAction
