import { Action } from '../core/store/types'
// import { DocumentsActions} from './documents.actions'
import {
  DocumentsState,
  initialDocumentState
} from './documents.state'

export function documents (state = initialDocumentState, action: Action): DocumentsState {
  switch (action.type) {
    default:
      return state
  }
}
