export type DocumentType = 'urd' | 'srd'

export interface IDocument {
  type: 'urd' | 'srd',
  path: string[],
  contents: object
}
