import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from '../auth/auth.guard'
// import { SchemasComponent } from './schemas.component'
// import { SchemaFormLayoutComponent } from './layout/form/form.component'
// import { SchemaLayoutComponent } from './layout/layout.component'
import { DocumentsComponent } from './documents.component'
import { DocumentsListComponent } from './list/list.component'
// import { SchemasFormComponent } from './form/form.component'
// import { SchemaCollectionGuard } from './guards'

export const routes: Routes = [
  {
    path: '',
    component: DocumentsComponent,
    canActivate: [
      AuthGuard
      // SchemaCollectionGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: ':schemaName/overview',
        component: DocumentsListComponent
      }
      /*
      {
        path: ':schemaName/create',
        component: SchemasFormComponent
      },
      {
        path: ':schemaName/:documentId/edit',
        component: SchemasFormComponent
      }
      */
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchemasRoutingModule {}
