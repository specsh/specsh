/*
import { Action, State, StateContext } from '@ngxs/store'
import {
  DocumentsActions,
  DocumentUpdateAction
} from './documents.actions'
import { DocumentService } from './documents.service'

import { get, set } from 'lodash'
*/

/*
export interface IDocumentsState {
  urd: any,
  srd: any
}
*/

import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { IDocument } from './document.model'

/*
export interface IDocumentState {
  documents: any,
  layouts: any
}
*/

export interface DocumentsState extends EntityState<IDocument> {
  currentDocumentId: string | null
  loaded: boolean
  loading: boolean
}

export const documentAdapter: EntityAdapter<IDocument> = createEntityAdapter<IDocument>({
  selectId: (document: IDocument) => document.path.join(':'),
  sortComparer: false
})

export const initialDocumentState: DocumentsState = documentAdapter.getInitialState({
  currentDocumentId: null,
  loaded: false,
  loading: false
})

/*
@State<IDocumentsState>({
  name: 'documents',
  defaults: {
    urd: {},
    srd: {}
  }
})
export class DocumentsState {
  constructor (
    private documentsService: DocumentService
  ) { }

  @Action(DocumentsActions.COMMIT)
  public commitDocument(_ctx: StateContext<IDocumentsState>) {
    // const state = ctx.getState()

    this.documentsService.commit()
  }

  // ok now only saving to sub sections work.
  // not to sections...
  @Action(DocumentsActions.UPDATE)
  public updateDocument(ctx: StateContext<IDocumentsState>, action: DocumentUpdateAction) {
    const state = ctx.getState()

    const originalPortion = action.payload.path.length === 1 ?
      get(state[action.payload.type], action.payload.path.slice()) :
      get(state[action.payload.type], action.payload.path.slice(0, -1))

    const data = set(
      { ...state[action.payload.type] }, // the whole document state e.g. .srd
      action.payload.path.length === 1 ?
        action.payload.path.slice() :
        action.payload.path.slice(0, -1), // bit of a hack?; the path to set. e.g. GeneralDescription
      {
        ...originalPortion,
        ...action.payload.contents // the new contents
      }
    )

    ctx.setState({
      ...state,
      [action.payload.type]: data
    })
  }
}

*/
