import { createSelector } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'

// export const getDocuments = (state: SpecshState) => state.documents.entities
export const getDocuments = (state: SpecshState) => []

// ok here schema to typescript would be useful.
// perhaps I also fork that plugin, so I can more easily chop the schema apart.
export const getDocument = (name: string) => createSelector(getDocuments, (documents: any) => documents[name])
