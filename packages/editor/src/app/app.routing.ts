import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from './auth'
import { PageNotFoundComponent } from './not-found.component'
import { UserComponent } from './user/user.component'

export const routes: Routes = [
  {
    path: '',
    // redirectTo: 'schemas',
    // pathMatch: 'full',
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: 'schemas',
        // loadChildren: () => SchemasModule
        loadChildren: 'app/schemas/schemas.module#SchemasModule'
      },
      {
        path: 'user',
        component: UserComponent
      }
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
