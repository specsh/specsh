// TODO: Models should always be generated from the json schema.
export class User {
  public _id: string
  public username: string
  public password: string
  public firstName: string
  public lastName: string
}
