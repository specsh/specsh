import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../../core/store/reducers'
import { UserLoginAction, UserLogoutAction } from '../user.actions'

const schema = require('../schemas/schema.json')
const layout = require('../schemas/layouts/login.json')

@Component({
  moduleId: module.id,
  selector: 'specsh-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public model: any = {}
  public loading = true

  public formActive = true
  public formObject = {
    layout,
    schema
  }
  public formOptions = {}

  constructor (
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<SpecshState>
  ) { }

  public ngOnInit () {
    this.store.dispatch(new UserLogoutAction())
  }

  public login () {
    this.loading = true

    this.store.dispatch(new UserLoginAction({
      email: this.model.email,
      password: this.model.password
    }))
  }

  public onSubmit (event) {
    this.login()
  }

  public validationErrors (event) {
    console.log('validationErrors', event)
  }

  public onChanges (event) {
    console.log('onChanges', event)
  }

  public isValid (event) {
    console.log('isValid', event)

    return true
  }
}
