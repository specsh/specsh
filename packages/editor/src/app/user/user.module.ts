import { CommonModule } from '@angular/common'
import { HttpClient } from '@angular/common/http'
import { NgModule } from '@angular/core'
import {
  JsonSchemaFormModule,
  MaterialDesignFramework,
  MaterialDesignFrameworkModule
} from 'angular2-json-schema-form'
import { AuthGuard } from '../auth/auth.guard'
import { AuthService } from '../auth/auth.service'
import { AuthProvider } from '../auth/provider/auth.provider'
import { MaterialModule } from '../vendor/material'
import { LoginComponent } from './login/login.component'
import { LostPasswordComponent } from './lost-password/lost-password.component'
import { RegisterComponent } from './register/register.component'
import { UserService } from './services/user.service'
import { SocialComponent } from './shared/social/social.component'
import { TabsComponent } from './shared/tabs/tabs.component'
import { UserRoutingModule } from './user-routing.module'
import { UserComponent } from './user.component'

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MaterialDesignFrameworkModule,
    JsonSchemaFormModule.forRoot(MaterialDesignFramework),
    UserRoutingModule
  ],
  declarations: [
    UserComponent,
    LoginComponent,
    LostPasswordComponent,
    RegisterComponent,
    TabsComponent,
    SocialComponent
  ],
  providers: [HttpClient, AuthProvider, AuthService, AuthGuard, UserService]
})
export class UserModule {}
