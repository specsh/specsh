
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import {
  Actions,
  Effect,
  ofType
} from '@ngrx/effects'
import { tap } from 'rxjs/operators'
import { UserActions } from './user.actions'

@Injectable()
export class UserEffects {
  /**
   * Seems to be a bit too duplicate
   *
   * @type {Observable<Action>}
   */
  @Effect({ dispatch: false })
  public login$ = this.actions$
    .pipe(
      ofType(UserActions.LOGIN),
      tap(() => this.router.navigate(['user', 'login']))
    )

  /*
  @Effect({ dispatch: false })
  logout$ = this.actions$
    .ofType(UserActions.LOGOUT)
    .do(() => this.router.navigate(['user', 'logout']))
  */

  @Effect({ dispatch: false })
  public signup$ = this.actions$.pipe(
      ofType(UserActions.SIGNUP),
      tap(() => this.router.navigate(['user', 'register']))
    )

  @Effect({ dispatch: false })
  public signin$ = this.actions$.pipe(
      ofType(UserActions.SIGNIN),
      tap(() => this.router.navigate(['user', 'login']))
    )

  constructor (
    private actions$: Actions,
    private router: Router
  ) { }
}
