// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

@Injectable()
export class UserActions {
  public static LOGIN = '[User] LOGIN'
  public static LOGOUT = '[User] LOGOUT'
  public static SIGNIN = '[User] SIGNIN'
  public static SIGNUP = '[User] SIGNUP'
}

// wanted to do this with schema..
export class LoginCredentials {
  public email: string
  public password: string
}

export class UserLoginAction implements Action {
  public readonly type = UserActions.LOGIN
  constructor (public payload: LoginCredentials) {}
}

export class UserSignUpAction implements Action {
  public readonly type = UserActions.SIGNUP
  constructor () {}
}

export class UserSignInAction implements Action {
  public readonly type = UserActions.SIGNIN
  constructor () {}
}

export class UserLogoutAction implements Action {
  public readonly type = UserActions.LOGOUT
}

export type Action =
  | UserLoginAction
  | UserLogoutAction
  | UserSignInAction
  | UserSignUpAction
