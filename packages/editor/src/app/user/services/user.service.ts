import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment'
import { User } from '../models/index'

@Injectable()
export class UserService {
  constructor (
    private http: HttpClient
  ) { }

  public getAll () {
    return this.http.get<User[]>(environment.api.url + '/user')
  }

  public getById (_id: string) {
    return this.http.get(environment.api.url + '/user/' + _id)
  }

  public create (user: User) {
    return this.http.post(environment.api.url + '/user/register', user)
  }

  public update (user: User) {
    return this.http.put(environment.api.url + '/user/' + user._id, user)
  }

  public delete (_id: string) {
    return this.http.delete(environment.api.url + '/user/' + _id)
  }
}
