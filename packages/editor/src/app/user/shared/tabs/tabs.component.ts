import { Component } from '@angular/core'

@Component({
  selector: 'specsh-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent {
  public navLinks = [
    {
      label: 'Login',
      path: '/user/login'
    },
    {
      label: 'Register',
      path: '/user/register'
    }
  ]
}
