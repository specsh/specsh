import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { NotificationService } from '../../notifications/notifications.service'
import { UserService } from '../services/user.service'

const schema = require('../schemas/schema.json')
const layout = require('../schemas/layouts/register.json')

@Component({
  moduleId: module.id,
  selector: 'specsh-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public model: any = {}
  public loading = true
  public formActive = true
  public formObject = {
    layout,
    schema
  }
  public formOptions = {}

  constructor (
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  public register () {
    this.loading = true
    // should first dispatch an action.
    this.userService.create(this.model)
      .subscribe(
        data => {
          this.notificationService.success('Registration successful')
          this.router.navigate(['/login'])
        },
        error => {
          this.notificationService.error(error)
          this.loading = false
        })
  }

  public onSubmit (event) {
    console.log('onSubmit', event, this.model)
    this.register()
  }

  public validationErrors (event) {
    console.log('validationErrors', event)
  }

  public onChanges (event) {
    console.log('onChanges', event)
  }

  public isValid (event) {
    console.log('isValid', event)

    return true
  }
}
