import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import {
  Observable,
  throwError as observableThrowError
} from 'rxjs'
import 'rxjs/add/operator/catch'

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  public intercept (request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // extract error message from http body if an error occurs
    return next.handle(request).catch(errorResponse => {
      return observableThrowError(errorResponse.error)
    })
  }
}

export const ErrorInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: ErrorInterceptor,
  multi: true
}
