import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import * as AppLayout from './app-layout/index'
import { SpecshState } from './core/store/reducers'
import * as RouterActions from './core/store/router-store/index'
import * as SchemasActions from './schemas/index'
import * as UserActions from './user-profile/index'

@Injectable()
export class AppApi {
  public themes$ = this.store.select(AppLayout.getAppThemes)
  public appVersion$ = this.store.select(AppLayout.getAppVersion)
  public user$ = this.store.select(UserActions.getUser)

  constructor (
    private store: Store<SpecshState>
  ) { }

  /**
   * Initialize app for a user.
   *
   * Basically load all needed state.
   *
   * Neh better er view.
   *
   *
   */
  public init () {
    this.store.dispatch(new AppLayout.ToggleSidebar())
  }

  public toggleSidebar () {
    this.store.dispatch(new AppLayout.ToggleSidebar())
  }

  public navigateBack () {
    this.store.dispatch(new RouterActions.Back())
  }

  public updateVersion () {
    this.store.dispatch(new AppLayout.UpdateAppVersion())
  }

  public checkVersion () {
    this.store.dispatch(new AppLayout.CheckVersion())
  }

  public changeTheme (theme: string) {
    this.store.dispatch(new AppLayout.ThemeChange(theme))
  }

  public notifyNewVersion (_response) {
    // this.store.dispatch(new AppLayout.RecievedAppVersion(response))
  }

  public recievedNewVersion (_response) {
    // this.store.dispatch(new AppLayout.RecievedAppVersion(response))
  }

  // AUTHORIZATION
  public signinUser () {
    this.store.dispatch(new UserActions.UserSignin())
  }

  public signoutUser () {
    this.store.dispatch(new UserActions.UserSignout())
  }

  // SCHEMAS
  public loadSchemas () {
    this.store.dispatch(new SchemasActions.LoadSchemasAction())
  }
}
