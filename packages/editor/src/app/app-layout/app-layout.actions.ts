// tslint:disable max-classes-per-file
import { Action } from '@ngrx/store'

export class ActionTypes {
  public static SIDEBAR_EXPAND = '[APP LAYOUT] SIDEBAR_EXPAND'
  public static SIDEBAR_COLLAPSE = '[APP LAYOUT] SIDEBAR_COLLAPSE'
  public static SIDEBAR_TOGGLE = '[APP LAYOUT] SIDEBAR_TOGGLE'

  public static APP_VERSION_RECEIVED = '[APP] APP_VERSION_RECEIVED'
  public static APP_UPDATE_VERSION = '[APP] APP_UPDATE_VERSION'
  public static APP_CHECK_VERSION = '[APP] APP_CHECK_VERSION'

  public static APP_THEME_CHANGE = '[App Theme] APP_THEME_CHANGE'
}
export class ReceivedAppVersion implements Action {
  public readonly type = ActionTypes.APP_VERSION_RECEIVED
  constructor (public payload: any) { }
}
export class UpdateAppVersion implements Action {
  public readonly type = ActionTypes.APP_UPDATE_VERSION
  public payload = ''
}
export class CheckVersion implements Action {
  public readonly type = ActionTypes.APP_CHECK_VERSION
  public payload = ''
}
export class ExpandSidebar implements Action {
  public readonly type = ActionTypes.SIDEBAR_EXPAND
  public payload = true
}

export class CollapseSidebar implements Action {
  public readonly type = ActionTypes.SIDEBAR_COLLAPSE
  public payload = false
}

export class ToggleSidebar implements Action {
  public type = ActionTypes.SIDEBAR_TOGGLE
  public payload = ''
}

export class ThemeChange implements Action {
  public type = ActionTypes.APP_THEME_CHANGE
  constructor (public payload: string) { }
}

export type ActionsUnion =
  | ReceivedAppVersion
  | UpdateAppVersion
  | CheckVersion
  | ExpandSidebar
  | CollapseSidebar
  | ToggleSidebar
  | ThemeChange
