export const Themes = [
  'light',
  'black',
  'default',
  'rhalff'
]

export const DEFAULT_THEME = Themes[0]
