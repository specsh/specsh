import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations'
import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ChangeDetectionStrategy
} from '@angular/core'
import { MatMenuTrigger } from '@angular/material'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { get } from 'lodash'
import * as tpl from 'mustache'
import { Observable } from 'rxjs'
import { take } from 'rxjs/operators'
import { SpecshState } from '../core/store/reducers'
import {
  DocumentUpdateAction,
  getDocument
  // IDocument
} from '../documents'
import { DocumentType } from '../documents/document.model'
import { getSchemaMenu } from '../schemas'
import { SchemaService } from '../schemas/schemas.service'
import { JsonPointer } from './shared'

@Component({
  selector: 'formed',
  templateUrl: 'editor.component.html',
  styleUrls: ['./editor.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('expandSection', [
      state('in', style({ height: '*' })),
      transition(':enter', [style({ height: 0 }), animate(100)]),
      transition(':leave', [
        style({ height: '*' }),
        animate(100, style({ height: 0 }))
      ])
    ])
  ]
})
export class EditorComponent implements OnInit, OnDestroy {
  public selector: string[]
  public selectedFramework = 'material-design'
  public visible: { [item: string]: boolean } = {
    options: false,
    schema: false,
    form: true,
    output: false
  }

  public markdownPreview: string

  public formActive = false
  public jsonFormValid = false
  public jsonFormStatusMessage = 'Loading form...'
  public jsonFormObject: any
  public jsonFormOptions: any = {
    addSubmit: true, // Add a submit button if layout does not have one
    debug: false, // Don't show inline debugging information
    // disabled: see if it did anything extra though..
    loadExternalAssets: false, // Load external css and JavaScript for frameworks
    returnEmptyFields: false, // Don't return values for empty input fields
    setSchemaDefaults: true, // Always use schema defaults for empty fields
    defaultWidgetOptions: { feedback: true } // Show inline feedback icons
  }
  public liveFormData: any = {}
  public formValidationErrors: any
  public formIsValid = null
  public submittedFormData: any = null

  public document: DocumentType = 'srd'
  public documentTitle: string
  public documents = ['srd', 'urd']
  public section: string
  public subSection: string
  public menu$: Observable<any>

  // public document$: Observable<IDocument>

  @ViewChild(MatMenuTrigger, { static: false })
  public menuTrigger: MatMenuTrigger

  constructor (
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<SpecshState>,
    private schemaService: SchemaService
  ) {}

  public ngOnInit () {
    // great but app has no notion of schemaName in the url
    //  this.route.snapshot.get('schemaName')
    // let's just not use that menu on the top level.
    // should be in-module

    this.route.params.subscribe(params => {
      console.log('also params for editor')
      const selector = []
      const { document, section, subSection } = params

      console.log('duplicate calls or not?')

      this.document = params.document

      this.menu$ = this.store.select(getSchemaMenu(this.document))

      if (section) {
        this.section = section
        selector.push(this.section)
      }
      if (subSection) {
        this.subSection = subSection
        selector.push(this.subSection)
      }

      this.selector = selector
      this.documentTitle = this.schemaService.getSchema(this.document).title
      this.loadDocument(this.document)
    })
  }

  public loadDocument (document: DocumentType) {
    /*
    if (this.menuTrigger.menuOpen) {
      this.menuTrigger.closeMenu()
    }
    */
    if (document !== this.document) {
      this.formActive = false
      this.document = document
      this.router.navigate([`/editor/${this.document}`])
      this.liveFormData = {}
      this.submittedFormData = null
      this.formIsValid = null
      this.formValidationErrors = null
    }

    // this runs whenever the document changes.
    this.store
      .select(getDocument(this.document))
      .pipe(take(1))
      .subscribe(data => this.generateForm(this.document, data))
  }

  // Display the form entered by the user
  // (runs whenever the user changes the jsonform object in the ACE input field)
  public generateForm (document, data = {}) {
    this.jsonFormStatusMessage = 'Loading form...'
    this.formActive = false
    this.liveFormData = {}
    this.submittedFormData = null

    const { layout, schema } = this.schemaService.getSchemaForm(
      document,
      this.selector
    )

    let dataSlice

    // TODO: this could be done much simpler.
    if (this.selector.length === 0) {
      dataSlice = data
    } else if (this.selector.length === 1) {
      dataSlice = get(data, this.selector)
    } else {
      dataSlice = get(data, this.selector.slice(0, -1))
    }

    this.jsonFormObject = {
      layout,
      schema,
      data: dataSlice
    }

    console.log('THE OBJECT', this.jsonFormObject)

    this.jsonFormValid = true
    this.formActive = true
  }

  public onSubmit (data: any) {
    this.submittedFormData = data
    console.log('submitting')
    if (this.isValid) {
      this.store.dispatch(
        new DocumentUpdateAction({
          type: this.document,
          path: this.selector,
          contents: data
        })
      )
    } else {
      alert('form not valid')
    }
  }

  get prettySubmittedFormData () {
    return JSON.stringify(this.submittedFormData, null, 2)
  }

  public onChanges (data: any) {
    this.liveFormData = data
    // how am I going to chop it? easy, you know the var names..
    // ok, this is not the way
    // I want to be able to chop it.
    // I am also not using the title and description of the schema now.

    let template = `
# Introduction

## Purpose

{{Purpose}}

## Scope

{{Scope}}

## Definitions and abbreviations

{{#DefinitionsAndAbbreviations}}
### Abbreviations
  {{#abbreviations}}
   - {{definition}}: {{description}}
  {{/abbreviations}}

### Definitions
  {{#definitions}}
   - {{definition}}: {{description}}
  {{/definitions}}

{{/DefinitionsAndAbbreviations}}

## List of References
{{#ListOfReferences}}
 - {{.}}
{{/ListOfReferences}}

## Overview

{{Overview}}
    `

    // Yeah, I want to just generate this.
    // and make that the restriction of the system.
    // no use in having to create these templates yourself.
    // perhaps always generate and allow for adjustments.
    template = `
## Relation to Current Projects

{{RelationToCurrentProjects}}

## Relation To Predecessor And Successor Projects
{{RelationToPredecessorAndSuccessorProjects}}

## Function and Purpose
{{FunctionAndPurpose}}

## Environment
{{Environment}}


## Relation To Other Systems
{{#RelationToOtherSystems}}
{{Description}}

{{#sections}}
### {{title}}

{{text}}
{{/sections}}

{{/RelationToOtherSystems}}



`

    const contents = tpl.render(template, data)

    // this.markdownPreview = JSON.stringify(data, null, 2)
    this.markdownPreview = contents
    console.log('changes?', contents, data)
  }

  get prettyLiveFormData () {
    return JSON.stringify(this.liveFormData, null, 2)
  }

  public isValid (isValid: boolean): void {
    console.log('set is valid, called three times already?')
    this.formIsValid = isValid
  }

  public validationErrors (data: any): void {
    this.formValidationErrors = data
  }

  get prettyValidationErrors () {
    if (!this.formValidationErrors) {
      return null
    }
    const errorArray = []
    for (const error of this.formValidationErrors) {
      const message = error.message
      const dataPathArray = JsonPointer.parse(error.dataPath)
      if (dataPathArray.length) {
        let field = dataPathArray[0]
        for (let i = 1; i < dataPathArray.length; i++) {
          const key = dataPathArray[i]
          field += /^\d+$/.test(key) ? `[${key}]` : `.${key}`
        }
        errorArray.push(`${field}: ${message}`)
      } else {
        errorArray.push(message)
      }
    }
    return errorArray.join('<br>')
  }

  public toggleVisible (item: string) {
    this.visible[item] = !this.visible[item]
  }

  public ngOnDestroy () {
    // not sure if observers are destroyed automatically (thought not)
    console.log('Editor Destroyed', this.document)
  }
}
