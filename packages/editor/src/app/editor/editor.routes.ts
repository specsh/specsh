import { Routes } from '@angular/router'
import { AuthGuard } from '../auth/auth.guard'
import { SchemaCollectionGuard } from '../schemas/guards'
import { DashboardComponent } from './dashboard/dashboard.component'
import { DocumentComponent } from './document/document.component'
import { EditorComponent } from './editor.component'

export const routes: Routes = [
  {
    path: 'editor',
    component: DashboardComponent,
    canActivate: [
      AuthGuard,
      SchemaCollectionGuard
    ]
  },
  {
    path: 'editor/:document',
    component: DocumentComponent,
    canActivate: [
      AuthGuard,
      SchemaCollectionGuard
    ]
  },
  {
    path: 'editor/:document/:section',
    component: EditorComponent,
    canActivate: [
      AuthGuard,
      SchemaCollectionGuard
    ]
  },
  {
    path: 'editor/:document/:section/:subSection',
    component: EditorComponent,
    canActivate: [
      AuthGuard,
      SchemaCollectionGuard
    ]
  }
]
