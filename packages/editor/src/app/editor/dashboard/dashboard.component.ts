import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  @Input() public menu = []

  constructor () { }

  public ngOnInit () {
  }

  public createProject () {
    alert('create project')
  }
}
