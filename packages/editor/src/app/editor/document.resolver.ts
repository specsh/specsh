import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  Resolve,
  Router
} from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { SchemaService } from '../schemas/schemas.service'

// where should this actually be loaded.
// The schemas
// It's necessary application wide.
// So let's make this resolver
// Yet initiate it in the app.
// Could also collect certain resolvers
// and try to resolve them all at once.
// showing a loader in the process.
// mainly on login you will already have to resolve several things.
// Some on the Module level, where several components need the loaded data.
// Sometimes on the component level.
// And sometimes on the app level.
// And sometimes like said on the after authentication level.
// that's a lot of resolving.
// Truth is this loading should not be done from within the controllers.
// because it does not make much sense to start displaying the view
// when essential data is not there yet.
// some say whatever you can show, show it and let data come afterward.
// Anway, it is always nice to have a clear spot where data is loaded.
// so this should be a seperate file at least.
// But now appearently the document controller depends on the schemas
// to already have been loaded.
// this is a whole pattern.
// and I know a super common one.
// it also shows how rest is not the right tool.
// because how to know if data was updated, that's why you would
// like to subscribe to the data instead.
// which means a resolver will not resolve until first data is received.
// with http this makes not much sense at all, because it's just a single request.
// at the moment I don't need it though.
// but with observables it will be more ready for e.g. socket connections or graphql subscribe.

// where are the schemas actually loaded?
// *only* from the SchemasComponent... I already knew this was wrong but did not fix it yet.
// So it should already be resolved by the application component.
// So lets do that with this resolver
// However that does not fit with redux right?
// although I could subscribe to the store ofcourse.
// then just listen for the loaded action.
// Ideally you could easily pick what needs to be loaded initially.
// Dispatch an APP_INIT action?
// then have each module be able to listen for this?
// it would mean it actually belongs in app.effects I guess.
// on APP_INIT I could aggregate a list of tasks to resolve.
// Then dispatch an APP_INITTED action.
// And the main application would resolve.
// let's first prohibit resolving for all components. is that possible?

// Don't understand the template of app loads the router.
// yet there is no ngOnInit  being called on the app component.
// Probably because editor has it's own router.. Yep... :-)

@Injectable()
export class ResolveSchemas implements Resolve<any> {
  constructor (
    private schemaService: SchemaService,
    private store: Store<SpecshState>,
    private router: Router
  ) { }

  public async resolve (route: ActivatedRouteSnapshot): Promise<any> {
    return false
    /*
    return this.heroService.getHero(id).then(hero => {
      if (hero) {
        return hero
      } else { // id not found
        this.router.navigate(['/dashboard'])
        return false
      }
    })
    */
  }
}
