import { JSONSchema6 } from 'json-schema'

declare module '*.json' {
  const value: JSONSchema6
  export default value
}
