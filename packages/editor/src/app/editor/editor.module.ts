import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'
import { FormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { JsonSchemaFormModule } from 'angular2-json-schema-form'
import { MarkedPipe } from '../core/pipes/markdown'
import { SidebarModule } from '../sidebar'
import { MaterialModule } from '../vendor/material'
import { DashboardComponent } from './dashboard/dashboard.component'
import { AceEditorDirective } from './directives/ace-editor.directive'
import { DocumentComponent } from './document/document.component'
import { EditorComponent } from './editor.component'
import { routes } from './editor.routes'

@NgModule({
  declarations: [
    AceEditorDirective,
    DashboardComponent,
    DocumentComponent,
    MarkedPipe,
    EditorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    SidebarModule,
    MaterialModule,
    JsonSchemaFormModule,
    RouterModule.forChild(routes)
  ]
})
export class EditorModule { }
