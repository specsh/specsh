import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../../core/store/reducers'
import { getSchema } from '../../schemas/schemas.selectors'
import { SchemaService } from '../../schemas/schemas.service'
import { createSchemaMenu } from '../../schemas/util/createSchemaMenu'

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})

export class DocumentComponent implements OnInit {
  public menu = []

  // actionSubscription: Subscription

  constructor (private schemaService: SchemaService,
               private route: ActivatedRoute,
               // private params$: Params,
               private store: Store<SpecshState>) {
    /*
    this.actionSubscription = route.params
      .pipe(
        map(params => new SelectDocumentAction(params.schemaName))
      )
      .subscribe(store)
    */
  }

  public ngOnInit () {
    // many ways to do this btw..
    const document = this.route.snapshot.paramMap.get('document')

    this.store.select(getSchema(document))
      .subscribe((schema) => {
        this.menu = createSchemaMenu(schema.schema, document)
      })
  }
}
