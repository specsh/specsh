// tslint:disable max-classes-per-file

import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

@Injectable()
export class ThemesActions {
  public static CHANGE_THEME = '[Themes] CHANGE_THEME'
}

export class ChangeThemeAction implements Action {
  public readonly type = ThemesActions.CHANGE_THEME
  constructor (public payload: string) {}
}

export type Action =
  | ChangeThemeAction
