import { Component, EventEmitter, Output } from '@angular/core'
import { Themes } from '../app.themes'

@Component({
  selector: 'specsh-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.scss']
})
export class ThemesComponent {
  public themes = Themes
  @Output() public themeChanged = new EventEmitter()
}
