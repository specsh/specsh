// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'

@Injectable()
export class NotificationsActions {
  public static ERROR = '[Notifications] ERROR'
  public static INFO = '[Notifications] INFO'
  public static SUCCESS = '[Notifications] SUCCESS'
  public static WARNING = '[Notifications] WARNING'
}

export interface INotification {
  title: string
  message: string
}

export class NotificationAction {
  public type: string
  public payload: INotification
}

// TODO: change to more complex structures (error etc.)

export class ErrorNotificationAction implements NotificationAction {
  public type = NotificationsActions.ERROR
  constructor (public payload: INotification) { }
}

export class InfoNotificationAction implements NotificationAction {
  public type = NotificationsActions.INFO
  constructor (public payload: INotification) { }
}

export class SuccessNotificationAction implements NotificationAction {
  public type = NotificationsActions.SUCCESS
  constructor (public payload: INotification) { }
}

export class WarningNotificationAction implements NotificationAction {
  public type = NotificationsActions.WARNING
  constructor (public payload: INotification) { }
}

export type Action =
  | ErrorNotificationAction
  | InfoNotificationAction
  | SuccessNotificationAction
  | WarningNotificationAction
