import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import {
  ErrorNotificationAction,
  InfoNotificationAction,
  SuccessNotificationAction,
  WarningNotificationAction
} from './index'

@Injectable()
export class NotificationService {
  constructor (
    private store: Store<SpecshState>
  ) {}

  public info (message: string, title: string = 'Information') {
    this.store.dispatch(new InfoNotificationAction({ title, message }))
  }

  public error (message: string, title: string = 'Error') {
    this.store.dispatch(new ErrorNotificationAction({ title, message }))
  }

  public warning (message: string, title: string = 'Warning') {
    this.store.dispatch(new WarningNotificationAction({ title, message }))
  }

  public success (message: string, title: string = 'Success') {
    this.store.dispatch(new SuccessNotificationAction({ title, message }))
  }
}
