
import { Injectable } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import {
  Actions,
  Effect,
  ofType
} from '@ngrx/effects'
import { Store } from '@ngrx/store'
import {
  map,
  tap
} from 'rxjs/operators'
import { SpecshState } from '../core/store/reducers'
import {
  Action,
  NotificationAction,
  NotificationsActions
} from './notifications.actions'

const duration = 3000
const verticalPosition = 'top'

// TODO: perhaps use openFromComponent and show different components per type
@Injectable()
export class NotificationsEffects {
  @Effect({ dispatch: false })
  public error$ = this.actions$.pipe(
    ofType<Action>(
      NotificationsActions.INFO,
      NotificationsActions.ERROR,
      NotificationsActions.WARNING
    ),
    map((action: NotificationAction) => action.payload),
    tap(({ title, message }) => this.notificationsService.open(
      message,
      title,
      { duration, verticalPosition }
    ))
  )

  constructor (
    private actions$: Actions,
    private store: Store<SpecshState>,
    private notificationsService: MatSnackBar
  ) { }
}
