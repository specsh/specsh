import { sandboxOf } from 'angular-playground'
import { FooterComponent } from './footer.component'

export default sandboxOf(FooterComponent)
  .add('show footer', {
    template: `<app-footer>Hey playground!</app-footer>`
  })
