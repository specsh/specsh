import { Component, Input, OnInit } from '@angular/core'
const pkg = require('../../../package.json')

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {
  public year = new Date().getFullYear()
  public version = pkg.version

  @Input() public menu = []

  constructor () { }

  public ngOnInit () {
  }
}
