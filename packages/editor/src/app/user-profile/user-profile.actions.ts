// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { GoogleBasicProfile } from './user-profile.state'

@Injectable()
export class UserProfileActions {
  public static UPDATE = '[UserProfile] UPDATE'
  public static UPDATE_TOKEN = '[UserProfile] UPDATE_TOKEN'
  public static UPDATE_NEXT_PAGE_TOKEN = '[UserProfile] UPDATE_NEXT_PAGE_TOKEN'
  public static USER_PROFILE_COMPLETED = '[UserProfile] USER_PROFILE_COMPLETED'
  public static UPDATE_USER_PROFILE = '[UserProfile] UPDATE_USER_PROFILE'
  public static USER_PROFILE_RECIEVED = '[UserProfile] USER_PROFILE_RECEIVED'

  public static USER_SIGNIN = '[UserProfile] USER_SIGNIN'
  public static USER_SIGNIN_START = '[UserProfile] USER_SIGNIN_START'
  public static USER_SIGNIN_SUCCESS = '[UserProfile] USER_SIGNIN_SUCCESS'

  public static USER_SIGNOUT = '[UserProfile] USER_SIGNOUT'
  public static USER_SIGNOUT_SUCCESS = '[UserProfile] USER_SIGNOUT_SUCCESS'

  public updateData = (data: any) => ({ type: UserProfileActions.UPDATE, payload: data })

  public updateToken = (payload: string) => ({ type: UserProfileActions.UPDATE_TOKEN, payload })

  public updatePageToken (token: string) {
    return {
      type: UserProfileActions.UPDATE_NEXT_PAGE_TOKEN,
      payload: token
    }
  }

  public userProfileCompleted () {
    return {
      type: UserProfileActions.USER_PROFILE_COMPLETED
    }
  }

  public userProfileRecieved (profile: any) {
    return {
      type: UserProfileActions.USER_PROFILE_RECIEVED,
      payload: profile
    }
  }

  public updateUserProfile (profile: GoogleBasicProfile) {
    return {
      type: UserProfileActions.UPDATE_USER_PROFILE,
      payload: profile
    }
  }
}

export class UserSignin implements Action {
  public readonly type = UserProfileActions.USER_SIGNIN
}

export class UserSigninStart implements Action {
  public readonly type = UserProfileActions.USER_SIGNIN_START
}

/*
export class UserSigninSuccess implements Action {
  readonly type = UserProfileActions.USER_SIGNIN_SUCCESS
  constructor (public payload: gapi.auth2.GoogleUser) { }
}
*/
export class UserSigninSuccess implements Action {
  public readonly type = UserProfileActions.USER_SIGNIN_SUCCESS
  constructor (public payload: true) { }
}

export class UserSignout implements Action {
  public readonly type = UserProfileActions.USER_SIGNOUT
}

export class UserSignoutSuccess implements Action {
  public readonly type = UserProfileActions.USER_SIGNOUT_SUCCESS
}
