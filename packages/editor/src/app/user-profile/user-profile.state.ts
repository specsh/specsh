export interface GoogleBasicProfile {
  name?: string
  imageUrl?: string
}

export interface IUserProfile {
  access_token: string
  data?: {}
  nextPageToken?: string
  profile: GoogleBasicProfile
}

export const initialUserState: IUserProfile = {
  access_token: '',
  data: {},
  nextPageToken: '',
  profile: {}
}
