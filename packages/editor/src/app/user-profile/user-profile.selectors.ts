import { createSelector } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { IUserProfile } from './user-profile.state'

export const getUser = (state: SpecshState) => state.user
export const getIsUserSignedIn = createSelector(getUser, (user: IUserProfile) => user.access_token !== '')
