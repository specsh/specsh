import { Action } from '@ngrx/store'
import { UserProfileActions } from './user-profile.actions'
import { initialUserState, IUserProfile } from './user-profile.state'

export * from './user-profile.actions'

interface UnsafeAction extends Action {
  payload: any
}
export function user (state = initialUserState, action: UnsafeAction): IUserProfile {
  switch (action.type) {
    case UserProfileActions.UPDATE_TOKEN:
      return { ...state, access_token: action.payload }

    case UserProfileActions.USER_SIGNOUT_SUCCESS:
      return { ...initialUserState }

    case UserProfileActions.UPDATE:
      return { ...state, data: action.payload }

    case UserProfileActions.UPDATE_NEXT_PAGE_TOKEN:
      return { ...state, nextPageToken: action.payload }

    case UserProfileActions.UPDATE_USER_PROFILE:
      return { ...state, profile: action.payload }

    default:
      return state
  }
}
