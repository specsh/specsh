# Auth 

Auth will react on the User.LOGIN, User.LOGOUT events.
This means the responsibility of the User module is to dispatch these events and nothing more.

This will make the User module very easy to reason about.
And the auth module is sure it has full control over authentication.

If we would have the User module dispatch Auth actions, the coupling would have been tight.
So in general you never dispatch somebody elses actions.

You can however listen to somebody elses actions, which *again* is much simpler to reason about.

Would there be a way to visualize this flow in the frontend?
Is redux doing this enough?


# References:

https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage

https://github.com/stormpath/stormpath-sdk-angularjs/tree/master/src

https://github.com/MindsEyeSociety/services-ui

