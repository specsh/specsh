import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { AuthProviderInterface } from './auth.provider.interface'

import { map } from 'rxjs/operators'

import * as qs from 'qs'

import { environment } from '../../../environments/environment'

@Injectable()
export class AuthProvider implements AuthProviderInterface {
  constructor (
    private http: HttpClient
  ) { }

  public login (
    username: string,
    password: string
  ) {
    return this.http.post<any>(
      environment.api.url + '/auth/login',
      qs.stringify({ username, password }),
      {
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        }
      })
      .pipe(
        map(user => {
          console.log('USER!', user)
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(
              'currentUser',
              JSON.stringify(user)
            )
          }

          return user
        })
      )
  }

  public register (
    username: string,
    password: string
  ) {
    return this.http.post<any>(
      environment.api.url + '/user/register',
      {
        username,
        password
      })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(
              'currentUser',
              JSON.stringify(user)
            )
          }

          return user
        })
      )
  }

  // remove user from local storage to log user out
  public logout () {
    localStorage.removeItem('currentUser')
    localStorage.removeItem('tokenExpire')
  }
}
