import { Action } from '../core/store/types'
import { UserActions } from '../user/user.actions'
import { AuthActions } from './auth.actions'
import {
  IAuthState,
  initialAuthState
} from './auth.state'

export function auth (state = initialAuthState , action: Action): IAuthState {
  switch (action.type) {
    case AuthActions.AUTHENTICATION_REDIRECT:
      return {
        ...state,
        returnUrl: action.payload.returnUrl
      }
    case AuthActions.AUTHENTICATED:
      return {
        ...state,
        isAuthenticated: true
      }

    case UserActions.LOGOUT:
    case AuthActions.DEAUTHENTICATE:
      return {
        ...state,
        isAuthenticated: false
      }

    default:
      return state
  }
}
