import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import {
  Actions,
  Effect,
  ofType
} from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import {
  map,
  tap
} from 'rxjs/operators'
import { SpecshState } from '../core/store/reducers'
import { NotificationService } from '../notifications/notifications.service'
import {
  UserActions,
  UserLoginAction,
  UserLogoutAction
} from '../user/user.actions'
import {
  AuthActions,
  AuthAuthenticatedAction,
  AuthAuthenticationRedirectAction
} from './auth.actions'
import { selectorAuthReturnUrl } from './auth.selectors'
import { AuthService } from './auth.service'

const defaultUrl = ['schemas']

@Injectable()
export class AuthEffects {
  constructor (// private actions$: Actions<AuthAction | UserAction>,
    private actions$: Actions<any>, // FIX ME
    private authService: AuthService,
    private notificationService: NotificationService,
    private store: Store<SpecshState>,
    private router: Router
  ) {}

  @Effect({ dispatch: false })
  public authenticationRedirect () {
    return this.actions$.pipe(
        ofType<AuthAuthenticationRedirectAction>(AuthActions.AUTHENTICATION_REDIRECT),
        tap(() => this.router.navigate(['user', 'login']))
      )
  }

  @Effect({ dispatch: false })
  public authenticated () {
    return this.actions$.pipe(
        ofType<AuthAuthenticatedAction>(AuthActions.AUTHENTICATED),
        tap(() => {
          this.store.select(selectorAuthReturnUrl)
            .subscribe((returnUrl) => {
              const url = returnUrl ? [returnUrl] : defaultUrl
              this.router.navigate(url)
            })
        })
      )
  }

  @Effect({ dispatch: false })
  public login () {
    return this.actions$.pipe(
        ofType<UserLoginAction>(UserActions.LOGIN),
        tap(({ type, payload: { email, password } }) => {
          this.authService.login(email, password)
            .subscribe(
              data => {
                this.store.dispatch(new AuthAuthenticatedAction())
              },
              error => {
                this.notificationService.error(error.message)
                // was set on login component
                // this.loading = false
              })
        })
      )
  }

  @Effect({ dispatch: true })
  public logout (): Observable<UserLogoutAction> {
    return this.actions$.pipe(
        ofType<UserLogoutAction>(UserActions.LOGOUT),
        tap(() => {
          this.authService.logout()
        }),
        map(() => new AuthAuthenticationRedirectAction({ returnUrl: '' }))
    )
  }
}
