// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

@Injectable()
export class AuthActions {
  public static AUTHENTICATED = '[Auth] AUTHENTICATED'
  public static DEAUTHENTICATE = '[Auth] DEAUTHENTICATE'
  public static AUTHENTICATION_REDIRECT = '[Auth] AUTHENTICATION REDIRECT'
}

export interface RedirectPayload {
  returnUrl: string
}

export class AuthAuthenticationRedirectAction implements Action {
  public readonly type = AuthActions.AUTHENTICATION_REDIRECT
  constructor (public payload: RedirectPayload) { }
}

export class AuthAuthenticatedAction implements Action {
  public readonly type = AuthActions.AUTHENTICATED
}

export class AuthDeauthenticateAction implements Action {
  public readonly type = AuthActions.DEAUTHENTICATE
}

export type ActionsUnion =
  | AuthAuthenticatedAction
  | AuthDeauthenticateAction
