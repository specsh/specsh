import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { AuthAuthenticationRedirectAction } from './auth.actions'
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor (
    private router: Router,
    private authService: AuthService,
    private store: Store<SpecshState>
  ) { }

  public protectedRoute (url: string) {
    if (this.authService.isAuthenticated()) {
      // logged in so return true
      return true
    }

    this.store.dispatch(new AuthAuthenticationRedirectAction({
      returnUrl: url || '/'
    }))

    return false
  }

  public canActivate (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.protectedRoute(state.url)
  }

  public canActivateChild (
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.protectedRoute(state.url)
  }
}
