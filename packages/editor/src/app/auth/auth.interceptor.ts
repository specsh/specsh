import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http'
import { Injectable, Injector } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from '../../environments/environment'
import { AuthService } from './auth.service'

const resourceServers = [
  environment.api.url
]

export function isResourceUrl (url: string) {
  return Boolean(resourceServers.find(server => (
    url.startsWith(server)
  )))
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private authService: AuthService
  constructor (
    private injector: Injector
  ) { }

  public intercept (
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.authService = this.injector.get(AuthService)

    const url = request.url.toLowerCase()

    if (isResourceUrl(url)) {
      const authHeader = this.authService.getAuthorizationHeader()

      if (authHeader) {
        const authHeaders = request.headers.set('Authorization', authHeader)

        const authRequest = request.clone({ headers: authHeaders })

        return next.handle(authRequest)
      }
    }

    return next.handle(request)
  }
}

export const AuthInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
}
