export const selectorAuth = state => state.auth
export const selectorIsAuthenticated = state => state.auth.isAuthenticated
export const selectorAuthReturnUrl = state => state.auth.returnUrl
