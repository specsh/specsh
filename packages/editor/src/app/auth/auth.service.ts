import { Injectable } from '@angular/core'
import { AuthProvider } from './provider/auth.provider'

import { map } from 'rxjs/operators'

@Injectable()
export class AuthService {
  constructor (
    private authProvider: AuthProvider
  ) {}
  public getToken () {
    return JSON.parse(localStorage.getItem('currentUser'))
  }

  public isAuthenticated () {
    const token = this.getToken()

    return token && !this.isExpired()
  }

  public isExpired () {
    const tokenExpire = Number(localStorage.getItem('tokenExpire'))

    return (tokenExpire - Date.now()) <= 0
  }

  public getAuthorizationHeader () {
    const token = this.getToken()

    if (
      token &&
      token.access_token &&
      token.refresh_token
    ) {
      return `Bearer ${token.access_token}`
    }

    return false
  }

  public login (username: string, password: string) {
    return this.authProvider.login(username, password)
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (
            user &&
            user.access_token &&
            user.expires_in
          ) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user))
            localStorage.setItem('tokenExpire', String(Date.now() + (user.expires_in * 1000)))
          }

          return user
        })
      )
  }

  public logout () {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser')
    localStorage.removeItem('tokenExpire')
  }
}
