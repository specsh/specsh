export interface IAuthState {
  isAuthenticated: boolean
  returnUrl: null | string
}

export const initialAuthState: IAuthState = {
  isAuthenticated: false,
  returnUrl: null
}
