export interface JsonSchemaFormInterface {
  onSubmit: (event: any) => void
  onChanges?: (event: any) => void
  isValid?: (event: any) => void
  validationErrors?: (event: any) => void
}
