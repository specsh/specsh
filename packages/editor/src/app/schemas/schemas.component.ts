import { Component, OnInit } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { SpecshState } from '../core/store/reducers'
import { Schema } from './schemas.models'
import { selectAllSchemas } from './schemas.selectors'

// This def is one on one with the database in service-document / schemas
// which is name, url
// I want to have that working:
//  - list schemas
//  - create new schema (point to a schema somewhere on the internet and name it)
//  - edit schema
//  - remove schema (optionally removing all data also)
//  - switch between the schemas from a drop down (e.g. switch between resume and requirements document
//  - Have generic tools for schema display.

@Component({
  selector: 'schemas',
  templateUrl: './schemas.component.html',
  styleUrls: ['./schemas.component.scss']
})

export class SchemasComponent implements OnInit {
  public schemas$: Observable<Schema[]>
  public layouts: any
  constructor (
    private store: Store<SpecshState>
  ) {
    this.schemas$ = this.store.pipe(select(selectAllSchemas))
  }

  public ngOnInit () {
  }
}
