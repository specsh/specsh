export function propertiesList (properties) {
  return Object.keys(properties).map((name) => {
    return {
      name,
      schema: properties[name]
    }
  })
}
