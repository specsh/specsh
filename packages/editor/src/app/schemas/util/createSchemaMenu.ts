export interface SchemaMenuItem {
  name: string
  href: string
  children?: SchemaMenuItem[]
}

export type SchemaMenu = SchemaMenuItem[]

export function createSchemaMenu (schema, schemaName: string) {
  if (schema.hasOwnProperty('properties')) {
    return Object.keys(schema.properties)
      .reduce((obj: SchemaMenu, name: string) => {
        const item = schema.properties[name]
        const href = `/editor/${schemaName}/${name}`

        if (item.properties && typeof item.properties === 'object' && item.properties !== 'null') {
          obj.push({
            name: item.title || name,
            href,
            children: Object.keys(item.properties).map((key: string) => {
              const subItem = item.properties[key]

              return {
                name: subItem.title || key,
                href: `${href}/${key}`,
                children: []
              }
            })
          })
        }

        return obj
      }, [])
  }

  return []
}
