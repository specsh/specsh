import * as _ from 'lodash'

const arrReg = new RegExp(/\.\[]/, 'g')

import { getInputType } from 'angular2-json-schema-form'

function formType (type: any) {
  switch (type) {
    case 'string':
      return 'textarea'
    case 'boolean':
      return 'textarea'
    case 'object':
      return 'textarea'
    case 'function':
      return 'textarea'
    case 'number':
      return 'textarea'
    default:
      return type
  }
}

export function dotFlatten ($schema) {
  function dotFlat (schema, path = []) {
    if (schema.hasOwnProperty('properties')) {
      return _.map(_.keys(schema.properties), (name) => {
        return dotFlat(schema.properties[name], path.concat(name))
      })
    } else if (schema.type === 'array' && schema.hasOwnProperty('items')) {
      if (_.isArray(schema.items)) {
        if (schema.items.length > 1) {
          throw Error('Multiple item types not supported')
        }
        return _.map(schema.items, (item, index) => {
          return dotFlat(schema.items[index], path.concat('[]'))
        })
      } else if (typeof schema.items === 'object') {
        return dotFlat(schema.items, path.concat('[]'))
      } else {
        throw Error('Unsupported items type')
      }
    }

    // need to know the current value, then I might get somewhere.
    return {
      key: path.join('.').replace(arrReg, '[]'),
      type: getInputType(schema), // formType(schema.type)
      description: schema.description
    }
  }

  return _.flattenDeep(dotFlat($schema)) // .map((key) => key.replace(arrReg, '[]'))
}
