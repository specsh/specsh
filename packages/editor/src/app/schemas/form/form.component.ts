import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { select, Store } from '@ngrx/store'
import { of } from 'rxjs'
import { filter, map, switchMap } from 'rxjs/operators'
import { SpecshState } from '../../core/store/reducers'
import { JsonSchemaFormInterface } from '../interfaces/JsonSchemaFormInterface'
import {
  CreateSchemaAction,
  UpdateSchemaAction
} from '../schemas.actions'
import { getSchemasSchemas } from '../schemas.selectors'
import { schemaSetSchema } from './schemas/schemaSet'
import { schemaSetForm } from './schemas/schemaSetForm'

// in a way this would only have to be instantiated once.
// and several can listen to it.
// as long as :schemaName is used anywhere in the route.
// and then you could be more generic.
// like who want to subscribe to the store and watch whether the schema collection was loaded.
// So that's one thing. Wait until the schemas are loaded.
// Next thing is some then want to pick a certain schema.
// Then again a simple promise once, makes it much more easier to reason about.

function resolveSchema (route: ActivatedRoute, store) {
  return route.params
    .pipe(
      map((params) => params.schemaName),
      switchMap(
        (schemaName) => {
          if (schemaName) {
            return store.pipe(
              select(getSchemasSchemas),
              filter((schemas: any) => schemas.loaded),
              map((schemas: any) => schemas.entities[schemaName]),
              switchMap((schema: any) => of({
                ...schema,
                // todo: leaf objects (as defined in schema) should always be stringified and parsed
                schema: JSON.stringify(schema.schema, null, 2),
                layouts: schema.layouts || []
              }))
            )
          }

          return of({})
        }
      )
    )
}

@Component({
  selector: 'schemas-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SchemasFormComponent implements OnInit, JsonSchemaFormInterface {
  @Input() public model
  public form = {}
  public formOptions = {}
  /*
  data$: Observable<Schema> = Observable.of({
    name: '',
    schema: '',
    uri: ''
  })
  */
  public data: any = {}

  public schemaName: string

  constructor (private store: Store<SpecshState>,
               private route: ActivatedRoute) {
  }

  public ngOnInit () {
    this.form = {
      schema: schemaSetSchema,
      // layout: ['*'] // schemaSetForm
      layout: schemaSetForm
    }
    this.schemaName = this.route.snapshot.paramMap.get('schemaName')
    resolveSchema(this.route, this.store)
      .subscribe((data) => this.data = data)
  }

  public onSubmit (data) {
    if (this.schemaName) {
      if (!this.data._id) {
        throw Error('Cannot find _id')
      }

      this.store.dispatch(new UpdateSchemaAction({
        _id: this.data._id,
        ...data
      }))
    } else {
      this.store.dispatch(new CreateSchemaAction(data))
    }
  }

  public onChanges (event) {

  }

  public isValid (event) {

  }

  public validationErrors (event) {

  }
}
