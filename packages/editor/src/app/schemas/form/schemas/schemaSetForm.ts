export const schemaSetForm = [
  {
    key: 'name'
  },
  {
    title: 'Schema Uri',
    key: 'uri'
  },
  {
    type: 'monaco-editor',
    title: 'Schema Content',
    editorOptions: {
      theme: 'vs-dark',
      language: 'json',
      // codeLens: false,
      minimap: {
        enabled: false
      }
    },
    key: 'schema'
  },
  {
    key: 'layouts',
    type: 'array',
    listItems: 1,
    items: [
      {
        key: 'layouts[].name',
        type: 'text'
      },
      {
        key: 'layouts[].type'
      },
      {
        key: 'layouts[].layout',
        type: 'monaco-editor',
        title: 'Layout',
        editorOptions: {
          theme: 'vs-dark',
          language: 'json',
          // codeLens: false,
          minimap: {
            enabled: false
          }
        }
      }
    ]
  }
]
