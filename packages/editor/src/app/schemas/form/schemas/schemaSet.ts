export const schemaSetSchema = {
  title: 'SchemaSet',
  descriptions: 'A set of schemas',
  properties: {
    name: {
      title: 'Name',
      type: 'string',
      description: 'name/handle of the schema will be used as a human friendly identifier for the schema.'
    },
    uri: {
      title: 'Uri',
      type: 'string',
      description: 'the universal resource identifier for the schema.'
    },
    schema: {
      title: 'Schema',
      type: 'string',
      description: 'the contents of the schema.'
    },
    layouts: {
      title: 'Layouts',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            title: 'Name'
          },
          type: {
            type: 'string',
            enum: ['markdown']
          },
          layout: {
            type: 'string'
          }
        }
      }
    },
    collection: {
      title: 'Allow multiple items to be created',
      type: 'boolean',
      description: 'Whether multiple instances of this schema can be created within the application. E.g. a user has one profile or can have multiple profiles.',
      default: true
    }
  }
}
