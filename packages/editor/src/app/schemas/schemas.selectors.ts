import { createSelector } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { schemaAdapter } from './schemas.state'
import { createSchemaMenu } from './util/index'

export const getSchemas = (state: SpecshState) => state.schemas
export const getSchemasSchemas = createSelector(getSchemas, (schemas) => schemas.schemas)

// TODO: not sure if this renaming is even necessary.
// could also just import * as schemaSelectors from 'schema.selectors'
export const {
  selectIds: selectSchemaIds,

  selectEntities: selectSchemaEntities,

  selectAll: selectAllSchemas,

  selectTotal: selectUserTotal

} = schemaAdapter.getSelectors(getSchemasSchemas)

export const getSchema = (name: string) => createSelector(
  selectAllSchemas,
  (schemas) => schemas.find(schema => schema.name === name)
)

export const hasSchema = (name: string) => createSelector(
  getSchema,
  (schema): boolean => Boolean(schema)
)

export const selectCurrentSchemaId = createSelector(
  getSchemasSchemas,
  (schemas) => schemas.currentSchemaId
)

export const getCurrentSchema = createSelector(
  selectSchemaEntities,
  selectCurrentSchemaId,
  (schemaEntities, schemaId) => schemaEntities[schemaId]
)

export const getSchemasLoaded = createSelector(
  getSchemasSchemas,
  (schema) => schema.loaded
)

export const getSchemasLoading = createSelector(
  getSchemasSchemas,
  (schema) => schema.loading
)

export const getSchemaMenu = (name) => createSelector(
  getSchema(name),
  schemaInfo => {
    return schemaInfo ?
      createSchemaMenu(
        schemaInfo.schema,
        schemaInfo.name
      ) : []
  }
)
