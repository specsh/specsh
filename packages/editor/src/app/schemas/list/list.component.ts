import { Component, Input, OnInit } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { SpecshState } from '../../core/store/reducers'
import {
  RemoveSchemaAction
} from '../schemas.actions'
import { Schema } from '../schemas.models'
import { selectAllSchemas } from '../schemas.selectors'

@Component({
  selector: 'schemas-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SchemasListComponent implements OnInit {
  @Input() public model
  public schemas$: Observable<Schema[]>

  constructor (
    private store: Store<SpecshState>
  ) {}

  public ngOnInit () {
    this.schemas$ = this.store.pipe(select(selectAllSchemas))
  }

  public remove (schemaName: string) {
    this.store.dispatch(new RemoveSchemaAction(schemaName))
  }
}
