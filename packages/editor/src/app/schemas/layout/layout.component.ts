import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../../core/store/reducers'

@Component({
  selector: 'layout-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SchemaLayoutComponent implements OnInit {
  public schemaName: string
  constructor (
    private store: Store<SpecshState>,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  public ngOnInit () {
    this.route.params
      .subscribe((params) => {
        const { schemaName } = params

        this.schemaName = schemaName
      })
    /*
    this.store.dispatch(new LoadSchemaFormLayoutAction())
    this.store.select(getSchemaFormLayout)
      .subscribe((schemas) => {
        console.log('the schemas', schemas)

        this.schemas = propertiesList(schemas.schemas.properties)
        this.layouts = schemas.layouts
      })
    */
  }

  public addView () {

  }
}
