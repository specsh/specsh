import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from '../../../core/store/reducers'
import { JsonSchemaFormInterface } from '../../interfaces/JsonSchemaFormInterface'
import { SchemaService } from '../../schemas.service'
import { dotFlatten } from '../../util'

const schema = require('@specsh/schemas/src/schemas/layout.schema.json')
const layout = require('@specsh/schemas/src/schemas/layout.schema.form.json')

// non dynamic for now
// const resumeSchema = require('@specsh/schemas/src/schemas/resume/resume.json')

/**
 *  Things like enum population should work with pre-filters.
 *  This means you will have to define what external sources
 *  Should be attached to which fields, loading of this data less to do with the layout definition of the form.
 *  So actually you will be able to hang services on form fields.
 */

// ok this can actually serve as input.
// no the layout of the resume can serve as input to this.
/*
{
  bla: {
    type: 'select'
    titleMap: dotFlatten(schema).map((value) => ({
      name: value,
      value
    }))
  }
}
*/

@Component({
  selector: 'schema-form-layout',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SchemaFormLayoutComponent implements JsonSchemaFormInterface, OnInit {
  public form: any
  public formOptions = {}
  public schemas: any
  public layouts: any

  public previewSchema: any
  public previewLayout: any
  public previewData: any

  constructor (
    private store: Store<SpecshState>,
    private router: Router,
    private schemaService: SchemaService,
    private route: ActivatedRoute
  ) {
  }

  public ngOnInit () {
    this.route.params
      .subscribe((params) => {
        const { schemaName } = params

        const entitySchema = this.schemaService.getSchema(schemaName)
        const entityLayout = dotFlatten(entitySchema)

        this.form = {
          schema,
          layout,
          data: {
            title: `${entitySchema.title || schemaName} Form Layout`,
            name: `${schemaName}-form-layout`,
            description: '',
            layout: entityLayout
          }
        }

        this.previewSchema = entitySchema
        this.previewLayout = this.form.layout
        this.previewData = {} // no data for now... :)
      })
  }

  public editFormLayout () {

  }

  public onSubmit (event) {
    alert('submit')
  }

  public onChanges (event) {
    // this.previewSchema = resumeSchema
    this.previewLayout = event.layout
    // this.previewData = {} // no data for now... :)
  }

  public isValid (event) {

  }

  public validationErrors (event) {

  }
}
