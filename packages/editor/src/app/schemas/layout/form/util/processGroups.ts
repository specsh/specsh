/*
const layout = processGroups(dotFlatten(schema))

console.log(JSON.stringify(layout, null, 2))
*/
export function processGroups (value) {
  const layout = []
  for (let i = 0; i < value.length; i++) {
    if (value[i].key.indexOf('[]') >= 0) {
      // only goes one level deep, should be possible to go deeper.
      const group = {
        type: 'tabarray',
        tabType: 'left',
        title: '{{ value.key }}',
        key: value[i].key.substr(0, value[i].key.indexOf('[]')),
        listItems: 1,
        items: [{
          type: 'div',
          // displayFlex: true,
          // 'flex-direction': 'row',
          items: []
        }]
      }
      while (value[i] && value[i].key.indexOf('[]')) {
        group.items[0].items.push(value[i])
        i++
      }
      layout.push(group)
    } else {
      layout.push(value[i])
    }
  }
  return layout
}
