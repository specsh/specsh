import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { SchemaLayoutComponent } from './layout.component'

describe('SchemaLayoutComponent', () => {
  let component: SchemaLayoutComponent
  let fixture: ComponentFixture<SchemaLayoutComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchemaLayoutComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SchemaLayoutComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
