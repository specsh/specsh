import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from '../auth/auth.guard'
import { SchemasFormComponent } from './form/form.component'
import { SchemaCollectionGuard } from './guards'
import { SchemaFormLayoutComponent } from './layout/form/form.component'
import { SchemaLayoutComponent } from './layout/layout.component'
import { SchemasListComponent } from './list/list.component'
import { SchemasComponent } from './schemas.component'

export const routes: Routes = [
  {
    path: '',
    component: SchemasComponent,
    canActivate: [
      AuthGuard,
      SchemaCollectionGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: SchemasListComponent
      },
      {
        path: 'create',
        component: SchemasFormComponent
      },
      {
        path: ':schemaName/layout',
        component: SchemaLayoutComponent
      },
      {
        path: ':schemaName/layout/form',
        component: SchemaFormLayoutComponent
      },
      {
        path: ':schemaName/edit',
        component: SchemasFormComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchemasRoutingModule {}
