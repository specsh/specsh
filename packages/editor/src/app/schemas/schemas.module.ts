import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { JsonSchemaFormModule } from 'angular2-json-schema-form'
import { MaterialModule } from '../vendor/material'
import { SchemasFormComponent } from './form/form.component'
import { guards } from './guards'
import { SchemaFormLayoutComponent } from './layout/form/form.component'
import { SchemaLayoutComponent } from './layout/layout.component'
import { SchemasListComponent } from './list/list.component'
import { SchemasNavigationComponent } from './navigation/navigation.component'
import { SchemasComponent } from './schemas.component'
import { SchemasRoutingModule } from './schemas.routing'

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    JsonSchemaFormModule,
    SchemasRoutingModule
  ],
  declarations: [
    SchemasComponent,
    SchemaLayoutComponent,
    SchemaFormLayoutComponent,
    SchemasNavigationComponent,
    SchemasListComponent,
    SchemasFormComponent
  ],
  exports: [
    SchemasComponent,
    SchemaLayoutComponent,
    SchemaFormLayoutComponent,
    SchemasNavigationComponent,
    SchemasListComponent,
    SchemasFormComponent
  ],
  providers: [
    ...guards
  ]
})
export class SchemasModule { }
