// const fullSchema = require('@specsh/schemas/src/schemas/fullSchema.json')
// import * as layouts from '@specsh/schemas/src/form/srd'
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'
import { Schema } from './schemas.models'

export interface ISchemaState {
  schemas: any, // JSONSchema6, Somehow complains although the types are correct!?
  layouts: any
}

export interface SchemasState extends EntityState<Schema> {
  currentSchemaId: string | null
  loaded: boolean
  loading: boolean
}

export const schemaAdapter: EntityAdapter<Schema> = createEntityAdapter<Schema>({
  selectId: (schema: Schema) => schema.name, // name more convenient than _id
  sortComparer: false
})

const schemas: SchemasState = schemaAdapter.getInitialState({
  currentSchemaId: null,
  loaded: false,
  loading: false
})

export const initialSchemaState: ISchemaState = {
  schemas,
  layouts: {} // put layouts in schemas..
  // layouts: layouts.layouts,
}
