import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import {
  Actions,
  Effect,
  ofType
} from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { of } from 'rxjs'
import {
  catchError,
  map,
  switchMap,
  tap
} from 'rxjs/operators'
import { SpecshState } from '../core/store/reducers'
import {
  AddSchemaAction,
  CreateSchemaAction,
  LoadSchemasFailAction,
  LoadSchemasSuccessAction,
  RemoveSchemaAction,
  SchemaChangeAction,
  SchemaEditAction,
  SchemaEditFormLayoutAction,
  SchemaRemovedAction,
  SchemasActions,
  SchemaUpdatedAction,
  UpdateSchemaAction
} from './schemas.actions'
import { SchemaService } from './schemas.service'

@Injectable()
export class SchemasEffects {
  @Effect()
  public appInit$ = this.actions$.pipe(
    ofType(SchemasActions.LOAD),
    switchMap(() =>
      this.schemasService
        .find()
        .pipe(
          map((schemas) => new LoadSchemasSuccessAction(schemas)),
          catchError(error => of(new LoadSchemasFailAction(error)))
        )
    )
  )

  @Effect() public updateSchema$ = this.actions$.pipe(
    ofType<UpdateSchemaAction>(SchemasActions.UPDATE),
    switchMap((action: UpdateSchemaAction) =>
      this.schemasService
        .update(action.payload)
        .pipe(
          map((result) => new SchemaUpdatedAction(result)),
          tap(() => this.router.navigate(['schemas', 'overview']))
        )
    )
  )

  @Effect({ dispatch: false })
  public createSchema$ = this.actions$.pipe(
      ofType<CreateSchemaAction>(SchemasActions.CREATE),
      tap((action: CreateSchemaAction) => {
        this.schemasService.create(action.payload)
          .subscribe((result) => {
            this.store.dispatch(new AddSchemaAction(result))
          })
      })
    )

  @Effect({ dispatch: false })
  public editSchema$ = this.actions$.pipe(
      ofType<SchemaEditAction>(SchemasActions.EDIT),
      tap((action: SchemaEditAction) => {
        this.router.navigate(['schemas', action.payload, 'edit'])
      })
    )

  @Effect({ dispatch: false })
  public removeSchema$ = this.actions$.pipe(
      ofType<RemoveSchemaAction>(SchemasActions.REMOVE),
      tap((action: RemoveSchemaAction) => {
        this.schemasService.removeSchema(action.payload)
          .subscribe(() => {
            this.store.dispatch(new SchemaRemovedAction(action.payload))
          })
      })
    )

  @Effect({ dispatch: false })
  public changeSchema$ = this.actions$
    .pipe(
      ofType<SchemaChangeAction>(SchemasActions.CHANGE),
      tap((action: SchemaChangeAction) => {
        this.router.navigate(['editor', action.payload])
      })
    )

  @Effect({ dispatch: false })
  public editFormLayout$ = this.actions$.pipe(
      ofType<SchemaEditFormLayoutAction>(SchemasActions.EDIT_FORM_LAYOUT),
      tap((action: SchemaChangeAction) => {
        this.router.navigate(['schemas', 'layout', 'form', action.payload])
      })
    )

  constructor (
    private actions$: Actions,
    // private actions$: any,
    private store: Store<SpecshState>,
    private router: Router,
    private schemasService: SchemaService
  ) { }
}
