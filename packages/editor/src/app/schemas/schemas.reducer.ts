import { Action } from '../core/store/types'
import { SchemasActions } from './schemas.actions'
import { initialSchemaState, ISchemaState, schemaAdapter } from './schemas.state'

export function schemas (state = initialSchemaState, action: Action): ISchemaState {
  switch (action.type) {
    case SchemasActions.CHANGE: {
      // this should also set the layout.
      // or maybe even the current layout
      // then have multiple layouts active?
      // Storage flows, one of them usb.
      return {
        ...state,
        schemas: {
          ...state.schemas,
          currentSchemaId: action.payload // currently not exactly the id..
        }
      }
    }

    // bit weird to do this independent from the load effect.
    // not sure if that's correct, will work, but still
    case SchemasActions.LOAD: {
      return {
        ...state,
        schemas: {
          ...state.schemas,
          loading: true
        }
      }
    }

    /*
    // so populate becomes load_success
    case SchemasActions.POPULATE: {
      return {
        ...state,
        schemas: {
          ...schemaAdapter.addMany(action.payload, state.schemas),
          currentSchemaId: state.schemas.currentSchemaId
        }
      }
    }
    */

    case SchemasActions.LOAD_SUCCESS: {
      return {
        ...state,
        schemas: {
          ...schemaAdapter.addMany(action.payload, state.schemas),
          currentSchemaId: state.schemas.currentSchemaId,
          loading: false,
          loaded: true
        }
      }
    }

    case SchemasActions.ADD: {
      return {
        ...state,
        schemas: {
          ...schemaAdapter.addOne(action.payload, state.schemas),
          currentSchemaId: state.schemas.currentSchemaId
        }
      }
    }

    case SchemasActions.UPDATED: {
      const updates = schemaAdapter.updateOne({
        id: action.payload.name,
        changes: action.payload
      }, state.schemas)

      return {
        ...state,
        schemas: {
          ...updates,
          currentSchemaId: state.schemas.currentSchemaId
        }
      }
    }

    case SchemasActions.REMOVED: {
      return {
        ...state,
        schemas: {
          ...schemaAdapter.removeOne(action.payload, state.schemas),
          currentSchemaId: action.payload._id === state.schemas.currentSchemaId ?
            null : state.schemas.currentSchemaId
        }
      }
    }

    default:
      return state
  }
}
