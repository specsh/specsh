import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot } from '@angular/router'
import { MemoizedSelector, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import {
  switchMap,
  take
} from 'rxjs/operators'
import { SpecshState } from '../../core/store/reducers'
import { hasSchema } from '../schemas.selectors'
import { SchemaCollectionGuard } from './collection.guard'

@Injectable()
export class ItemGuard extends SchemaCollectionGuard {
  constructor (
    store: Store<SpecshState>,
    private route: ActivatedRouteSnapshot
  ) {
    super(store)
  }

  public canActivate (): Observable<boolean | MemoizedSelector<any,boolean>> {
    return this.checkCollection().pipe(
      switchMap(() => {
        const id = parseInt(this.route.params.schemaName, 10)
        return this.hasItem(id)
      })
    )
  }

  public hasItem (id: number | string): Observable<boolean | MemoizedSelector<any,boolean>> {
    return this.store
      .select<any>(hasSchema)
      .pipe(
        take(1)
      )
  }
}
