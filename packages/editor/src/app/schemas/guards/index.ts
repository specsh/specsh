import { SchemaCollectionGuard } from './collection.guard'

export const guards: any[] = [SchemaCollectionGuard]

export * from './collection.guard'
