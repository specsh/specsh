
import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'

import { Store } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import {
  catchError,
  filter,
  switchMap,
  take,
  tap
} from 'rxjs/operators'
import { SpecshState } from '../../core/store/reducers'
import { LoadSchemasAction } from '../schemas.actions'
import { getSchemasLoaded } from '../schemas.selectors'

@Injectable()
export class SchemaCollectionGuard implements CanActivate {
  constructor (
    protected store: Store<SpecshState>
  ) {}

  public canActivate (): Observable<boolean | any> {
    return this.checkCollection().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    )
  }

  public checkCollection (): Observable<boolean> {
    return this.store.select(getSchemasLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new LoadSchemasAction())
        }
      }),
      filter(loaded => loaded),
      take(1)
    )
  }
}
