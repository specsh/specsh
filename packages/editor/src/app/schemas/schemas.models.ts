export interface Schema {
  _id?: string,
  name: string,
  uri: string,
  schema?: string
}
