import { Component, Input } from '@angular/core'

@Component({
  selector: 'schemas-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class SchemasNavigationComponent {
  @Input() public menu = []
  @Input() public model
}
