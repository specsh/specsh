# Schemas

This is the schemas module.

It interacts with the schemas api to save json schema definitions and
automatically create storage collections for the definitions within this schema.

Instances of this schema are rendered using the documents module and the editor module.




### Module

#### Components

#### Models


### Control

#### Resolver

#### Routing

#### Guards


### Ngrx

#### Actions

#### Reducer

#### Selectors

#### Service

#### State

#### Effects


### Layouts

Forms have the concept of layouts, where you tie a form layout to a schema.
The schema defines what the form should look like.

In addition the current system is also aware of what part of the schema to render
based on the url. And also the forms system in this case is aware of which part of the
form to render, so you can take partial slices of the form.

I would like to expand this to rendering of different kind of layouts also.
E.g. markdown documents, or just display in general. You will be showing
partial slices of your schema. Whether this makes sense depends a lot on
how you structure your schemas. In general it will expect the first 1 or two levels.
To be useful for toplevel rendering.

I think this concept works pretty well for certain kind of content.
It's not the holy grail for any kind of content though, but let that just be it's boundaries.

Right now I give the ability to also store layout definitions directly along with
the schemas. The idea being you can pick these layouts whenever a schema instance
is to be displayed. 

One problem with the slicing though, slicing is more difficult in Markdown, because
it's not really hierarchical, the headers could define the layers and this it would be
possible to also show the slices. However HTML and xml are much better for this.

The problem is Markdown is not structured. Something like xslt would work much better.
Because it's designed to work with paths and selection and morph content into
different representations.

I must not underestimate how much content I am already able to create with this system.

Do I have a clear vision of what it should eventually become.

Extremely pluggable system.

Everything created, is actually automatically "mounted" at an endpoint.
So every instance I create, you can fetch back also, ofcourse this is nothing
new, but these instances should then be linkable to content, functionality etc.

e.g. a user requirements document, now will automatically have crud actions.
And also the swagger docs are instantly available. All by just adding specific 
schemas.

Anyway, first make these schemas instances saveable again.

I see a problem with this typescript way of doing things.
We are creating a lot of duplication this way.
I like how every action is different in the redux devtools.
I would prefer to limit the amount of code to create actions though.
If there are crud actions in the application I really would lik to be able
to do just createCrudActions('documents') and be done with it.
or even:
[
  ...createCrudActions('documents'),
  ...createOtherCommonActionsFor('documents')
]

Because I already feel I'm creating way to much duplicated code.
And somehow with redux this became embraced, which is weird.

Creating actions that way will also become much less error prone.

We could even generate the selectors and reducers this way.
And still have them all be distinct.

So I want to refactor schemas, and then use what is refactor with documents also.
And perhaps also for user profiles etc. 

