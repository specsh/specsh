import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import * as Ajv from 'ajv'
import { JSONSchema6 } from 'json-schema'
import { get, set } from 'lodash'
import { of } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { schemaPath } from '../../../../schemas/src/form/util/schemaPath'
import { environment } from '../../environments/environment'
import { SpecshState } from '../core/store/reducers'
import { getSchemas } from './schemas.selectors'
import { createSchemaMenu } from './util/index'

const jsonSchemaDraft06 = require('ajv/lib/refs/json-schema-draft-06.json')

const validator = Ajv()

validator.addMetaSchema(jsonSchemaDraft06)

export interface SchemaForm {
  schema: JSONSchema6,
  layout: any
}

export interface Schemas {
  [key: string]: JSONSchema6
}

export interface Layouts {
  [key: string]: any
}

@Injectable()
export class SchemaService {
  private schemas
  private layouts

  constructor (
    private store: Store<SpecshState>,
    private http: HttpClient
    ) {
    this.store.select(getSchemas)
      .subscribe(({ schemas: schemaSet, layouts }) => {
        this.schemas = schemaSet
        /*
        this.schemas = Object.keys(schemaSet.properties)
          .reduce((schemas, name) => {
            schemas[name] = schemaSet.properties[name]

            return schemas
          }, {})
        */
        this.layouts = layouts
      })
  }

  public create (data: any) {
    return this.http.post(`${environment.api.url}/schemas`, data)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error(error) // notification service
          return of(undefined)
        })
      )
  }

  public update (data: any) {
    return this.http.put(`${environment.api.url}/schemas`, data)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error(error) // notification service
          return of(undefined)
        })
      )
  }

  public removeSchema (schemaName: string) {
    return this.http.delete(`${environment.api.url}/schemas/${schemaName}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error(error) // notification service
          return of(undefined)
        })
      )
  }

  public find (params?: any) {
    return this.http.get(`${environment.api.url}/schemas`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          console.error(error) // notification service
          return of(undefined)
        })
      )
  }

  public createSelector (selector) {
    return selector.join('.')
  }

  public getTitlePath (selector) {

  }

  public getSchemaMenu (name: string) {
    return createSchemaMenu(this.getSchema(name), name)
  }

  public validate (name: string, data: any) {
    return validator.validate(this.getSchema(name), data)
  }

  public getValidationErrors () {
    return validator.errorsText()
  }

  public getSchema (name: string) {
    // return this.schemas[name]
    // return this.schemas.find((schemaInfo) => schemaInfo.name === name)
    return this.schemas.entities[name].schema
  }

  public getSchemaForm (name: string, selector: string[]) {
    return this.selectSchema(this.getSchema(name), selector)
  }

  public selectLayout (layouts, selector) {
    let sectionLayout = selector.length ? layouts[selector[0]] : layouts

    if (selector[1]) {
      const reg = new RegExp(`^\\b${selector[1]}\\b`) // 'a.b' and 'a' but not 'ab'

      sectionLayout = sectionLayout.filter((subSection) => reg.test(subSection.key))
    }

    return sectionLayout
  }

  /**
   * Selects the (sub)schema and (sub)layout.
   *
   * Provided that the first entries in the path of the full schema are objects.
   *
   * The Spec.sh schema adheres to these rules.
   *
   * @param schema
   * @param {string[]} selector
   * @returns {{schema: JSONSchema6; layout: any}}
   */
  public selectSchema (schema, selector: string[]) {
    let newSchema: JSONSchema6 = {
      type: 'object',
      properties: {}
    }

    if (selector.length > 0) {
      if (selector.length === 1) {
        newSchema = get(schema, schemaPath(selector))
      } else {
        set(
          newSchema.properties,
          selector.slice(1).join('.properties.'),
          get(schema, schemaPath(selector))
        )
      }
    } else {
      newSchema = {
        definitions: schema.definitions,
        ...schema
      }
    }

    return {
      schema: newSchema,
      layout: this.selectLayout(this.layouts, selector)
    }
  }
}
