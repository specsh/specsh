// tslint:disable max-classes-per-file

import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Schemas } from './schemas.service'

@Injectable()
export class SchemasActions {
  public static ADD = '[Schemas] Add'
  public static ADD_SUCCESS = '[Schemas] Add Success'
  public static ADD_FAIL = '[Schemas] Add Fail'

  public static REMOVE = '[Schemas] Remove'
  public static REMOVE_SUCCESS = '[Schemas] Remove Success'
  public static REMOVE_FAIL = '[Schemas] Remove Fail'
  public static REMOVED = '[Schemas] REMOVED'

  public static CREATE = '[Schemas] CREATE'

  public static LOAD = '[Schemas] Load'
  public static LOAD_SUCCESS = '[Schemas] Load Success'
  public static LOAD_FAIL = '[Schemas] Load Fail'

  public static CHANGE = '[Schemas] CHANGE'
  public static EDIT = '[Schemas] EDIT'

  public static UPDATE = '[Schemas] UPDATE'
  public static UPDATED = '[Schemas] UPDATED'

  public static POPULATE = '[Schemas] POPULATE'
  public static EDIT_FORM_LAYOUT = '[Schemas] EDIT_FORM_LAYOUT'
}

export class LoadSchemasAction implements Action {
  public readonly type = SchemasActions.LOAD
}

export class LoadSchemasSuccessAction implements Action {
  public readonly type = SchemasActions.LOAD_SUCCESS
  constructor (public payload: Schemas[]) { }
}

export class LoadSchemasFailAction implements Action {
  public readonly type = SchemasActions.LOAD_FAIL
  constructor (public payload: Error) { }
}

export class RemoveSchemaAction implements Action {
  public readonly type = SchemasActions.REMOVE
  constructor (public payload: any) { }
}

export class RemoveSchemaSuccessAction implements Action {
  public readonly type = SchemasActions.REMOVE_SUCCESS
  constructor (public payload: any) { }
}

export class RemoveSchemaFailAction implements Action {
  public readonly type = SchemasActions.REMOVE_FAIL
  constructor (public payload: any) { }
}

export class CreateSchemaAction implements Action {
  public readonly type = SchemasActions.CREATE
  constructor (public payload: any) { }
}

export class SchemaEditAction implements Action {
  public readonly type = SchemasActions.EDIT
  constructor (public payload: any) { }
}

export class UpdateSchemaAction implements Action {
  public readonly type = SchemasActions.UPDATE
  constructor (public payload: any) { }
}

export class SchemaUpdatedAction implements Action {
  public readonly type = SchemasActions.UPDATED
  constructor (public payload: any) { }
}

export class SchemaRemovedAction implements Action {
  public readonly type = SchemasActions.REMOVED
  constructor (public payload: any) { }
}

export class AddSchemaAction implements Action {
  public readonly type = SchemasActions.ADD
  constructor (public payload: any) { }
}

export class SchemaChangeAction implements Action {
  public readonly type = SchemasActions.CHANGE
  constructor (public payload: string) { }
}

export class SchemaEditFormLayoutAction implements Action {
  public readonly type = SchemasActions.EDIT_FORM_LAYOUT
  constructor (public payload: string) { }
}

export class SchemasPopulateAction implements Action {
  public readonly type = SchemasActions.POPULATE
  constructor (public payload: any) { }
}

export type Action =
  | LoadSchemasAction
  | SchemaChangeAction
