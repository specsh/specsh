import { Component, EventEmitter, Input, Output } from '@angular/core'
import { User } from '../user/models'

@Component({
  selector: 'user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent {
  @Input() public user: User
  @Input() public isAuthenticated: boolean

  @Output() public themeChange = new EventEmitter()
  @Output() public logout = new EventEmitter()
  @Output() public signIn =  new EventEmitter()
  @Output() public signUp = new EventEmitter()
}
