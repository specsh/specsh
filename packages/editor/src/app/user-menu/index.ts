import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { ThemesComponent } from '../themes/themes.component'
import { MaterialModule } from '../vendor/material/index'
import { UserMenuComponent } from './user-menu.component'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [
    ThemesComponent,
    UserMenuComponent
    // UserMenuMenuComponent,
    // UserMenuUserComponent
  ],
  exports: [
    UserMenuComponent
  ]
})
export class UserMenuModule { }
