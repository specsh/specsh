import {
  APP_BOOTSTRAP_LISTENER,
  ComponentRef,
  NgModule
} from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'
import { BrowserModule } from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { ResolveApp } from './app.resolver'
import { AppRoutingModule } from './app.routing'
import { AuthInterceptorProvider } from './auth/auth.interceptor'
import { CommitModule } from './commit'
import { CoreModule } from './core'
import { DebugModule } from './debug/debug.module'
import { EditorModule } from './editor/editor.module'
import { FooterModule } from './footer'
import { NavbarModule } from './navbar'
import { PageNotFoundComponent } from './not-found.component'
import { SchemasModule } from './schemas/schemas.module'
import { SidebarModule } from './sidebar'
import { UserMenuModule } from './user-menu'
import { UserModule } from './user/user.module'
import { MaterialModule } from './vendor/material'

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    UserModule,
    CommitModule,
    DebugModule,
    CoreModule,
    MaterialModule,
    SidebarModule,
    NavbarModule,
    EditorModule,
    FooterModule,
    UserMenuModule,
    SchemasModule,
    FlexLayoutModule,
    AppRoutingModule
  ],
  providers: [
    AuthInterceptorProvider,
    ResolveApp,
    {
      provide: APP_BOOTSTRAP_LISTENER, multi: true, useFactory: () => {
        return (component: ComponentRef<any>) => {
          console.log('App Instance)', component.instance.title)
        }
      }
    }
    /*
    ...[
      {
        provide: APP_INITIALIZER,
        useFactory: async () => {
          alert('Initializing app! APP_INITIALIZER 1')
          console.log('Initializing app!')

          return true
        },
        multi: true
      },
      {
        provide: APP_INITIALIZER,
        useFactory: async () => {
          alert('Initializing app! APP_INITIALIZER 2')
          console.log('Initializing app 2!')

          return true
        },
        multi: true
      }
    ]
    */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
