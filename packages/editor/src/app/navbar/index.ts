import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { MaterialModule } from '../vendor/material/index'
import { CommitModule } from '../commit'
import { NavbarComponent } from './navbar.component'
import { UserMenuModule } from '../user-menu'

@NgModule({
  imports: [
    // SharedModule,
    CommonModule,
    CommitModule,
    RouterModule,
    UserMenuModule,
    MaterialModule
  ],
  declarations: [
    NavbarComponent
    // NavbarMenuComponent,
    // NavbarUserComponent
  ],
  exports: [
    NavbarComponent
  ]
})
export class NavbarModule { }
