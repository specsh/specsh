import { Component, Input } from '@angular/core'
import { User } from '../user/models'

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
  @Input() public user: User
  @Input() public isAuthenticated: boolean

  @Input() public themeChange: () => {}
  @Input() public logout: () => {}
  @Input() public signIn: () => {}
  @Input() public signUp: () => {}
  public logo = require('../../assets/logo.png')

  public navigation = [
    /*
    { link: 'about', label: 'About' },
    */
  ]
  public navigationSideMenu = [
    ...this.navigation,
    { link: 'settings', label: 'Settings' }
  ]

  // private unsubscribe$: Subject<void> = new Subject<void>()
}
