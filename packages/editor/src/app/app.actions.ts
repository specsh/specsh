// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

@Injectable()
export class AppActions {
  public static INIT = '[App] INIT'
}

export class AppInitAction implements Action {
  public readonly type = AppActions.INIT
}

export type Action =
  | AppInitAction
