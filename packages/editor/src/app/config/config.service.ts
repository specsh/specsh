import { Injectable } from '@angular/core'
import { get } from 'lodash'

export type Config = any

@Injectable()
export class ConfigService {
  /**
   * Provide configurable service
   */
  public static provide () {
    return {
      provide: ConfigService,
      useFactory: (config: Config) => new ConfigService(config)
      // deps: [Greeter]
    }
  }

  constructor (private config: Config) { }

  public get (option: string) {
    return get(this.config, option)
  }
}
