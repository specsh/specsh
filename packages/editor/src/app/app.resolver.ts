import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  Resolve
} from '@angular/router'
import { Store } from '@ngrx/store'
import { SpecshState } from './core/store/reducers'

@Injectable()
export class ResolveApp implements Resolve<any> {
  constructor (
    private store: Store<SpecshState>
  ) { }

  public async resolve (route: ActivatedRouteSnapshot): Promise<any> {
    confirm('Are you ready to resolve?')
    return false
  }
}
