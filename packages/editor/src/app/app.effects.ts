import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import {
  Actions,
  Effect,
  ofType
} from '@ngrx/effects'
import { Store } from '@ngrx/store'
import { tap } from 'rxjs/operators'
import { AppActions } from './app.actions'
import { SpecshState } from './core/store/reducers'
import { LoadSchemasAction } from './schemas'

@Injectable()
export class AppEffects {
  @Effect({ dispatch: false })
  public appInit$ = this.actions$.pipe(
      ofType(AppActions.INIT),
      tap(() => {
        this.store.dispatch(new LoadSchemasAction())
      })
    )

  constructor (
    private actions$: Actions,
    private store: Store<SpecshState>,
    private router: Router
  ) { }
}
