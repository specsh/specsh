// tslint:disable max-classes-per-file
import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'

@Injectable()
export class AppActions {
  public static LIST = '[Crud] LIST'
  public static ADD = '[Crud] ADD'
  public static EDIT = '[Crud] EDIT'
  public static REMOVE = '[Crud] REMOVE'

  public static CREATE = '[Crud] CREATE'
  public static DELETE = '[Crud] DELETE'
  public static UPDATE = '[Crud] UPDATE'
}

export class CrudListAction implements Action {
  public readonly type = AppActions.LIST
}

export class CrudAddAction implements Action {
  public readonly type = AppActions.ADD
}

export class CrudEditAction implements Action {
  public readonly type = AppActions.EDIT
}

export class CrudRemoveAction implements Action {
  public readonly type = AppActions.REMOVE
}

export class CrudDeleteAction implements Action {
  public readonly type = AppActions.DELETE
}

export class CrudUpdateAction implements Action {
  public readonly type = AppActions.UPDATE
}

export class CrudCreateAction implements Action {
  public readonly type = AppActions.CREATE
}

export type Action =
  | CrudCreateAction
  | CrudListAction
