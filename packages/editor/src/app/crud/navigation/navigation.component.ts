import { Component, Input, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import { SpecshState } from '../../core/store/reducers'
import { CrudCreateAction, CrudListAction } from '../crud.actions'

@Component({
  selector: 'crud-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Input() public menu = []
  @Input() public model

  constructor (
    private store: Store<SpecshState>
  ) { }

  public ngOnInit () {
  }

  public create () {
    this.store.dispatch(new CrudCreateAction())
  }

  public list () {
    this.store.dispatch(new CrudListAction())
  }
}
