import { Component, Input } from '@angular/core'
import { Store } from '@ngrx/store'
import { SpecshState } from '../core/store/reducers'
import { DocumentCommitAction } from '../documents'

@Component({
  selector: 'app-commit',
  templateUrl: './commit.component.html',
  styleUrls: ['./commit.component.scss']
})

export class CommitComponent {
  public year = new Date().getFullYear()

  @Input() public menu = []

  constructor (
    private store: Store<SpecshState>
  ) { }

  public commit () {
    this.store.dispatch(new DocumentCommitAction())
  }
}
