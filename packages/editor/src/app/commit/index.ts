import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { MaterialModule } from '../vendor/material/index'
import { CommitComponent } from './commit.component'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [
    CommitComponent
  ],
  exports: [
    CommitComponent
  ]
})
export class CommitModule { }
