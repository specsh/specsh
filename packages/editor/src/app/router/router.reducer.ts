import { Params, RouterStateSnapshot } from '@angular/router'
import { RouterStateSerializer } from '@ngrx/router-store'

export interface RouterStateUrl {
  params: Params
  queryParams: Params
  url: string
}

export class NavigationSerializer
  implements RouterStateSerializer<RouterStateUrl> {
  public serialize (
    routerState: RouterStateSnapshot
  ): RouterStateUrl {
    let { root: route } = routerState
    const { url, root: { queryParams } } = routerState

    while (route.firstChild) {
      route = route.firstChild
    }

    const { params } = route

    return {
      url,
      params,
      queryParams
    }
  }
}
