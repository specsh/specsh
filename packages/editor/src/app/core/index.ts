import { NgModule } from '@angular/core'
import { APP_APIS } from './api'
import { AppEffectsModules } from './effects'
import { APP_RESOLVERS } from './resolvers'
import { APP_SERVICES } from './services'
import { CoreStoreModule } from './store'

@NgModule({
  imports: [
    CoreStoreModule,
    AppEffectsModules
  ],
  declarations: [],
  exports: [CoreStoreModule],
  providers: [
    ...APP_SERVICES,
    ...APP_RESOLVERS,
    ...APP_APIS
  ]
})
export class CoreModule {
}
