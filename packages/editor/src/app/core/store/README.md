# Store

### Reference

[Comprehensive Introduction to ngrx store](https://blackholegalaxy1.gitbooks.io/comprehensive-introduction-to-ngrx-store)

[Ngrx Patterns And Techniques](https://blog.nrwl.io/ngrx-patterns-and-techniques-f46126e2b1e5)

[Ngrx Example App](https://github.com/ngrx/platform/tree/master/example-app)

# Storage for spec.sh

The ideal is a single source of truth for specs.
This however does not mean we store the specs in one location.

The single source of truth is eventually a github repository storing the specs.

This can be a very simple setup, where it's very obvious the specs just belong
to the repository at which the code resides or a more involved setup.

Like said the document will not be stored at one location.

Storage points:
 - Redux state
 - Local storage as part of the saved redux state
 - In case of the web app storage is within the backend of the rest server.
   These will store the documents within a database as part of a user's
   current session. This in general is in draft state. In draft state
   because the database will not be the single source of truth.
 - Stored in git, this is the single sort of truth.

Git makes it easy to comment on your changes (commits).
I think to support this when not saving to git is to just provide a
way to specify the changes you've made. Else after editing for a while
the eventual merge into git will be a bit non descriptive.

I'm also still very focused on only creating one document.
because the idea is document > repository is one on one.

But that will not work with multiple projects ofcourse.

ok feel a lot about using graphql for the backend.
Or just use the knex thing, dunno.

Or I actually like: https://pubkey.github.io/rxdb
The best, because it already uses json schema.
Only think I have to do is make sure when I use $ref in the schema.
To normalize the document, I also create a `ref` in the database.
So it will be more relational.
I would like to be able to totally recreate this.

### Problem

Life time of a click

example-app:
  book-detail.html: (click)="remove.emit(book)"
  book-detail.ts: @output remove = new EventEmitter<Book>()
  selected-book-page.html: <book-detail (remove)="removeFromCollection($event)">
  selected-book-page.ts: removeFromCollection(book: Book) => this.store.dispatch(new collection.RemoveBook(book)
  ... RemoveBook Action Dispatch ...
  Either Effect or reducer will handle it or both.
  The *problem* here is that none is an observable yet.
  What is supposed to be the source of the observable (the click event) is not treated as such.
  However! The actions *are* treated as observables. So I guess it's a matter of where you want to hook in.
  You either e.g. rate limit the actions or you rate limit the click event itself.
  
  Anyway, how does https://github.com/ngrx/store/issues/296 solve my problem.
  It seems the problem is their regardless of where the observable comes to life.
  
  My problem is, I do not give enough information about my intend.
  I give the intend `commit` but don't tell it what to commit. and hence I need to fetch back.. neh..
  That's not the problem. I'm not going to pass the entire document through my commit click, that makes even less sense.
  So I should put this effects and then take the latest value from the store and then process it.
  My problem is, I am trying to fetch the value from the service. Whereas the @effects are the ones observing the actions.
  The service is not observing anything.
  
  But what about the $http service then, it's also using observables?
  How and when does that observable get destroyed? 
  
  Anyway, observables from services seems wrong. So let's look at some other services and see if that statement is correct.
  
