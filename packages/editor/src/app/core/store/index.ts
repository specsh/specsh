import { NgModule } from '@angular/core'
import {
  RouterStateSerializer,
  StoreRouterConnectingModule
} from '@ngrx/router-store'
import {
  ActionReducer,
  MetaReducer,
  StoreModule
} from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { localStorageSync } from 'ngrx-store-localstorage'

import { environment } from '../../../environments/environment'
import { NavigationSerializer } from '../../router/router.reducer'
import {
  SpecshActions,
  SpecshReducers
} from './reducers'

export function localStorageSyncReducer (reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: Object.keys(SpecshReducers),
    rehydrate: true
  })(reducer)
}
// const metaReducers: MetaReducer<any, any>[] = [localStorageSyncReducer]

// disable for now, stores too much
const metaReducers: Array<MetaReducer<any, any>> = []

const optionalImports = []
if (!environment.production) {
  // Note that you must instrument after importing StoreModule
  optionalImports.push(
    StoreDevtoolsModule.instrument({
      name: 'Spec.sh Store DevTools',
      logOnly: environment.production
      // maxAge: 25
    })
  )
}

@NgModule({
  imports: [
    StoreModule.forRoot(
      SpecshReducers,
      {
        metaReducers
      }),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router'
    }),
    ...optionalImports
  ],
  declarations: [],
  exports: [],
  providers: [
    ...SpecshActions,
    {
      provide: RouterStateSerializer,
      useClass: NavigationSerializer
    }
  ]
})
export class CoreStoreModule { }
