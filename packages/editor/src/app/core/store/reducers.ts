import { ActionReducerMap } from '@ngrx/store'
// import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import {
  appLayout,
  IAppSettings
} from '../../app-layout'
import { auth, IAuthState } from '../../auth'
/*
import {
  documents,
  IDocumentsState
} from '../../documents'
*/
import { ISchemaState, schemas } from '../../schemas'
import { IUserProfile, user, UserProfileActions } from '../../user-profile'

export interface SpecshState {
  user: IUserProfile
  appLayout: IAppSettings
  schemas: ISchemaState
  // documents: IDocumentsState
  auth: IAuthState
  // routerReducer: RouterReducerState
}

export let SpecshReducers = {
  auth,
  appLayout,
  // documents,
  user,
  schemas
  // routerReducer
}

export let SpecshActions = [UserProfileActions]
