# Core

Many of the best practises setup contain this core module system.

However most of them implement the contents of it differently.

I should review again these repositories and choose the best practise.

Repositories to review are:

  - [Ngx Model Hacker News Example](https://github.com/tomastrajan/ngx-model-hacker-news-example)
  - [Echos Player](https://github.com/orizens/echoes-player)
  - [Angular JumpStart](https://github.com/DanWahlin/Angular-JumpStart)
  - [Angular Material Starter](https://github.com/tomastrajan/angular-ngrx-material-starter)
  - [Yatrum](https://github.com/aviabird/yatrum)
  - [AngularSpree](https://github.com/aviabird/angularspree)
  - [AngularSeed](https://github.com/aviabird/angular-seed)


What I definitely want to create is a repository of re-usable services.
All this core functionality can eventually be moved to either:
 - specsh-core OR robberthalff-core
 
When this application is finished I want to be able to quickly created
similar applications. And finally stop re-inventing the wheel for myself.

That's why I want to choose for Angular now and for a considered period of time.


