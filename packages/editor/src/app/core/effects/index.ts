import { EffectsModule } from '@ngrx/effects'

import { AppSettingsEffects } from '../../app-settings/app-settings.effects'
import { AppEffects } from '../../app.effects'
import { AuthEffects } from '../../auth'
// import { DocumentsEffects } from '../../documents/documents.effects'
import { NotificationsEffects } from '../../notifications/notifications.effects'
import { RouterEffects } from '../../router/router.effects'
import { SchemasEffects } from '../../schemas/schemas.effects'
import { UserEffects } from '../../user'

export const AppEffectsModules = EffectsModule.forRoot([
  AppEffects,
  AppSettingsEffects,
  AuthEffects,
  // DocumentsEffects,
  NotificationsEffects,
  RouterEffects,
  UserEffects,
  SchemasEffects
])
