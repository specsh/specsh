import { LayoutService } from '../../layout/layout.service'
import { LocalStorageService } from '../../local-storage/local-storage.service'
import { NotificationService } from '../../notifications/notifications.service'
import { SchemaService } from '../../schemas/schemas.service'
// import { DocumentService } from '../../documents/documents.service'

export const APP_SERVICES = [
  // DocumentService,
  LocalStorageService,
  NotificationService,
  LayoutService,
  SchemaService
]
