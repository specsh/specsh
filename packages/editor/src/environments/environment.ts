const packageJson = require('../../package.json')

export const environment = {
  production: false,
  api: {
    url: 'http://specsh:8000/v1'
  }
}
