import { createActions, handleActions } from 'redux-actions'

// think this could even be type save.

function createCrudActions (type) {
  const defaultState = {}
  const TYPE = type.toUpperCase()
  const type = type.toLowerCase()

  const actions = createActions({
    [`${TYPE}_CREATE`]: amount => ({ amount: 1 }),
    [`${TYPE}_UPDATE`]: amount => ({ amount: 1 }),
    [`${TYPE}_LIST`]: amount => ({ amount: -1 })
  })

  const reducer = handleActions({
    [actions[`${type}_create`]] (state, { payload: { amount } }) {
      return { counter: state.counter + amount }
    },
    [actions[`${type}_update`](state, { payload: { amount } }) {
      return { counter: state.counter + amount }
    },
    [actions[`${type}_list`]] (state, { payload: { amount } }) {
      return { counter: state.counter + amount }
    }
  }, defaultState)
}

createCrudActions('Schemas')
