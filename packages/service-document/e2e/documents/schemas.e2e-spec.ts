import * as express from 'express'
import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { assert, expect } from 'chai'
import { SchemasModule } from '../../src/app/schemas/schemas.module'
import { SchemasService } from '../../src/app/schemas/schemas.service'
import { INestApplication } from '@nestjs/common'

describe('Schemas', () => {
  let server
  let app: INestApplication

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [SchemasModule],
    })
      .compile()

    server = express()
    app = module.createNestApplication(server)
    await app.init()
  })

  it(`/DELETE Schemas`, () => {
    return request(server)
      .delete('/schemas')
      .set('Accept', 'application/json')
      .expect(200)
  })

  it(`/GET Schemas`, () => {
    return request(server)
      .get('/schemas')
      .set('Accept', 'application/json')
      .expect(200)
      .expect([])
  })

  it(`/POST create Schema`, (done) => {
    return request(server)
      .post('/schemas')
      .set('Accept', 'application/json')
      .send({
        name: 'resume',
        uri: 'https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json'
      })
      .expect(201)
      .end((error, response) => {
        if (error) return done(error)

        expect(response.body._id).to.be.ok
        expect(response.body.name).to.equal('resume')
        expect(response.body.uri).to.equal('https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json')

        done()
      })
  })

  it(`/DELETE Schema`, (done) => {
    return request(server)
      .delete('/schemas/:resume')
      .set('Accept', 'application/json')
      .expect(200)
      .end((error, response) => {
        if (error) return done(error)

        expect(response.body).to.be.ok

        done()
      })
  })

  afterAll(async () => {
    await app.close()
  })
})
