import * as objenv from 'objenv'

export const environment = objenv({
  clientId: 'specsh',
  port: 2303,
  jwt: {
    secret: 'change_me'
  },
  mongo: {
    uri: 'mongodb://localhost:30017/specsh'
  },
  whitelist: [
    'http://localhost:4200',
    'http://localhost:2303',
    'http://specsh'
  ]
})
