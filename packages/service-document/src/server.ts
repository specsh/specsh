import { NestFactory } from '@nestjs/core'
import { whitelist } from '@nestling/cors'
import { LogService } from '@nestling/logger'
import * as bodyParser from 'body-parser'
import { readJSON } from 'fs-extra'
import { resolve } from 'path'
import { ApplicationModule } from './app/app.module'
import { environment } from './environments/environment'
// import { ConfigService } from '@nestling/config'
// import { NestExpressApplication } from '@nestjs/platform-express'

;(async () => {
  const pkg = await readJSON(resolve(__dirname, '../package.json'))
  const app = await NestFactory.create(ApplicationModule)

  app.enableCors(whitelist(environment.whitelist))

  /*
  const config = app
    .get<ConfigService>(ConfigService)
  */

  const logger = app
    .get(LogService)

  // logger.level((config as any).log.level || 'info')
  logger.level('info')

  app.use(bodyParser.json({
    limit: '100mb'
  }))

  await app.listen(environment.port)

  logger.info(`${pkg.name} is listening on port ${environment.port}`)
})()
  .catch((error) => {
    console.error(error)
  })
