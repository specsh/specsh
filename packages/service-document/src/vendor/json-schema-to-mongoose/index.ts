import * as _ from 'lodash'
import * as mongoose from 'mongoose'

const typeStringToMongooseType = {
  string: String,
  boolean: Boolean,
  number: Number,
  integer: Number
}

const typeRefToMongooseType = {
  '#/definitions/objectid': mongoose.Schema.Types.ObjectId,
  '#/definitions/dateOrDatetime': Date
}

const subSchemaTypeV3 = (parentSchema, subSchema, key) => {
  return parentSchema.required.indexOf(key) >= 0 && !_.isPlainObject(subSchema)
    ? { type: subSchema, required: true }
    : subSchema
}

const subSchemaTypeV4 = (parentSchema, subSchema, key) => {
  return parentSchema.required.indexOf(key) >= 0
    ? !_.isPlainObject(subSchema)
      ? { type: subSchema, required: true }
      : subSchema.hasOwnProperty('type')
        ? _.assign(subSchema, { required: true })
        : subSchema
    : subSchema
}

const schemaParamsToMongoose = {
  /**
   * default value
   */
  default: (defaultValue: string) => {
    const func = (_.last(/^\[Function=(.+)\]$/.exec(defaultValue)) || '')
      .replace(/\\_/g, '`underscore`')
      .replace(/_/g, ' ')
      .replace(/`underscore`/g, '_')

    return {
      default: eval(func) || defaultValue
    }
  },

  /**
   * Pattern for value to match
   */
  pattern: (pattern: string) => {
    return { match: RegExp(pattern) }
  },

  type: (type: string) => {
    return { type: typeStringToMongooseType[type] }
  },

  minLength: (min: number) => {
    return { minlength: min }
  },
  maxLength: (max: number) => {
    return { maxlength: max }
  },
  minimum: (min: number) => {
    return { min }
  },
  maximum: (max: number) => {
    return { max }
  },
  enum: (members: any[]) => {
    return { enum: members }
  }
}

const toMongooseParams = (acc, val, key) => {
  const func = schemaParamsToMongoose[key]

  return func ? _.assign(acc, func(val)) : acc
}

const unsupportedRefValue = jsonSchema => {
  throw new Error(`Unsupported $ref value: ${jsonSchema.$ref}`)
}
const unsupportedJsonSchema = jsonSchema => {
  throw new Error(`Unsupported JSON schema type, \`${jsonSchema.type}\``)
}

const convert = (refSchemas: any, jsonSchema: any): any => {
  if (jsonSchema.$schema === 'http://json-schema.org/draft-03/schema#') {
    return convertV(3, refSchemas, jsonSchema)
  } else if (jsonSchema.$schema === 'http://json-schema.org/draft-04/schema#') {
    return convertV(4, refSchemas, jsonSchema)
  } else {
    // backwards compatibility
    return convertV(3, refSchemas, jsonSchema)
  }
}

const convertV = (version: any, refSchemas: any, jsonSchema: any): any => {
  if (!_.isPlainObject(jsonSchema)) {
    unsupportedJsonSchema(jsonSchema)
  }

  let converted
  const format = jsonSchema.format
  const isRef = !_.isEmpty(jsonSchema.$ref)
  const isTypeDate =
    jsonSchema.type === 'string' &&
    (format === 'date' || format === 'date-time')
  const mongooseRef = typeRefToMongooseType[jsonSchema.$ref]
  const isMongooseRef = typeof mongooseRef !== 'undefined'
  const subSchema = _.isEmpty(refSchemas) ? false : refSchemas[jsonSchema.$ref]
  const subSchemaType = version === 4 ? subSchemaTypeV4 : subSchemaTypeV3

  return (isRef
    ? isMongooseRef
      ? mongooseRef
      : subSchema
        ? convertV(version, refSchemas, subSchema)
        : unsupportedRefValue(jsonSchema)
    : isTypeDate
      ? _.reduce(
          _.omit(jsonSchema, 'type', 'format') as any,
          toMongooseParams,
          { type: typeRefToMongooseType['#/definitions/dateOrDatetime'] }
        )
      : _.has(typeStringToMongooseType, jsonSchema.type)
        ? _.reduce(jsonSchema, toMongooseParams, {})
        : jsonSchema.type === 'object'
          ? _.isEmpty(jsonSchema.properties)
            ? mongoose.Schema.Types.Mixed
            : ((converted = _.mapValues(
                jsonSchema.properties,
                convertV.bind(null, version, refSchemas)
              )),
              jsonSchema.required
                ? _.mapValues(converted, subSchemaType.bind(null, jsonSchema))
                : converted)
          : jsonSchema.type === 'array'
            ? !_.isEmpty(jsonSchema.items)
              ? [convertV(version, refSchemas, jsonSchema.items)]
              : []
            : !_.has(jsonSchema, 'type')
              ? mongoose.Schema.Types.Mixed
              : unsupportedJsonSchema(jsonSchema))
}

interface CreateMongooseSchema {
  (refSchemas: any, jsonSchema: any): any
  (refSchemas: any): (jsonSchema: any) => any
}

export const createMongooseSchema = _.curry(convert) as CreateMongooseSchema
