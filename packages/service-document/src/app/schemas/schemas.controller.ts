import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards
} from '@nestjs/common'
import { AuthGuard } from '../auth'
import { User, UserModel } from '../user'
import { SchemasService } from './schemas.service'

/**
 *  /robberthalff/schemas (post new schema {name, schema})
 *  /robberthalff/schemas (get list of schemas [{name, schema}])
 *  /robberthalff/resume/schema (get schema)
 *  /robberthalff/resume (post instance)
 *  /robberthalff/resume (get instance, first if no params)
 *
 *  So should split creation of the schema and posting of the schema.
 *  During schema post you also have to submit a name.
 */

@Controller('schemas')
export class SchemasController {
  constructor (
    private readonly schemasService: SchemasService
  ) {}

  @Post()
  @UseGuards(AuthGuard)
  public async create (
    @User() user: UserModel,
    @Body() body: any // CreateSchemaDto
  ) {
    return this.schemasService.createSchemaCollectionIfNotExists(
      user.id,
      body.name,
      body.uri,
      body.schema,
      body.layouts,
      body.collection
    )
  }

  @Put()
  @UseGuards(AuthGuard)
  public async update (
    @User() user: UserModel,
    @Body() body: any
  ) {
    return this.schemasService.updateSchemaCollection(
      user.id,
      body
    )
  }

  @Get('/ping')
  public async ping (): Promise<any> {
    return 'OK'
  }

  @Get()
  @UseGuards(AuthGuard)
  public async find (
    @User() user: UserModel
  ): Promise<any> {
    return this.schemasService.find(user.id)
  }

  @Delete()
  @UseGuards(AuthGuard)
  public async removeAll (
    @User() user: UserModel
  ) {
    return this.schemasService.removeSchemas(user.id)
  }

  @Delete('/:schemaName')
  @UseGuards(AuthGuard)
  public async removeSchema (
    @User() user: UserModel,
    @Param('schemaName') schemaName: string
  ) {
    return this.schemasService.removeSchema(user.id, schemaName)
  }

  public async destroy () {
    await this.schemasService.close()
  }
}
