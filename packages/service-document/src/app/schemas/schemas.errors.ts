import { IErrorMessages } from '@nestling/errors'

export const schemasErrors: IErrorMessages = {
  type: 'schema',
  errors: []
}
