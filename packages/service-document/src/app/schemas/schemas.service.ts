import { Injectable } from '@nestjs/common'
import { DatabaseService } from '../database/database.service'

export function assertJson (layouts: any) {
  try {
    JSON.parse(JSON.stringify(layouts))
  } catch (error) {
    throw Error('Layouts contains invalid JSON')
  }
}

@Injectable()
export class SchemasService {
  constructor (
    private readonly databaseService: DatabaseService
  ) {}

  /**
   * Creates a collection named after the schema uri.
   *
   * The collection is created using the schema.
   *
   * @param clientName {String}
   * @param schemaName {String}
   * @param schemaUri {String}
   * @param schemaContents {String}
   * @param layouts
   * @param multiple {Boolean}
   * @returns {Promise<void>}
   */
  public async createSchemaCollectionIfNotExists (
    clientName: string,
    schemaName: string,
    schemaUri: string,
    schemaContents: any = null,
    layouts: any[] = [],
    multiple: boolean = true
  ) {
    const schemaInfo = await this.databaseService.getSchemaInfo(clientName, schemaName)

    if (!schemaInfo) {
      console.log('NO SCHEMA YET')
      return this.databaseService.createSchemaCollection(
        clientName,
        schemaName,
        schemaUri,
        schemaContents,
        layouts,
        multiple
      )
    }

    console.log('SCHEMA!', schemaInfo)
    return schemaInfo
  }

  /**
   * Updates a schemas collection
   *
   * @param clientName {String}
   * @param schema {SchemaInfo}
   * @returns {Promise<void>}
   */
  public async updateSchemaCollection (
    clientName: string,
    schema: any
  ) {
    const schemaInfo = await this.databaseService.getSchemaInfo(clientName, schema.name)

    if (schemaInfo) {
      return this.databaseService.updateSchemaCollection(
        clientName,
        schema
      )
    }

    throw Error('No such schema')
  }

  /**
   * Find all schemas, optionally filtered by params.
   *
   * @param clientName
   * @param {{}} params
   * @returns {Promise<Promise<any>>}
   */
  public async find (clientName: string, params: any = {}) {
    const db = this.databaseService.getClientDb(clientName)

    const schemaInfo = await db.collection('schemas').find(params)

    if (schemaInfo) {
      return schemaInfo.toArray()
    }

    return []
  }

  public async removeSchema (clientName: string, schemaName: string) {
    return this.databaseService.removeSchema(clientName, schemaName)
  }

  public async removeSchemas (clientName: string) {
    return this.databaseService.removeCollection(clientName, 'schemas')
  }

  public async removeClient (clientName: string) {
    await this.databaseService.removeClientDb(clientName)
  }

  public async close () {
    await this.databaseService.close()
  }
}
