export interface IDocument {
  readonly schema: string
  readonly instance: object
}
