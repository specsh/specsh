import { IsString, IsInt } from 'class-validator'
import { ApiModelProperty } from '@nestjs/swagger'

export class CreateSchemaDto {
  /*
  @ApiModelProperty({type: String})
  @IsString()
  */
  readonly name

  /*
  @ApiModelProperty({type: String})
  @IsInt()
  */
  readonly uri
  /*
  @ApiModelProperty({type: object})
  @IsObject()
  */
  readonly schema
}
