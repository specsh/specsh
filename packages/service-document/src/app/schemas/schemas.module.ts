import { Module } from '@nestjs/common'

import { DatabaseModule } from '../database/database.module'
import { databaseProviders } from '../database/database.providers'
import { SchemasController } from './schemas.controller'
import { SchemasService } from './schemas.service'

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [
    SchemasController
  ],
  providers: [
    SchemasService,
    ...databaseProviders
  ],
  exports: [
    SchemasService
  ]
})
export class SchemasModule {}
