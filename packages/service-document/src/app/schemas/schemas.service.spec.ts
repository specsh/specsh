import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { expect } from 'chai'
import {DatabaseModule} from '../database/database.module'
import { SchemasService } from './schemas.service'
import { databaseProviders } from '../database/database.providers'

describe('SchemasService', () => {
  let module: TestingModule
  beforeEach(() => {
    return Test.createTestingModule({
      imports: [
        DatabaseModule
      ],
      providers: [
        SchemasService
      ]
    }).compile()
      .then(compiledModule => module = compiledModule)
  })

  let service: SchemasService
  beforeEach(() => {
    service = module.get(SchemasService)
  })

  afterEach(() => {
    service.close()
  })

  it('should exist', () => {
    expect(service).to.exist
  })

  it('should create schema collection if not exists', async () => {
    await service.removeClient('testClient')
    const created = await service.createSchemaCollectionIfNotExists(
      'testClient',
      'resume',
      'https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json'
    )
  })
})
