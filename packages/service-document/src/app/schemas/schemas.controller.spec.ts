import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { SchemasController } from './schemas.controller'
import { SchemasService } from './schemas.service'
import { databaseProviders } from '../database/database.providers'
import { expect } from 'chai'
import {DatabaseModule} from '../database/database.module'
import {SchemasModule} from './schemas.module'

describe('SchemasController', () => {
  let module: TestingModule
  before(() => {
    return Test.createTestingModule({
      imports: [
        SchemasModule
      ],
      controllers: [
        SchemasController
      ]
    }).compile()
      .then(compiledModule => module = compiledModule)
  })

  let controller: SchemasController
  before(() => {
    controller = module.get(SchemasController)
  })

  after(async() => {
    await controller.destroy()
  })

  it('exists', () => {
    expect(controller).to.exist
  })
})
