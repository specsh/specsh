import { ErrorMessage } from '@nestling/errors'
import { authErrors } from './auth.errors'

ErrorMessage.addErrorMessages(authErrors)

export * from './auth.guard'
export * from './auth.errors'
