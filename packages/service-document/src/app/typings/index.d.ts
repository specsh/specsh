import { MongoClient } from 'mongodb'

declare module 'mongo-schemer' {
  function connect(url: string): MongoClient
}
