export interface JWTModel {
  iss: string
  aud: string
  sub: string
  username: string
  exp: number
  iat: number
  jti: string
}
