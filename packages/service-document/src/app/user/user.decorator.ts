import { createParamDecorator } from '@nestjs/common'
import { getContext } from '@nestling/context'
import { ErrorMessage } from '@nestling/errors'
import { JWTModel } from './jwt.model'
import { userErrors } from './user.errors'
import { UserModel } from './user.model'

ErrorMessage.addErrorMessages(userErrors)

export interface UserDecoratorOptions {
  required?: boolean
}

export const User = createParamDecorator((options: UserDecoratorOptions, request): UserModel | null => {
  const decoded = getContext<JWTModel>('jwt', request)

  if (decoded) {
    const { sub, username } = decoded

    if (sub && username) {
      return {
        id: sub,
        name: username
      }
    }
  }

  if (options && options.required) {
    throw new ErrorMessage('user:invalid')
  }

  return null
})
