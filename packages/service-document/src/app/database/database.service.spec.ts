import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { assert, expect } from 'chai'
import { DatabaseModule } from '../database/database.module'
import { DatabaseService } from '../database/database.service'

const clientName = 'databaseTestClient'
const schemaName = 'testSchema'
const schemaUrl = 'https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json'

const personSchemaName = 'personSchema'
const personSchema =  {
  title: 'Person',
  type: 'object',
  properties: {
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    age: {
      description: 'Age in years',
      type: 'integer',
      minimum: 0
    }
  },
  required: ['firstName', 'lastName']
}
const personSchemaUrl = 'http://test.com/personSchema'

describe('DatabaseService', () => {
  let module: TestingModule
  let lastId;

  before(() => {
    return Test.createTestingModule({
      imports: [
        DatabaseModule
      ],
      providers: [
        DatabaseService
      ]
    }).compile()
      .then(compiledModule => module = compiledModule)
  })

  let databaseService: DatabaseService
  before(async () => {
    databaseService = module.get(DatabaseService)
  })

  after(async () => {
    await databaseService.close()
  })

  it('should exist', () => {
    expect(databaseService).to.exist
  })

  it('get client db', async () => {
    const result = await databaseService.getClientDb(clientName)

    assert.isOk(result)
  })

  it('create schema collection', async () => {
    const result = await databaseService.createSchemaCollection(
      clientName,
      schemaName,
      schemaUrl
    )

    assert.isOk(result)
  })

  it('create bogus schema collection', async () => {
    const result = await databaseService.createSchemaCollection(
      clientName,
      personSchemaName,
      personSchemaUrl,
      personSchema
    )

    assert.isOk(result)
  })

  it('get schema info', async () => {
    const result = await databaseService.getSchemaInfo(clientName, 'testSchema')
    assert.isOk(result)
  })

  it('insert document', async () => {
    const instance = {
      basics: {
        name: 'Rob Halff'
      }
    }

    const result = await databaseService.insert(
      clientName,
      'testSchema',
      instance
    )
    assert.ok(result._id)

    expect(result.basics).to.deep.equal({
      name: 'Rob Halff'
    })

    lastId = result._id
  })

  it('find document', async () => {
    const result = await databaseService.find(
      clientName,
      schemaName,
      {
      _id: lastId
    })

    expect(result.basics).to.deep.equal({
      name: 'Rob Halff'
    })
  })

  // Implement the whole query params, filter, include and relationship logic...
  // json-api (Request) -> query params -> mongo query params
  // Specsh (Request) -> query params -> mongo query params
  // Mongo response -> spec.sh response -> json api response
  // Would be nice to find some mongo help on pagination etc.
  // also note that insert, update, find is not trivial handling all those
  // parameters.
  // First: I should be able to define relations and set those relations.
  // After this is the ability to query for them
  // These relations..
  // Basically the client specifies the relation?
  // That's what's wrong with include I guess.
  // It's about freedom of the client on what to request.
  // When I have actually saved the relations that would be simpler.
  // Let's just start with being able to save relations to the database.
  // I think my approach is different, it does not serve frontend devs.
  // This is about defining all of these relations and using them.
  // making all named queries.
  // So for me you would specify which relations you want to use.

  it('create relation', async () => {
      const relations = await databaseService.addRelation(
        clientName,
        'oneToOne',
        {
          type: schemaUrl
        },
        {
          type: personSchemaUrl
        }
      )

      assert.ok(relations._id)

      expect(relations.source).to.deep.equal({
        type: schemaUrl
      })

      expect(relations.target).to.deep.equal({
        type: personSchemaUrl
      })
  })

  // but now how to relate the resume with the author?
  // at least make both available.
  // ofRelation.
  // the `relations` collection is only the definition.
  // - create project (type: project/uri)
  // - list available document types (project/1/relations/
  // - edit available documents.
  // - srd, urd are related one on one to the project
  //
  //  What I miss the concept of 'document' itself. srd/urd
  //  so the relation is project -> anyOf(srd, urd) (not really..)
  // It seems to me I'm just missing a Documents schema?
  // yeah, then it becomes project -> Documents relationship.
  // Thus project/1/documents/urd
  // no id's there because can ony have one.
  // also note, that none are tightly coupled.
  // the tight coupling is only realized by choosing a relation set.
  // if another relation set is loaded, the context will become different.
  // also keep thinking ipfs.
  // I can directly target Document which in this case contains srd, urd.
  // Document is the type of instances.
  // So there are instances which say their type is Document.
  // But what I'm then saying is that the relation ship is already
  // specified within each json schema.
  // In which case it's mainly a matter of what refs to resolve and what
  // references not.
  // which means my entire state can be considered one big schema.
  // ofcourse we will not create just one collection for this big app schema.
  // So how to chop them apart? How fine grained.
  // At least everything ending in a primitive is not a candidate to be a collection.
  // everything object is. What I'm getting at is to automatically
  // generate many collections. *always* even if it seem unnecessary.
  // break them down immediately.
  // which would involve a library just breaking a schema down into
  // mongodb collections.
  // I at least want to arrive at a workable implementation.
  // also note when everything is within it's own collection.
  // querying will become more difficult.
  // yet also think about atomic ipfs.
  // also would like some kind of filledBy
  // e.g. firstName, lastName
  // thus schemaOverlay.
  // which would include a relation and an overlay description.
  // which would also save the type of filledBy and not
  // fill in the schema instance itself.
  // I also note the chopability of a schema is arbitrary.
  // e.g. .basics of resume doesn't make much sense to be another collection.
  // whereas address does make sense to be another collection.
  // Storage Profile. What's the storage profile.
  // Where the storage profile describes how entities are chopped and saved.
  // Ok I think input should be a full schema containing the entire structure.
  // ref's are not important.
  // Then with dot paths I'm going to define what we are going to do with this structure.
  // Also there can then be multiple of those.
  // so in case of Project.Documents.urd etc.
  // There are two things.
  // One is what part of the slice will we use during rendering.
  // Probably that is then also one on one with what we are going to save.
  // Anyways, with that view, we are *not* going to render Project.documents
  // so that's the view.
  // Which works with omission instead of including.
  // Now if we want to show the overview, we do include Project.documents.
  // This is relevant for rendering, storing, client state, server side.
  // Then where is my relational problem in this case?
  // It seems to be non-existant, because we have provided the entire root structure.
  // Which makes all relations known. And ofcourse this only works, because
  // everything within this project is a one on one relations ship...
  // Anywayyy. for that this is sufficient.
  //
  // Another concept:
  // user, products, shopping cart.. :p
  const u = {
    user: {
      products: [
        'productId1',
        'productId2'
      ]
    }
  }

  const p = {
    type: 'object',
    title: 'Product',
    properties: {
      sku: {
        type: 'string'
      },
      title: {
        type: 'string'
      }
    }
  }

  const a = {
    type: 'object',
    title: 'Available Product',
    properties: {
      $ref: '#/product',
      available: {
        type: 'boolean'
      }
    }
  }

  const app = {
    type: 'object',
    properties: {
      user: {
        products: {
          type: 'array',
          items: {
            // we don't want to use productID here.
            // but just define that we mean a product.
            // however we are not going to create these products ourself.
            // so there should be a way to define we are not creating this ourselfs.
            // then even we accomplish that, we would have to be able to switch
            // sources
            $ref: '#AvailableProduct'
          }
        }
      }
    }
  }

  // 1. if nothing is done, we are creating our own embedded products.
  // 1. collections are always created by their type/uri.
  // 1. gah. we would have to replace the Product with AvailableProduct etc.
  // 2. user.products -> (clientId: ..., available: true} {extended with params)
  // 3. we cannot use attributes which are not known to the schema.
  //    hence the schemas will become more complex. and also will include
  //    application specific schemas, like the AvailableProduct schema.
  //    these extra schemas are necessary because we do not want to touch
  //    the original generic schemas. It's basically what you do in typescript/flow also.
  // 4. now all this is queryable by path.
  // 5. However how to determine which collections to create?
  //    as soon as things come relational collections should be made.
  //    as soon as items are non primitive collections should be made.
  // is it at all possible to make this generic???
  // could also be even mongo isn't the correct storage method.
  // while a nested product is no different then a product in a collection.
  // they are both the same type. Collection does not determine the type.
  // and a collection in general is detemined by type.
  // and even then a type is determined by structure not by it's place of definition.
  // can I make it less complex by using different query and storage methods..
  // at least not having to worry where to store it, or what to even name the collection.
  // Or I want to make it too generic right..
  // And if you build an application you hardly ever look back at the storage anyway.
  // if it runs it runs. (totally different approach)
  // Should not mix up what clients do with schemas and server side.
  //
  // Somehow have trouble keeping schema and schema instance apart.
  // The population of relations is demanded by the client.
  // The server side has less to do with the concept.
  // The server side can serve the relations
  // When stored, we enter a less schematic space.
  // we have schemas to work with for validation e.g.
  // where and how to store it is the main problem.
  // because I want to do it differently and more dynamic.
  // When a client pushes something with relations I should at least know
  // where it originates from.
  // let's keep it in-house.
  // do I have enough to create the urd and srd?
  // Perhaps better now go back to client side.


  it('update document', async () => {
    const result = await databaseService.update(
      clientName,
      schemaName,
      {
        _id: lastId,
        basics: {
          name: 'John Doe'
        }
      })

    expect(result.basics).to.deep.equal({
      name: 'John Doe'
    })
  })

  it('delete document', async () => {
    await databaseService.delete(
      clientName,
      schemaName,
      {
        _id: lastId
      })

    const result = await databaseService.find(
      clientName,
      schemaName,
      {
        _id: lastId
      })

    expect(result).to.equal(null)
  })

  it('remove client db', async() => {
    const result = await databaseService.removeClientDb(clientName)

    assert.isTrue(result)
  })
})
