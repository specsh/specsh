import { Inject, Injectable } from '@nestjs/common'
import { ObjectId } from 'mongodb'
import { environment } from '../../environments/environment'
import { convertSchema, fetchSchema } from './util'
// import * as MongoSchemer from 'mongo-schemer'

const clientId = environment.clientId

@Injectable()
export class DatabaseService {
  public static getSchemaCollection (db: any, schemaInfo: any) {
    return db.collection(DatabaseService.getCollectionName(schemaInfo))
  }

  public static getCollectionName (schemaInfo: any) {
    if (!schemaInfo._id) {
      throw Error('no schema _id found')
    }
    return `schema-${schemaInfo._id}`
  }

  constructor (
    @Inject('DbConnectionToken') private readonly mongo: any
  ) {}

  public getClientDb (clientName: string) {
    return this.mongo.db(`${clientId}-${clientName}`)

    /*
    if (!process.env.PRODUCTION) {
      return MongoSchemer.explainSchemaErrors(db, {
        onError: (errors) => console.error('SchemaErrors', errors)
      })
    }
    */
  }

  /**
   * Ensure collections exists.
   *
   * @param clientName
   * @returns {Promise<void>}
   */
  public async init (clientName: string) {
    const db = this.getClientDb(clientName)

    await db.collection('schemas')
  }

  /**
   * Creates a new schema collection.
   *
   * The schema *must* be available from a remote location,
   * while it will be the single source of truth.
   *
   * Currently supported schema locations:
   *   - http(s)
   *   - ipfs (TODO)
   *
   * @param clientName
   * @param schemaName
   * @param schemaUri
   * @param schemaContents
   * @param layouts
   * @param multiple
   * @returns {Promise<SchemaInfo>}
   */
  public async createSchemaCollection (
      clientName: string,
      schemaName: string,
      schemaUri: string,
      schemaContents: string | null = null,
      layouts: any[] = [],
      multiple: boolean = true
    ) {
    const db = this.getClientDb(clientName)
    const schemaCollection = await db.collection('schemas')

    let schema: any

    let uri: string

    if (schemaContents) {
      schema = JSON.parse(schemaContents)
      uri = `http://specsh/api/schemas/${schemaName}`
    } else {
      schema = await fetchSchema(schemaUri)
      uri = schemaUri
    }

    // adhoc removal of extension to schema
    if (schema.self) {
      delete schema.self
    }

    const $jsonSchema = convertSchema(JSON.parse(JSON.stringify(schema)))

    if (schema.hasOwnProperty('$schema')) {
      schema.schema = schema.$schema
      delete schema.$schema
    }

    await schemaCollection.insertOne({
      name: schemaName,
      uri,
      schema,
      layouts,
      multiple,
      createdAt: Date.now()
    })

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    await db.createCollection(
      DatabaseService.getCollectionName(schemaInfo),
      {
        validator: {
          $jsonSchema
        }
      }
    )

    return this.getSchemaInfo(clientName, schemaName)
  }

  /**
   * Updates a schema collection.
   *
   * @param clientName
   * @param schemaInfo
   * @returns {Promise<SchemaInfo>}
   */
  public async updateSchemaCollection (clientName: string, schemaInfo: any) {
    // replace with schema validation.

    const _id: ObjectId = schemaInfo._id
    const schemaName = schemaInfo.name
    const schemaUri = schemaInfo.uri
    const schemaContents = schemaInfo.schema
    const layouts = schemaInfo.layouts
    const multiple = schemaInfo.collection || true

    if (!_id) {
      throw Error('Cannot update without an id')
    }

    const db = this.getClientDb(clientName)
    const schemaCollection = await db.collection('schemas')

    let schema
    let uri

    if (schemaContents) {
      schema = JSON.parse(schemaContents)
      uri = `http://specsh/api/schemas/${schemaName}`
    } else if (schemaUri) {
      schema = await fetchSchema(schemaUri)
      uri = schemaUri
    } else {
      throw Error('Both schema contents and schemaUri are empty')
    }

    // adhoc removal of extension to schema
    if (schema.self) {
      delete schema.self
    }

    const $jsonSchema = convertSchema(JSON.parse(JSON.stringify(schema)))

    await schemaCollection.updateOne(
      { _id: new ObjectId(_id) },
      {
        $set: {
          name: schemaName,
          uri,
          schema,
          layouts,
          multiple,
          createdAt: Date.now()
        }
      })

    const enabled = false

    if (enabled) {
      const newSchemaInfo = await this.getSchemaInfo(clientName, schemaName)

      // will fail always except when rules are added.
      // so must handle the case of removal.
      // and then also update the collection
      await db.runCommand({
        collMod: DatabaseService.getCollectionName(newSchemaInfo),
        validator: $jsonSchema,
        validationLevel: 'strict'
      })
    }

    return this.getSchemaInfo(clientName, schemaName)
  }

  public async removeSchema (clientName: string, schemaName: string) {
    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const db = await this.getClientDb(clientName)

      await db.collection('schemas')
        .deleteOne({
          id: schemaInfo._id
        })

      await db.dropCollection(DatabaseService.getCollectionName(schemaInfo))
      return
    }

    return
  }

  public async removeCollection (clientName: string, collection: string) {
    return this.getClientDb(clientName)
      .collection(collection)
      .deleteMany({})
  }

  /**
   *  Retrieve schema info which consist of name, uri
   *
   * @param clientName
   * @param schemaName
   * @returns {Promise<SchemaInfo>}
   */
  public async getSchemaInfo (clientName: string, schemaName: string) {
    return this.getClientDb(clientName)
      .collection('schemas')
      .findOne({
        name: schemaName
      })
  }

  public async insert (clientName: string, schemaName: string, instance: any) {
    const db = this.getClientDb(clientName)

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const collection = DatabaseService.getSchemaCollection(db, schemaInfo)

      const result = await collection.insertOne(instance)

      return collection.findOne({ _id: result.insertedId })
    }

    return false
  }

  /**
   * Find a document in a collection schema.
   *
   * @param clientName
   * @param schemaName
   * @param params
   * @returns {Promise<any>}
   */
  public async find (clientName: string, schemaName: string, params: any) {
    const db = this.getClientDb(clientName)

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const collection = DatabaseService.getSchemaCollection(db, schemaInfo)

      return collection.findOne(params)
    }

    return null
  }

  public async update (clientName: string, schemaName: string, instance: any) {
    const db = this.getClientDb(clientName)

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const collection = DatabaseService.getSchemaCollection(db, schemaInfo)

      if (!instance._id) {
        throw Error('Refuse to update instance without an _id')
      }

      await collection.updateOne({
        _id: instance._id
      }, {
        $set: instance
      })

      return collection.findOne({
        id: instance._id
      })
    }

    return false
  }

  public async deleteById (clientName: string, schemaName: string, id: string) {
    const db = this.getClientDb(clientName)

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const collection = DatabaseService.getSchemaCollection(db, schemaInfo)

      return collection.deleteOne({
        _id: new ObjectId(id)
      })
    }

    return false
  }

  public async delete (clientName: string, schemaName: string, instance: any) {
    const db = this.getClientDb(clientName)

    const schemaInfo = await this.getSchemaInfo(clientName, schemaName)

    if (schemaInfo) {
      const collection = DatabaseService.getSchemaCollection(db, schemaInfo)

      if (!instance._id) {
        throw Error('Refuse to remove instance without an _id')
      }

      await collection.deleteOne({
        _id: instance._id
      })

      return true
    }

    return false
  }

  public async removeClientDb (clientName: string) {
    return this.getClientDb(clientName)
      .dropDatabase()
  }

  /**
   * Adds a relation between two document schemas to the database.
   *
   * Each side of the relation is a filter object where the default is
   * to find { _id }
   *
   * @param clientName
   * @param type
   * @param source
   * @param target
   * @returns {Promise<void>}
   */
  public async addRelation (clientName: string, type: string, source: any, target: any) {
    const db = this.getClientDb(clientName)

    const relations = await db.collection('relations')

    // for now
    // oneToOne
    // oneToMany

    let result
    if (type === 'oneToOne') {
      result = await relations.insertOne({
        type: 'oneToOne',
        source: {
          type: source.type // schema uri
        },
        target: {
          type: target.type // schema uri
        }
      })
    } else if (type === 'oneToMany') {
      result = await relations.insertOne({
        type: 'oneToMany',
        source: {
          type: source.type // schema uri
        },
        target: {
          type: target.type, // schema uri
          query: target.query || ['_id']
        }
      })
    } else {
      throw Error('Unknown relation type')
    }

    return relations.findOne({
      _id: result.insertedId
    })
  }

  public async close () {
    return this.mongo.close()
  }
}
