import * as $RefParser from 'json-schema-ref-parser'
// ...
/*
    ObjectId “objectId”

    Double	           “double”
    String	           “string”
    Object	           “object”
    Array	             “array”
    Binary data	       “binData”
    Boolean	           “bool”
    Date	             “date”
    Null	             “null”
    Regular Expression“regex”
    JavaScript	       “javascript”
    32-bit integer	   “int”
    Timestamp	         “timestamp”
    64-bit integer	   “long”
    Decimal128	       “decimal”
    Min key	           “minKey”
    Max key	           “maxKey”
*/
const primitives = {
  string: 'string',
  number: 'int',
  integer: 'int',
  boolean: 'bool'
}

const dropKeywords = [
  'format',
  // TODO: don't really want to drop keyword
  // example schema using it: https://raw.githubusercontent.com/rhalff/build-jsonschema-schema.org/master/build/Airline.json
  // should be $id nowadays, and that information is valuable
  'id'
]

/**
 * Takes a json schema and converts it to $jsonSchema.
 *
 * @param $schema
 */
export async function convertSchema ($schema: any) {
  const inlinedSchema = await $RefParser.dereference($schema)

  delete inlinedSchema.definitions

  if (inlinedSchema.$schema) {
    delete inlinedSchema.$schema
  }

  const $jsonSchema = (function walkSchema (schema: any) {
    if (!schema.hasOwnProperty('type')) {
      schema.type = 'object'
    }

    dropKeywords.forEach((keyword: string) => {
      if (schema.hasOwnProperty(keyword)) {
        delete schema[keyword]
      }
    })

    // Pick first type if there are multiple.
    const type: string = Array.isArray(schema.type) ? schema.type[0] : schema.type

    switch (type) {
      case 'string':
      case 'number':
      case 'integer':
      case 'boolean':
        schema.bsonType = primitives[type]
        break
      case 'array':
        schema.bsonType = 'array'

        if (schema.hasOwnProperty('items')) {
          if (Array.isArray(schema.items)) {
            schema.items = schema.items.map((item: any) => {
              const itemType = typeof item
              if (itemType === 'string') {
                // how does mongo handle that?
                return {
                  bsonType: primitives[itemType]
                }

              } else if (typeof item === 'object') {
                return walkSchema(item)
              } else {
                throw Error('Do not know how to handle item')
              }
            })
          } else {
            schema.items = walkSchema(schema.items)
          }
        }
        break
      case 'object':
        schema.bsonType = 'object'

        if (schema.hasOwnProperty('properties')) {
          Object.keys(schema.properties).forEach((name: string) => {
            schema.properties[name] = walkSchema(schema.properties[name])
          })
        }
        break
      default:
        if (type) {
          throw Error(`Unhandled schema type: ${type}`)
        } else {
          schema.bsonType = 'object'
        }
        break
    }

    delete schema.type

    return schema
  })(inlinedSchema)

  if ($jsonSchema.additionalProperties === false) {
    $jsonSchema.properties._id = {
      bsonType: 'objectId'
    }
  }

  return $jsonSchema
}

/**
 * Example mongodb $jsonSchema
 *    {
 *      $jsonSchema: {
 *        bsonType: "object",
 *          required: [ "name", "year", "major", "gpa" ],
 *          properties: {
 *          name: {
 *            bsonType: "string",
 *              description: "must be a string and is required"
 *          },
 *          gender: {
 *            bsonType: "string",
 *              description: "must be a string and is not required"
 *          },
 *          year: {
 *            bsonType: "int",
 *              minimum: 2017,
 *              maximum: 3017,
 *              exclusiveMaximum: false,
 *              description: "must be an integer in [ 2017, 3017 ] and is required"
 *          },
 *          major: {
 *          enum: [ "Math", "English", "Computer Science", "History", null ],
 *              description: "can only be one of the enum values and is required"
 *          },
 *          gpa: {
 *            bsonType: [ "double" ],
 *              description: "must be a double and is required"
 *          }
 *        }
 *      }
 *    }
 */
