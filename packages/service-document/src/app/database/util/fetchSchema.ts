import axios from 'axios'

export async function fetchSchema (schemaUrl: string): Promise<boolean> {
  const result = await axios.get(schemaUrl, {
    headers: {
      'Content-Type': 'application/json'
    }
  })

  if (result && result.data) {
    return result.data
  }

  return false
}
