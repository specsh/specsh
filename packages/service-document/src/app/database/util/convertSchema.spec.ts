import { expect } from 'chai'
import { convertSchema } from './convertSchema'

const resumeSchema = require('./mocks/schema.json')
const $resumeSchema = require('./mocks/$schema.json')

describe('convertSchema', () => {
  beforeEach(() => {
  })

  it('converts json schema to mongo $jsonSchema', () => {
    const jsonSchema = {
      $schema: 'http://json-schema.org/draft-04/schema#',
      title: 'Schema',
      properties: {
        name: {
          type: 'string'
        },
        email: {
          type: 'string',
          format: 'email'
        },
        agree: {
          type: 'boolean'
        },
        amount: {
          type: 'number'
        },
        stuff: {
          type: 'array',
          items: [
            'string',
            {
              type: 'boolean'
            },
            {
              type: 'object',
              properties: {
                something: {
                  type: 'string'
                }
              }
            }
          ]
        },
        otherStuff: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              many: {
                type: 'number'
              },
              ofThese: {
                type: 'string'
              }
            }
          }
        }
      }
    }

   const $jsonSchema =  convertSchema(jsonSchema)

    expect($jsonSchema).to.deep.equal({
      title: 'Schema',
      bsonType: 'object',
      properties: {
        name: {
          bsonType: 'string'
        },
        email: {
          bsonType: 'string'
        },
        agree: {
          bsonType: 'boolean'
        },
        amount: {
          bsonType: 'int'
        },
        stuff: {
          bsonType: 'array',
          items: [
            {
              bsonType: 'string'
            },
            {
              bsonType: 'boolean'
            },
            {
              bsonType: 'object',
              properties: {
                something: {
                  bsonType: 'string'
                }
              }
            }
          ]
        },
        otherStuff: {
          bsonType: 'array',
          items: {
            bsonType: 'object',
            properties: {
              many: {
                bsonType: 'int'
              },
              ofThese: {
                bsonType: 'string'
              }
            }
          }
        }
      }
    })
  })

  it('converts mock 1', () => {
    expect(convertSchema(resumeSchema)).to.deep.equal($resumeSchema)
  })
})
