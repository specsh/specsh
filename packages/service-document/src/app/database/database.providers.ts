import { MongoClient } from 'mongodb'
import { environment } from '../../environments/environment'

const {
  mongo: {
    uri
  }
} = environment

export const databaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => {
      try {
        console.log('Connecting to:', uri)
        return MongoClient.connect(uri)
      } catch (error) {
        throw Error('Failed to connect')
      }
    }
  }
]
