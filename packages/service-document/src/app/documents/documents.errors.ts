import { IErrorMessages } from '@nestling/errors'

export const documentsErrors: IErrorMessages = {
  type: 'document',
  errors: []
}
