import { Module } from '@nestjs/common'
import { DatabaseModule } from '../database/database.module'
import { databaseProviders } from '../database/database.providers'
import { SchemasModule } from '../schemas/schemas.module'
import { DocumentsController } from './documents.controller'
import { DocumentsService } from './documents.service'

@Module({
  imports: [
    DatabaseModule,
    SchemasModule
  ],
  controllers: [
    DocumentsController
  ],
  providers: [
    DocumentsService,
    ...databaseProviders
  ]
})
export class DocumentsModule {}
