import { Injectable } from '@nestjs/common'
import { DatabaseService } from '../database/database.service'

@Injectable()
export class DocumentsService {
  constructor (
    private readonly databaseService: DatabaseService
  ) {}

  /**
   * Inserts a document into the database of the client.
   *
   * Collection depends on the schema being used.
   *
   * @param clientName
   * @param schemaName
   * @param instance
   * @returns {Promise<void | Promise<InsertOneWriteOpResult> | OrderedBulkOperation | UnorderedBulkOperation>}
   */
  public async insert (clientName: string, schemaName: string, instance: any) {
    const result = this.databaseService.insert(clientName, schemaName, instance)

    if (result) {
      return result
    }

    throw Error(`Could not find schema for ${clientName}/${schemaName}`)
  }

  /**
   * Updates a document into the database of the client.
   *
   * Collection depends on the schema being used.
   *
   * @param clientName
   * @param schemaName
   * @param instance
   * @returns {Promise<any>}
   */
  public async update (clientName: string, schemaName: string, instance: any) {
    const result = this.databaseService.update(clientName, schemaName, instance)

    if (result) {
      return result
    }

    throw Error(`Could not find schema for ${clientName}/${schemaName}`)
  }

  /**
   * Removes a document from the database by id.
   *
   * Collection depends on the schema being used.
   *
   * @param clientName
   * @param schemaName
   * @param id
   * @returns {Promise<any>}
   */
  public async deleteById (clientName: string, schemaName: string, id: string) {
    const result = this.databaseService.deleteById(clientName, schemaName, id)

    if (result) {
      return result
    }

    throw Error(`Could not find schema for ${clientName}/${schemaName}`)
  }

  /**
   * Removes a document from the database of the client.
   *
   * Collection depends on the schema being used.
   *
   * @param clientName
   * @param schemaName
   * @param instance
   * @returns {Promise<any>}
   */
  public async delete (clientName: string, schemaName: string, instance: any) {
    const result = this.databaseService.delete(clientName, schemaName, instance)

    if (result) {
      return result
    }

    throw Error(`Could not find schema for ${clientName}/${schemaName}`)
  }

  /**
   * Find one document
   *
   * @param clientName
   * @param schemaName
   * @param {{}} params
   * @returns {Promise<SchemaCollection>}
   */
  public async findOne (clientName: string, schemaName: string, params: any = {}) {
    const db = this.databaseService.getClientDb(clientName)

    const schemaInfo = await this.databaseService.getSchemaInfo(clientName, schemaName)

    return DatabaseService
      .getSchemaCollection(db, schemaInfo)
      .findOne(params)
  }

  /**
   * Find all documents, optionally filtered by params.
   *
   * @param clientName
   * @param schemaName
   * @param {{}} params
   * @returns {Promise<Promise<any>>}
   */
  public async find (clientName: string, schemaName: string, params: any = {}) {
    const db = this.databaseService.getClientDb(clientName)

    const schemaInfo = await this.databaseService.getSchemaInfo(clientName, schemaName)

    // ok now should start properly implementing error codes.
    if (schemaInfo) {
      return DatabaseService
        .getSchemaCollection(db, schemaInfo)
        .find(params).toArray()
    }

    return {}
  }

  public async close () {
    await this.databaseService.close()
  }
}
