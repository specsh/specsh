import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import { AuthGuard } from '../auth'
import { User, UserModel } from '../user'
import { DocumentsService } from './documents.service'
import { IDocument } from './interfaces/IDocument'

/**
 *  /robberthalff/schemas (post new schema {name, schema})
 *  /robberthalff/schemas (get list of schemas [{name, schema}])
 *  /robberthalff/resume/schema (get schema)
 *  /robberthalff/resume (post instance)
 *  /robberthalff/resume (get instance, first if no params)
 *
 *  So should split creation of the schema and posting of the document.
 *  During schema post you also have to submit a name.
 */

@Controller('documents')
export class DocumentsController {
  constructor (private readonly documentsService: DocumentsService) {}

  @Get('ping')
  public async ping (): Promise<string> {
    return 'OK'
  }

  @Get(':schemaName')
  @UseGuards(AuthGuard)
  public async find (
    @User() user: UserModel,
    @Param('schemaName') schemaName: string,
    @Query() query: any
  ): Promise<IDocument> {
    console.log('schemaName', schemaName)
    return this.documentsService.find(user.id, schemaName, query.filter)
  }

  @Post(':schemaName')
  @UseGuards(AuthGuard)
  public async create (
    @User() user: UserModel,
    @Param('schemaName') schemaName: string,
    @Body() instance: object
  ): Promise<IDocument> {
    // TODO: we can use clientId here from the token to create the correct database.
    return this.documentsService.insert(user.id, schemaName, instance)
  }

  @Put(':schemaName')
  @UseGuards(AuthGuard)
  public async put (
    @User() user: UserModel,
    @Param('schemaName') schemaName: string,
    @Body() instance: object
  ): Promise<IDocument> {
    // TODO: we can use clientId here from the token to create the correct database.
    return this.documentsService.update(user.id, schemaName, instance)
  }

  @Delete(':schemaName/:id')
  @UseGuards(AuthGuard)
  public async deleteDocumentById (
    @User() user: UserModel,
    @Param('schemaName') schemaName: string,
    @Param('id') id: string,
  ): Promise<boolean> {
    // TODO: we can use clientId here from the token to create the correct database.
    return this.documentsService.deleteById(user.id, schemaName, id)
  }

  public async destroy () {
    await this.documentsService.close()
  }
}
