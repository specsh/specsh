import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { DocumentsController } from './documents.controller'
import { DocumentsService } from './documents.service'
import { databaseProviders } from '../database/database.providers'
import { expect } from 'chai'
import { DatabaseModule } from '../database/database.module'

describe('DocumentsController', () => {
  let module: TestingModule
  beforeEach(() => {
    return Test.createTestingModule({
      imports: [
        DatabaseModule
      ],
      controllers: [
        DocumentsController
      ],
      components: [
        DocumentsService,
        ...databaseProviders
      ]
    }).compile()
      .then(compiledModule => module = compiledModule)
  })

  let controller: DocumentsController
  beforeEach(() => {
    controller = module.get(DocumentsController)
  })

  afterEach(() => {
    controller.destroy()
  })

  it('exists', () => {
    expect(controller).to.exist
  })
})
