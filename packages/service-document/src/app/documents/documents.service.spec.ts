import { Test } from '@nestjs/testing'
import { TestingModule } from '@nestjs/testing/testing-module'
import { DocumentsService } from './documents.service'
import { SchemasService } from '../schemas/schemas.service'
import { assert, expect } from 'chai'
import { databaseProviders } from '../database/database.providers'
import {DatabaseModule} from '../database/database.module'
import {DatabaseService} from '../database/database.service'

describe('DocumentsService', () => {
  let module: TestingModule
  beforeEach(() => {
    return Test.createTestingModule({
      imports: [
        DatabaseModule
      ],
      components: [
        DatabaseService,
        SchemasService,
        DocumentsService
      ]
    }).compile()
      .then(compiledModule => module = compiledModule)
  })

  let documentsService: DocumentsService
  let schemasService: SchemasService
  beforeEach(async () => {
    documentsService = module.get(DocumentsService)
    schemasService = module.get(SchemasService)

    await schemasService.removeClient('testClient')

    await schemasService.createSchemaCollectionIfNotExists(
    'testClient',
     'resume',
      'https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json'
    )
  })

  afterEach(() => {
    documentsService.close()
    schemasService.close()
  })

  it('should exist', () => {
    expect(documentsService).to.exist
  })

  it('should be able to insert new schema instance', async () => {
    const instance = {
      basics: {
        name: 'Rob Halff',
        label: 'developer'
      }
    }
    const result = await documentsService.insert(
      'testClient',
      'resume',
      instance
    )

    assert.isOk(result._id)

    expect(result.basics).to.deep.equal({
      name: 'Rob Halff',
      label: 'developer'
    })
  })
})
