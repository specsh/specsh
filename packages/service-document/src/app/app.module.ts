import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { APP_FILTER } from '@nestjs/core'
import { ConfigModule } from '@nestling/config'
import { ContextModule } from '@nestling/context'
import { ErrorMessage, HttpExceptionFilter } from '@nestling/errors'
import { LoggerModule } from '@nestling/logger'
import { environment } from '../environments/environment'
import { authErrors } from './auth'
import { documentsErrors } from './documents/documents.errors'
import { DocumentsModule } from './documents/documents.module'
import { schemasErrors } from './schemas/schemas.errors'
import { SchemasModule } from './schemas/schemas.module'

ErrorMessage.addErrorMessages(authErrors)
ErrorMessage.addErrorMessages(documentsErrors)
ErrorMessage.addErrorMessages(schemasErrors)

@Module({
  imports: [
    ContextModule,
    LoggerModule.forRoot({
      name: 'specsh'
    }),
    ConfigModule.forRoot(environment, {
      prefix: process.env.OBJENV_PREFIX
    }),
    DocumentsModule,
    SchemasModule
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter
    }
  ]
})
export class ApplicationModule implements NestModule {
  configure (_: MiddlewareConsumer): void {
  }
}
