# Document Service 

This is the main document service for spec.sh

It stores the schemas and the document instances resulting from these schemas.

In essence it supports much more outside of the scope of spec.sh

In order to run the server a mongodb service must be available.

The authentication is handled by [Authumn](https://gitlab.com/authumn).

## Setup

Setup is done by providing environment variables and overwriting the environment defined in
`src/environment/environment.ts`



So:
 - Source schemas are in the repository specsh/packages/schemas
 - These schemas can be easily imported into mongodb, above repository provides a tool for this.
 - Then these schemas are served, to stay consistent, these are served using swagger-node also.
 - Once that's up and running, we start using the server to reference our schemas using `$ref`
   in this case within the swagger definitions. These definitions will always point to the
   live version. The deployed version.
   
First step is to actually create the schema repository.

# Frontend / Backend flow

I briefly mentioned how forms are generated from schemas, then create instances of those schemas.

And perhaps the new mongo feature can help us with the storage of these.

What I need:
  - conversion from json-schema to mongo schema (they have bsonType not type)
  - schema importer, which creates the collections.
  
Ok great it's possible to name your collection after an url.

This means I can use this for at least the user server and the document server.


```
$jsonSchema can be used in a document validator,
which enforces that inserted or updated documents are valid against the schema.
It can also be used to query for documents with the find command or
$match aggregation stage.
```

What this means, we can totally discard traditional lookups.
To me it's unclear if I can search database wide for documents matching a certain schema.
Otherwise it just means one collection per schema, which seems ok.

So it seems within this system, the most concern will go to the instances created.
The documents, the records.

I think at first I don't worry about that.
One schema is, one collection, which is defined using the schema.
The collection names can be directly derived from the schema name. 
Or even be directly named after the url (if possible)


### Super explode

Next step is to also put the swagger definitions themselve in the database.
Thus creating an api repository, that's right, an api repository...
A repository, stored within the database which defines all api's in use.

### The owner

The owner of this structure

Where to put it? AWS, ipfs, hosted api.

High level User. Not for developers.
Easy wizard
Sources.
Advanced tab.

### Mongo

Makes use of new features in 3.6, make sure feature compatibility is set to 3.6:
db.adminCommand( { setFeatureCompatibilityVersion: "3.6" } )

Good Read (fresh up):
https://blog.philipphauer.de/restful-api-design-best-practices/
