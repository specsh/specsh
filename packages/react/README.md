# Spec.sh React

# Running application locally

```shell
npm install
```

```shell
npm run build:dev
```

```shell
npm run start
```

# Testing the project

Updating snapshot
```shell
npm run test:update-snapshot
```

```shell
npm run test
```
