import "./dashboard.scss";
import * as React from "react";
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';

export class Dashboard extends React.Component {
    render() {
      return (
          <SplitterLayout primaryMinSize={400} secondaryMinSize={200}>
              <div className="my-pane">
                  <h2>Resume Editor</h2>
              </div>
              <div className="my-pane">
                  <h1>Resume Editor</h1>
              </div>
          </SplitterLayout>
      )
    }
  }
  