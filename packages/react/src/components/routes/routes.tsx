import * as React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { Main, NotFound } from "..";
import { Home, Dashboard } from "../../modules";

export class Routes extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/dashboard" />
                    </Route>
                    <Main path="/home" component={Home} checkAuthentication={false} />
                    <Main path="/dashboard" component={Dashboard} checkAuthentication={false} />
                    <Main path="*" component={NotFound} checkAuthentication={false} />
                </Switch>
            </Router>
        )
    }
}