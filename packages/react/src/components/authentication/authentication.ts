export const authenticate = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve({
            name: "Rob Halff",
            uid: "1"
        }), 0)
    });
}

export const checkIfAuthenticated = (store: any) => {
    return store.getState().isAuthed;
}