# Spec.sh DB

This repository contains the Spec.sh database driver.

It will be used in the Spec.sh http server.

### Schemas

`specsh-schemas` provides the document schemas used by Spec.sh
This schemas are also used to create the database structure.

The schema format for rxdb is very compatible with jsonschema.
However we will augment the schema with some extra data to ensure
proper creation of the database and collections.

Whenever our specsh document schemas use a `$ref` we will convert this
to a `ref` for rxdb also.

### Public Interface

 - `findDocument(document)`
 - `createDocument(document)`
 - `updateDocument(document)`
 - `removeDocument(document)`
