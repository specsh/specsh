import memdown from 'memdown'
import * as levelDbAdapter from 'pouchdb-adapter-leveldb'
import RxDB, { RxJsonSchema } from 'rxdb'

const dbName = 'specshdb'

const fullSchema = require('specsh-schemas/src/schemas/fullSchema.json')

RxDB.plugin(levelDbAdapter)

async function initDb() {
  RxDB.removeDatabase(dbName, memdown)

  const db = await RxDB.create({
    name: dbName,
    adapter: memdown
  })

  db.$.subscribe(changeEvent => console.dir(changeEvent))

  console.log('db initialized')

  const json = await db.dump()

  console.log(json)

  const schema = {
    version: 0,
    type: 'object',
    properties: fullSchema.properties.urd.properties
  } as RxJsonSchema
  // console.log(JSON.stringify(schema, null, 2))

  const collection = await db.collection({
    name: 'urd',
    schema
  })


  collection.remove()
}

initDb()
  .catch((error) => {
    console.error(error)
  })

