import { Logger } from '../roarr/log'
import {
  Middleware,
  NestMiddleware,
  ExpressMiddleware
} from '@nestjs/common'

@Middleware()
export class LoggerMiddleware implements NestMiddleware {
  resolve (context: object = {}): ExpressMiddleware {
    const logger = Logger.child(context)

    return (req, res, next) => {
      logger(req.headers)
      next()
    }
  }
}
