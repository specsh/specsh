import {
  RoarrGlobalStateType
} from '../types';

export const createRoarrInitialGlobalState = (): RoarrGlobalStateType => {
  return {
    prepend: {},
    sequence: 0
  };
};
