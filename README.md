# Spec.sh

This is the monorepo for [Spec.sh](https://spec.sh)

In consists of the follow packages:
  - editor: a Specifications editor
  - cli: Command line tool to maintain specifications.
  - desktop: The desktop application which can be initialized from your git project.

Besides that it contains all shared libraries.

# Setup

The api server is meant to be protected by an authentication service.

https://github.com/authumn can be used for this.

The api itself only checks if there is a valid token and does not validate the user itself
as this is to be done by the authentication proxy itself.

Once authentication is setup the api can be used.


